<?php
/**
 * Created by PhpStorm.
 * User: jaskokoyn
 * Date: 6/12/2015
 * Time: 12:36 PM
 */

require( 'inc/db.php' );
require( 'inc/func.inc.php' );


$updateQuery        =   $db->prepare("
ALTER TABLE `settings` ADD `currency_code` VARCHAR(12) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'USD' ;
ALTER TABLE `settings` ADD `infinite_pagination` TINYINT(1) UNSIGNED NOT NULL DEFAULT '2' ;
");

$updateQuery->execute();

echo 'If you see no errors before this message, then you have successfully updated Rocketeer. Please delete this file.';