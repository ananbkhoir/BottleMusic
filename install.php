<?php
include('inc/func.inc.php');
$m  =   "";

if(isset($_POST['dbhost'], $_POST['dbname'], $_POST['dbusername'], $_POST['dbpass'])){
    try {
        $db = new PDO('mysql:host=' . $_POST['dbhost'] . ';dbname=' . $_POST['dbname'], $_POST['dbusername'], $_POST['dbpass']);
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $dbSQL   =   file_get_contents('db.sql.txt');
        $createSite =   $db->prepare($dbSQL);
        $createSite->execute();
        $createSite->closeCursor();

        $insertSettingsQuery    =   $db->prepare("INSERT INTO settings(site_domain,site_dir) VALUES(:sd,:dir)");
        $insertSettingsQuery->execute(array(
            ":sd"               =>  $_POST['site_domain'],
            ":dir"              =>  $_POST['site_dir']
        ));

        // Create Directory
        if ( !mkdir("uploads/a/1/admin", 0777, true) ) {
            die("Unable to make admin directory");
        }

        $insertUserQuery    =   $db->prepare("INSERT INTO users(username,pass,email,ip,upl_dir,display_name,header_img,user_code,isAdmin,isPro,email_confirmed,pro_expires)
                                              VALUES( :u, :p, :e, :i, :upl, :dn, :hi, :uc, :admin, :pro, :confirmed, :expires  )");
        $insertUserQuery->execute(array(
            ":u"    =>  "admin",
            ":p"    =>  secure("admin", true),
            ":e"    =>  "admin@" . $_POST['site_domain'],
            ":i"    =>  $_SERVER['REMOTE_ADDR'],
            ":upl"  =>  "a/1/admin",
            ":dn"   =>  "Admin",
            ":hi"   =>  "headers/" . mt_rand(1,9) . ".jpg",
            ":uc"   =>  md5("admin"),
            ":admin"    =>  2,
            ":pro"      =>  2,
            ":confirmed"    =>  2,
            ":expires" =>   99999999999
        ));

        unlink("db.sql.txt");

        $dbFile   =   file_get_contents('inc/db-sample.php');
        $dbFile   =   str_replace("PHOHOST",trim($_POST["dbhost"]),$dbFile);
        $dbFile   =   str_replace("PHODBNAME",trim($_POST["dbname"]),$dbFile);
        $dbFile   =   str_replace("PHOUSER",trim($_POST["dbusername"]),$dbFile);
        $dbFile   =   str_replace("PHOPASS",trim($_POST["dbpass"]),$dbFile);
        $fh         =   fopen('inc/db.php', 'w') or die("Can't open config.php. Make sure it is writable.");
        fwrite($fh, $dbFile);
        fclose($fh);

        $m  =   '
<div class="alert alert-success text-center">
    <strong>Euphonize hass been installed successfully!</strong><br>
    To get started, please log into the account we created for you during installation.<br>
    <strong>Username: </strong> admin <br>
    <strong>Password: </strong> admin<br>
    <strong>PLEASE DELETE THE install.php FILE! THIS IS VERY IMPORTANT</strong>
</div>';
    } catch(PDOException $e) {
        $m  = 'ERROR: ' . $e->getMessage();
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Euphonize Installer</title>

    <!-- Bootstrap -->
    <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body style="background-color: #E2DFDA;">

<div class="container">
    <div class="panel panel-primary" style="margin-top: 25px;">
        <div class="panel-heading">
            <h3 class="panel-title">Euphonize Installer</h3>
        </div>
        <div class="panel-body">
            <?php echo $m; ?>
            <form method="POST">
                <div class="form-group">
                    <label>Database Host</label>
                    <input type="text" class="form-control" name="dbhost">
                </div>
                <div class="form-group">
                    <label>Database Name</label>
                    <input type="text" class="form-control" name="dbname">
                </div>
                <div class="form-group">
                    <label>Database Username</label>
                    <input type="text" class="form-control" name="dbusername">
                </div>
                <div class="form-group">
                    <label>Database Password</label>
                    <input type="text" class="form-control" name="dbpass">
                </div>
                <div class="form-group">
                    <label>Site Domain</label>
                    <input type="text" class="form-control" name="site_domain">
                    <span id="helpBlock" class="help-block">
                        The domain name of your site. Do not include http:// or a forward slash after the domain.
                        Example: euphonize-demo.com
                    </span>
                </div>
                <div class="form-group">
                    <label>Site Directory</label>
                    <input type="text" class="form-control" name="site_dir">
                    <span id="helpBlock" class="help-block">
                        The directory your site is installed. If you're installing on the root, then
                        just leave it as / If your site is installed in a folder called music then you would input
                        /music/
                    </span>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-success btn-block">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>