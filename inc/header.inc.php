<!DOCTYPE html>
<html lang="en" ng-app="euphonizeApp">
<head>
    <base href="<?php echo $settings['site_dir']; ?>">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $settings['site_name']; ?></title>
    <meta name="description" content="<?php echo $settings['site_desc']; ?>">
    <link rel="shortcut icon" href="assets/main/img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="assets/main/img/favicon.ico" type="image/x-icon">
    <!-- Bootstrap -->
    <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Library -->
    <link href="assets/nprogress/ngprogress-lite.css" rel="stylesheet">
    <link href="assets/toastr/toastr.min.css" rel="stylesheet">
    <link href="assets/tagsinput/bootstrap-tagsinput.css" rel="stylesheet">
    <link href="assets/soundmanager/demo/bar-ui/css/bar-ui.css" rel="stylesheet">

    <!-- Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:500,900,300,700,400' rel='stylesheet' type='text/css'>
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="assets/flaticon/flaticon.css" rel="stylesheet" type="text/css" >

    <!-- Custom Styles -->
    <link href="assets/main/css/main.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div id="fb-root"></div>
<header>
    <div class="colorful-header"><div></div><div></div><div></div><div></div></div>
    <?php include( 'top-nav.inc.php' ); ?>
</header>