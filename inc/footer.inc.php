<?php

include('terms.php');
include('share-modal.php');
include('playlist-modal.php');
include('add-track-playlist.php');
include('player.php');

?>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/dropzone/dropzone.js"></script>
<script src="assets/toastr/toastr.min.js"></script>
<script src="assets/tagsinput/bootstrap-tagsinput.min.js"></script>
<script src="assets/soundmanager/script/soundmanager2-jsmin.js"></script>
<script src="https://www.google.com/recaptcha/api.js?onload=vcRecaptchaApiLoaded&render=explicit" async defer></script>
<script src="https://platform.twitter.com/widgets.js" id="twitter-wjs"></script>

<!-- Custom -->
<script src="assets/main/js/main.js"></script>

<!-- Angular -->
<script src="assets/angular/angular.min.js"></script>
<script src="app/libs/angular-ui-router.min.js"></script>
<script src="app/libs/angular-animate.min.js"></script>
<script src="assets/nprogress/ngprogress-lite.js"></script>
<script src="assets/ui-bootstrap/ui-bootstrap.min.js"></script>
<script src="assets/file-upload/angular-file-upload-shim.min.js"></script>
<script src="assets/file-upload/angular-file-upload.min.js"></script>
<script src="assets/angular/angular-sanitize.min.js"></script>
<script src="assets/recaptcha/angular-recaptcha.min.js"></script>

<!-- App -->
<script src="app/app.js"></script>
<script src="app/routes.js"></script>
<script src="app/directives/scrollglue.js"></script>
<script src="app/controllers/nav.js"></script>
<script src="app/controllers/home.js"></script>
<script src="app/controllers/register.js"></script>
<script src="app/controllers/login.js"></script>
<script src="app/controllers/forgot.js"></script>
<script src="app/controllers/profile.js"></script>
<script src="app/controllers/profile.following.js"></script>
<script src="app/controllers/profile.followers.js"></script>
<script src="app/controllers/profile.playlists.js"></script>
<script src="app/controllers/upload-tracks.js"></script>
<script src="app/controllers/edit-track.js"></script>
<script src="app/controllers/edit-profile.js"></script>
<script src="app/controllers/discover.js"></script>
<script src="app/controllers/playlists.js"></script>
<script src="app/controllers/player.js"></script>
<script src="app/controllers/track.js"></script>
<script src="app/controllers/user-playlist.js"></script>
<script src="app/controllers/add-track-playlist.js"></script>
<script src="app/controllers/playlist.js"></script>
<script src="app/controllers/report.js"></script>
<script src="app/controllers/inbox.js"></script>
<script src="app/controllers/validate.js"></script>
<script src="app/controllers/share.js"></script>
<script src="app/controllers/terms.js"></script>
<script src="app/controllers/manage-tracks.js"></script>
<script src="app/controllers/pro.js"></script>
<script src="app/controllers/reset-verify.js"></script>

</body>
</html>