<?php
/**
 * Created by PhpStorm.
 * User: jaskokoyn
 * Date: 2/24/2015
 * Time: 8:56 PM
 */
require( 'inc/db.php' );
include( 'inc/func.inc.php' );

if( !isset($_SESSION['loggedin']) && isset($_COOKIE['PHO_TOKEN'])){
    $token  =   secure($_COOKIE['PHO_TOKEN']);
    $tokenQuery  =   $db->prepare("SELECT * FROM users WHERE login_token = :token");
    $tokenQuery->execute(array(
        ":token"    =>  $token
    ));
    if($tokenQuery->rowCount() == 1){
        $tokenRow    =   $tokenQuery->fetch();
        $_SESSION['loggedin']   =   true;
        $_SESSION['username']   =   $tokenRow['username'];
        $_SESSION['uid']        =   $tokenRow['id'];

        if($tokenRow['isAdmin'] == 2){
            $_SESSION['isAdmin']    =   true;
        }
    }
}

if( isset($_SESSION['loggedin']) ){
    $userQuery  =   $db->prepare("SELECT * FROM users WHERE id = :id LIMIT 1");
    $userQuery->execute(array( ":id"    =>  $_SESSION['uid'] ));
    if($userQuery->rowCount() == 1){
        $userRow    =   $userQuery->fetch();

        if($userRow['status'] == 2){
            header("location:logout.php");
            exit();
        }

        if($userRow['pro_expires'] < time()){
            $updateProQuery =   $db->prepare("UPDATE users SET isPro='1' WHERE id = :uid");
            $updateProQuery->execute(array(
                ":uid"  =>  $userRow['id']
            ));
        }
    }
}

$genresQuery        =   $db->prepare("SELECT * FROM genres ORDER BY `pos`");
$genresQuery->execute();
$genres             =   $genresQuery->fetchAll(PDO::FETCH_ASSOC);

include( 'inc/header.inc.php' );
?>
<script>
    var countryArr  =   <?php echo json_encode($countryArray); ?>;
    var genresArr   =   <?php echo json_encode($genres); ?>;
    var settings    =   <?php echo json_encode($settings); ?>;
</script>
<div class="ui-view-container">
    <div ui-view></div>
    <footer class="container">
        <?php echo $settings['footer_ad']; ?>
        <p class="text-center">
            <?php echo $settings['footer_text']; ?>
            <a href="#" data-toggle="modal" data-target="#termsModal">Terms & Condition</a>
        </p>
    </footer>
</div>
<?php
include( 'inc/footer.inc.php' );