<?php
/**
 * Created by PhpStorm.
 * User: jaskokoyn
 * Date: 2/26/2015
 * Time: 5:03 PM
 */

require( 'inc/db.php' );
include( 'inc/func.inc.php' );

session_start();

if( isset($_SESSION['loggedin']) ){
    $userQuery  =   $db->prepare("UPDATE users SET login_token = :token, token_exp = :exp WHERE id = :id");
    $userQuery->execute(array(
        ":token"    =>  null,
        ":exp"      =>  null,
        ":id"       =>  $_SESSION['uid']
    ));
}


session_unset();
session_destroy();

setcookie( "PHO_TOKEN", "", (time() - 15552000), "/" );

header("location: index.php");
exit();