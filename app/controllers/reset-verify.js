/**
 * Created by jaskokoyn on 4/7/2015.
 */
euphonizeApp.controller("resetVerifyController", function($rootScope, $scope, $stateParams, $state, $http, $timeout){
    $scope.isResetted           =   false;
    $scope.isSuccess            =   false;
    $scope.isError              =   false;
    $scope.validMessage         =   null;

    if($stateParams.code === ""){
        console.log("ayy");
        $state.go("home");
        return null;
    }

    $http({
        method: 'POST',
        url: 'actions/reset-password.php',
        data: $stateParams
    }).success(function(data,config,headers,status){
        $scope.isResetted  =   true;

        if(data.status == 2){
            $scope.validMessage =   "Your password has been reset. Please check your email for a new password.";
            $scope.isSuccess    =   true;
        }else{
            $scope.validMessage =   "Unable to reset password. Try again later.";
            $scope.isError      =   true
        }
    });
});