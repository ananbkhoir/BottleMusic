/**
 * Created by jaskokoyn on 3/15/2015.
 */
euphonizeApp.controller("addTrackPlaylistController", function($scope, $rootScope, $http){
    $scope.checkingTrack                =   true;
    $scope.addTrackPlaylists            =   [];
    $scope.currentPlaylistTrack         =   null;
    $scope.loggedIn                     =   false;

    $rootScope.checkTrackPlaylist       =   function(track){
        $scope.checkingTrack            =   true;
        $scope.currentPlaylistTrack     =   track;

        $http({
            method: 'POST',
            data: {
                id: track.id
            },
            url: "actions/check-track-playlist.php"
        }).success(function(data,status,headers,config){
            $scope.checkingTrack        =   false;

            if(data.status == 1){
                $scope.loggedIn         =   true;
                return null;
            }

            $scope.addTrackPlaylists    =   data.playlists;
        });
    };

    $scope.toggleTrackPlaylist          =   function(playlist, $event){
        if($(event.target).find(".fa").hasClass("fa-spinner")){
            return null;
        }

        $($event.target).find(".fa").removeClass("fa-plus fa-check").addClass("fa-spinner fa-pulse");

        $http({
            method: 'POST',
            data: {
                pid: playlist.id,
                tid: $scope.currentPlaylistTrack.id
            },
            url: "actions/toggle-track-playlist.php"
        }).success(function(data,status,headers,config){
            if(data.status == 2){
                $($event.target).find(".fa").removeClass("fa-spinner fa-pulse").addClass("fa-plus");
            }else if(data.status == 3){
                $($event.target).find(".fa").removeClass("fa-spinner fa-pulse").addClass("fa-check");
            }else{
                $($event.target).find(".fa").removeClass("fa-spinner fa-pulse").addClass("fa-plus");
            }
        });
    }
});