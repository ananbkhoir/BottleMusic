/**
 * Created by jaskokoyn on 4/6/2015.
 */
euphonizeApp.controller("proController", function($rootScope, $scope, $http){
    $scope.isLoggedIn   =   false;
    $scope.uid          =   null;
    $scope.isPro        =   false;
    $scope.expires      =   null;

    $http({
        method: 'POST',
        url: 'actions/get-pro-details.php'
    }).success(function(data,status,headers,config){
        if(data.status == 1){
            return null;
        }
        $("input[name=custom]").val(data.uid);
        $scope.uid          =   data.uid;
        $scope.isLoggedIn   =   true;
        $scope.isPro        =   data.isPro;
        $scope.expires      =   data.expires;
    });
});