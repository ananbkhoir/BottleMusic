/**
 * Created by jaskokoyn on 3/7/2015.
 */
euphonizeApp.controller("editProfileController", function($rootScope, $scope, $http, $state, $upload, profileObj){
    profileObj.intro_text   =   $rootScope.sanitizeText(profileObj.intro_text);
    profileObj.about        =   $rootScope.sanitizeText(profileObj.about);
    $scope.profile          =   profileObj;
    $scope.pass             =   {
        newPass1: null,
        newPass2: null
    };

    if($scope.profile.status == 1){
        $state.go("home");
        return null;
    }

    $scope.editStatus   =   false;
    $scope.editAlert    =   "alert-info";
    $scope.editPasswordMessage  =   "";
    $scope.editPasswordStatus   =   false;
    $scope.editPasswordAlert    =   "alert-info";
    $scope.editMessage  =   "";
    $scope.countries    =   countryArr;
    $scope.profileImg   =   profileObj.profile_img;
    $scope.headerImg    =   profileObj.header_img;

    $scope.updateProfile    =   function(){
        $scope.editStatus   =   true;
        $scope.editMessage  =   "Please wait while your profile is being updated.";
        $scope.editAlert    =   "alert-info";

        $("#editProfileForm").slideUp('fast', function(){
            $http.post('actions/edit-profile.php', $scope.profile).success(function(data, status, headers, config) {
                $("#editProfileForm").slideDown('fast');
                $scope.editMessage  =   data.message;

                if(data.status == 2){
                    $scope.editAlert            =   "alert-success";
                }else{
                    $scope.editAlert    =   "alert-warning";
                }
            }).error(function(data, status, headers, config) {
                $scope.editMessage  =   "This profile could not be updated. Please try again later.";
                $scope.editAlert    =   "alert-danger";
                console.log(data);
            });
        });
    };

    $scope.uploadImg = function (files, strType) {
        if (!files || !files.length) {
            return;
        }
        toastr.info("Your " + strType + " image is uploading!", 'Please wait!');

        $upload.upload({
            url: 'actions/upload-' + strType + '-img.php',
            file: files[0]
        }).success(function (data, status, headers, config) {
            console.log(data);
            if(data.status != 2){
                return toastr.warning(data.message, 'Uh oh!');
            }

            if(strType == "profile") {
                $scope.profileImg = data.img;
                $("#menu-profile-img").attr('src', 'uploads/' + data.img);
            }else{
                $scope.headerImg =   data.img;
            }

            toastr.success(data.message, 'Success!');
        });
    };

    $scope.updatePassword   =   function(){
        $scope.editPasswordStatus   =   true;
        $scope.editPasswordMessage  =   "Please wait while your password is being updated.";
        $scope.editPasswordAlert    =   "alert-info";

        $("#changePasswordForm").slideUp('fast', function(){
            $http.post('actions/update-password.php', $scope.pass).success(function(data, status, headers, config) {
                console.log(data);
                $("#changePasswordForm").slideDown('fast');
                $scope.editPasswordMessage  =   data.message;

                if(data.status == 2){
                    $scope.editPasswordAlert            =   "alert-success";
                }else{
                    $scope.editPasswordAlert    =   "alert-warning";
                }
            }).error(function(data, status, headers, config) {
                $scope.editPasswordMessage  =   "This profile could not be updated. Please try again later.";
                $scope.editPasswordAlert    =   "alert-danger";
                console.log(data);
            });
        });
    };
});