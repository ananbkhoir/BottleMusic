/**
 * Created by jaskokoyn on 3/15/2015.
 */
euphonizeApp.controller("userPlaylistController", function($scope, $rootScope, $http){
    $scope.editMode                 =   false;
    $scope.editedPlaylist           =   {};
    $scope.editMessage              =   "";
    $scope.editAlert                =   "";
    $scope.showEditStatus           =   false;
    $scope.newPlaylistName          =   null;
    $scope.creatingPlaylist         =   false;
    $scope.userPlaylists            =   [];

    $scope.createPlaylist           =   function(isValid){
        if(!isValid){
            return null;
        }

        $scope.creaingPlaylist      =   true;

        $http({
            method: 'POST',
            data: {
                name: $scope.newPlaylistName
            },
            url: 'actions/create-playlist.php'
        }).success(function(data, status, headers, config){
            if(data.status == 1){
                return null;
            }

            $scope.newPlaylistName  =   null;
            $scope.creatingPlaylist =   false;
            $scope.userPlaylists.push(data.playlist);
        });
    };

    $scope.editPlaylist             =   function(playlist){
        $scope.editMode             =   true;
        $scope.editedPlaylist       =   playlist;
    };

    $scope.submitEditPlaylist       =   function(isValid){
        $scope.showEditStatus       =   true;
        $scope.editAlert            =   "alert-info";
        $scope.editMessage          =   "Please wait. We're updating your playlist info.";

        if(!isValid){
            $scope.editAlert        =   "alert-danger";
            $scope.editMessage      =   "Unable to update playlist. Please try again later.";
            return null;
        }

        $http({
            method: 'POST',
            data: $scope.editedPlaylist,
            url: 'actions/edit-playlist.php'
        }).success(function(data, status, headers, config){
            if(data.status == 2){
                $scope.editAlert    =   "alert-success";
                $scope.editMessage  =   "Playlist successfully updated!";
            }else{
                $scope.editAlert    =   "alert-danger";
                $scope.editMessage  =   "Unable to update playlist. Please try again later.";
            }
        });
    };

    $scope.goBackToPlaylists        =   function(){
        $scope.editMode             =   false;
        $scope.editedPlaylist       =   {};
        $scope.showEditStatus       =   false;
        $scope.editAlert            =   "";
        $scope.editMessage          =   "";
    };

    $scope.deletePlaylist           =   function(playlist, $index){
        if(confirm("Are you sure you want to do this? This action cannot be undone.")){
            $rootScope.userPlaylists.splice($index,1);
            $http({
                method: 'POST',
                url: 'actions/delete-playlist.php',
                data: playlist
            });
        }
    }
});