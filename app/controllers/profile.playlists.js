/**
 * Created by jaskokoyn on 3/17/2015.
 */
euphonizeApp.controller("profilePlaylistsController", function($scope, $rootScope, $http, playlistsObj){
    $scope.profilePlaylists             =   playlistsObj.playlists;

    $scope.toggleLike                   =   function(playlist, $index){
        $http({
            method: 'POST',
            url: 'actions/update-playlist-likes.php',
            data: playlist
        }).success(function(data, status, headers, config){
            console.log(data);
            if(data.status == 1){
                toastr.error("Unable to like/unlike playlist. Try again later.", 'Error!');
                return null;
            }else if(data.status == 2){
                $scope.profilePlaylists[$index].isOrange  =   false;
                $scope.profilePlaylists[$index].like_count--;
            }else if(data.status == 3){
                $scope.profilePlaylists[$index].isOrange  =   true;
                $scope.profilePlaylists[$index].like_count++;
            }
        });
    };

    $scope.playPlaylist                 =   function(playlist){
        $http({
            method: 'POST',
            url: 'actions/get-playlist-tracks.php',
            data: playlist
        }).success(function(data, status, headers, config){
            $rootScope.playlistTracks   =   data.tracks;
            if(data.tracks[0]){
                $rootScope.playTrack(data.tracks[0], true, 0);
            }
        });
    };
});