/**
 * Created by jaskokoyn on 4/3/2015.
 */
euphonizeApp.controller("validateController", function($rootScope, $scope, $stateParams, $state, $http, $timeout){
    $scope.isValidated          =   false;
    $scope.isSuccess            =   false;
    $scope.isError              =   false;
    $scope.validMessage         =   null;

    if($stateParams.code === ""){
        $state.go("home");
        return null;
    }

    $http({
        method: 'POST',
        url: 'actions/validate-email.php',
        data: $stateParams
    }).success(function(data,config,headers,status){
        $scope.isValidated  =   true;

        if(data.status == 2){
            $scope.validMessage =   "E-mail confirmed! You may now start browsing the site. You will be redirected to the home page in 10 seconds.";
            $scope.isSuccess    =   true;

            $timeout(function(){
                $state.go("home");
            }, 10000);
        }else{
            $scope.validMessage =   "Unable to confirm your account. Please try again later.";
            $scope.isError      =   true
        }
    });
});