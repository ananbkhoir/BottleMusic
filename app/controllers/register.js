/**
 * Created by jaskokoyn on 2/25/2015.
 */
euphonizeApp.controller("registerController", function($rootScope, $scope, $http, $state, vcRecaptchaService){
    $http({
        method: 'POST',
        url: 'actions/check-login.php'
    }).success(function(data,status,headers,config){
        console.log(data);
        if(data.status == 1){
            $state.go("home");
        }
    });

    $scope.regUser          =   {
        username:   "",
        pass1:      "",
        pass2:      "",
        email:      "",
        agree:      false,
        captcha:    null
    };
    $scope.regAlert         =   "";
    $scope.isSubmitted      =   false;
    $scope.regStatus        =   "";
    $scope.isCaptcha        =   $rootScope.settings.captcha_enabled == 1;
    $scope.captchaID        =   null;

    $scope.setWidgetId      = function (widgetId) {
        $scope.captchaID    =   widgetId;
    };

    $scope.setResponse = function (response) {
        $scope.regUser.captcha  =   response;
        $scope.isCaptcha        =   false;
    };

    $scope.register =   function(){
        $scope.isSubmitted      =   true;
        $scope.regStatus        =   "Please wait! We are creating your account.";
        $scope.regAlert         =   "alert-info";

        $("#registerForm").slideUp('fast', function(){
            $http.post('actions/register.php', $scope.regUser).success(function(data, status, headers, config) {
                // this callback will be called asynchronously
                // when the response is available
                console.log(data);

                $scope.regStatus    =   data.message;
                if(data.status == 1){
                    $scope.regAlert         =   "alert-success";

                    if($rootScope.settings.validate_email == 1){
                        $scope.regStatus    = "Before you can do anything please confirm your email.";
                        return null;
                    }

                    location.href           =   "index.php";
                }else{
                    $scope.regAlert         =   "alert-warning";
                    $("#registerForm").slideDown('fast');
                }
            }).error(function(data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                $scope.regAlert         =   "alert-danger";
                $scope.regStatus        =   "Something went wrong. Please try again later.";
                $("#registerForm").slideDown('fast');
                console.log(data);
            });
        });
    };
});