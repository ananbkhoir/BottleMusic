/**
 * Created by jaskokoyn on 4/6/2015.
 */
euphonizeApp.controller("manageTracksController", function($rootScope, $scope, $state, $http){
    $scope.tracks           =   [];

    $http({
        method: 'POST',
        url: 'actions/get-all-user-tracks.php'
    }).success(function(data,status,headers,config){
        if(data.status == 1){
            $state.go("home");
        }

        $scope.tracks       =   data.tracks;
    });
});