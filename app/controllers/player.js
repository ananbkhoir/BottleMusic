/**
 * Created by jaskokoyn on 3/11/2015.
 */
euphonizeApp.controller("playerController", function($scope, $rootScope, $http){
    $scope.isPlaying            =   false;
    $scope.currentTrack         =   null;
    $scope.trackDuration        =   "0:00";
    $scope.artist               =   null;
    $scope.song_title           =   null;
    $scope.currentTime          =   "0:00";
    $scope.volumeElem           =   {
        x: 0,
        y: 0,
        width: 0,
        height: 0,
        backgroundSize: 0
    };
    $scope.isMouseDown          =   false;
    $rootScope.playlistTracks   =   [];
    $scope.showCurrentPlaylist  =   false;
    $scope.playlistMode         =   false;
    $scope.currentPlaylistTrack =   null;

    $rootScope.playTrack        =   function(trackObj, playlistMode, curPlaylistTrck){
        $scope.playlistMode     =   typeof playlistMode !== 'undefined' ? playlistMode : false;
        $scope.currentPlaylistTrack =   curPlaylistTrck;

        if(!$scope.playlistMode){
            $rootScope.playlistTracks   =   [];
        }

        if($scope.currentTrack){
            $scope.currentTrack.destruct();
        }

        $scope.currentTrack = soundManager.createSound({
            url: 'uploads/' + trackObj.upl_dir,
            whileplaying: function() {
                var progressMaxLeft = 100,
                    left = Math.min(progressMaxLeft, Math.max(0, (progressMaxLeft * ($scope.currentTrack.position / $scope.currentTrack.durationEstimate)))) + '%',
                    width = Math.min(100, Math.max(0, (100 * $scope.currentTrack.position / $scope.currentTrack.durationEstimate))) + '%';

                if ($scope.currentTrack.duration) {
                    $(".sm2-progress-ball").css( 'left', left );
                    $(".sm2-progress-bar").css( 'width', width );
                    $scope.$apply(function(){
                        $scope.currentTime = $scope.getTime($scope.currentTrack.position, true);
                    });
                }
            },
            whileloading: function() {
                $scope.$apply(function(){
                    $scope.trackDuration = $scope.getTime($scope.currentTrack.durationEstimate, true);
                });
            },
            onload: function() {
                $scope.$apply(function(){
                    $scope.trackDuration = $scope.getTime($scope.currentTrack.duration, true);
                });
            },
            onfinish: function() {
                $(".sm2-progress-ball").css( 'left', "0%" );
                $(".sm2-progress-bar").css( 'width', "0%" );

                $scope.isPlaying    =   false;
                $scope.currentTime          =   "0:00";

                $http({
                    method: 'POST',
                    url: 'actions/update-play-count.php',
                    data: trackObj
                });

                if(!$scope.playlistMode){
                    return null;
                }

                $scope.currentPlaylistTrack++;

                if(!$rootScope.playlistTracks[$scope.currentPlaylistTrack]){
                    return null;
                }

                $rootScope.playTrack(
                    $rootScope.playlistTracks[$scope.currentPlaylistTrack],
                    true, $scope.currentPlaylistTrack
                );
            },
            onbufferchange: function(isBuffering) {
                if (isBuffering) {
                    $(".icon-overlay").show();
                } else {
                    $(".icon-overlay").hide();
                }
            }
        });
        $scope.currentTrack.play();
        $scope.isPlaying            =   true;
        $scope.artist               =   trackObj.username;
        $scope.song_title           =   trackObj.title;
    };

    $scope.getTime              =   function(msec, useString){
        // convert milliseconds to hh:mm:ss, return as object literal or string
        var nSec = Math.floor(msec/1000),
            hh = Math.floor(nSec/3600),
            min = Math.floor(nSec/60) - Math.floor(hh * 60),
            sec = Math.floor(nSec -(hh*3600) -(min*60));

        // if (min === 0 && sec === 0) return null; // return 0:00 as null

        return (useString ? ((hh ? hh + ':' : '') + (hh && min < 10 ? '0' + min : min) + ':' + ( sec < 10 ? '0' + sec : sec ) ) : { 'min': min, 'sec': sec });
    };

    $scope.togglePlay           =   function(){
        if($scope.currentTrack){
            $scope.currentTrack.togglePause();
            $scope.isPlaying            =   !$scope.isPlaying;
        }
    };

    $scope.checkVolume          =   function(evt){
        // Volume button is clicked. Update volume element values.
        var target                  =   target = evt.target || evt.srcElement;
        $scope.isMouseDown          =   true;

        $scope.volumeElem.x         =   $(target).offset().left;
        $scope.volumeElem.y         =   0;
        $scope.volumeElem.width     =   target.offsetWidth;
        $scope.volumeElem.height    =   target.offsetHeight;

        // potentially dangerous: this should, but may not be a percentage-based value.
        if($(evt.target).hasClass("sm2-volume-control")){
            $scope.volumeElem.backgroundSize = parseInt($(target).css('background-size'), 10);
        }else{
            $scope.volumeElem.backgroundSize = parseInt($(target).find("a").css('background-size'), 10);
        }

        // IE gives pixels even if background-size specified as % in CSS. Boourns.
        if (window.navigator.userAgent.match(/msie|trident/i)) {
            $scope.volumeElem.backgroundSize = ($scope.volumeElem.backgroundSize / $scope.volumeElem.width) * 100;
        }

        $scope.adjustVolume(evt);
    };

    $scope.adjustVolume         =   function(evt){
        var backgroundSize,
            backgroundMargin,
            pixelMargin,
            value,
            volume;

        // based on getStyle() result
        backgroundSize  =   $scope.volumeElem.backgroundSize;

        // figure out spacing around background image based on background size, eg. 60% background size.
        backgroundSize  =   100 - backgroundSize;
        // 60% wide means 20% margin on each side.
        backgroundMargin = backgroundSize / 2;

        // relative position of mouse over element
        value = Math.max(0, Math.min(1, (evt.clientX - $scope.volumeElem.x) / $scope.volumeElem.width));

        if($(evt.target).hasClass("sm2-volume-control")){
            $(evt.target).css(
                'clip',
                'rect(0px, ' + ($scope.volumeElem.width * value) + 'px, ' + $scope.volumeElem.height + 'px, ' + ($scope.volumeElem.width * (backgroundMargin/100)) + 'px)'
            );
        }else{
            $(evt.target).find(".sm2-volume-control").css(
                'clip',
                'rect(0px, ' + ($scope.volumeElem.width * value) + 'px, ' + $scope.volumeElem.height + 'px, ' + ($scope.volumeElem.width * (backgroundMargin/100)) + 'px)'
            );
        }

        // determine logical volume, including background margin
        pixelMargin = ((backgroundMargin/100) * $scope.volumeElem.width);

        volume = Math.max(0, Math.min(1, ((evt.clientX - $scope.volumeElem.x) - pixelMargin) / ($scope.volumeElem.width - (pixelMargin*2))));

        // set volume
        if ($scope.currentTrack) {
            $scope.currentTrack.setVolume(volume * 100);
        }
    };

    $scope.mouseVolumeMove      =   function(evt){
        if($scope.isMouseDown){
            $scope.adjustVolume(evt);
        }
    };

    $scope.previousPlaylistTrack    =   function(){
        if(!$scope.playlistMode){
            return null;
        }

        if(!$rootScope.playlistTracks[$scope.currentPlaylistTrack - 1]){
            return null;
        }

        $scope.currentPlaylistTrack--;

        $rootScope.playTrack(
            $rootScope.playlistTracks[$scope.currentPlaylistTrack],
            true,
            $scope.currentPlaylistTrack
        );
    };

    $scope.nextPlaylistTrack    =   function(){
        if(!$scope.playlistMode){
            return null;
        }

        if(!$rootScope.playlistTracks[$scope.currentPlaylistTrack + 1]){
            return null;
        }

        $scope.currentPlaylistTrack++;

        $rootScope.playTrack(
            $rootScope.playlistTracks[$scope.currentPlaylistTrack],
            true,
            $scope.currentPlaylistTrack
        );
    };

    $scope.moveTrackBall        =   function($evt){
        if(!$scope.currentTrack){
            return null;
        }

        var xPos                =   parseInt($evt.clientX - $(".sm2-progress-track").offset().left);
        var trackWidth          =   parseInt($(".sm2-progress-track").width());
        var newDurationPerc     =   (xPos / trackWidth);
        var newDuractionSecs    =   $scope.currentTrack.duration * newDurationPerc;
        $scope.currentTrack.setPosition(newDuractionSecs);
    };
});