/**
 * Created by jaskokoyn on 4/5/2015.
 */
euphonizeApp.controller("homeController", function($scope, $rootScope, $http){
    $scope.homeTracks           =   [];
    $scope.stopLoading          =   false;

    $scope.getHomeTracks        =   function(){
        if($scope.stopLoading){
            return null;
        }

        $http({
            method: 'POST',
            url: 'actions/get-home-tracks.php',
            data: {
                offset: $scope.homeTracks.length
            }
        }).success(function(data,status,headers,config){
            if(data.tracks.length === 0){
                $scope.stopLoading  =   true;
                toastr.warning("No more tracks to load.");
                return null;
            }

            data.tracks.forEach(function(ele,ind,arr){
                $scope.homeTracks.push(ele);
            });
        });
    };

    $scope.getHomeTracks();

    $scope.getGenre     =   function(gid){
        for(i = 0; i < genresArr.length; i++)
            if(gid == genresArr[i].id) return genresArr[i].genre_name;
        return null;
    };

    $scope.updateTrackLikes =   function(trackObj, $index){
        $http({
            method: 'POST',
            url: 'actions/update-track-likes.php',
            data: trackObj
        }).success(function(data, status, headers, config){
            if(data.status == 1){
                return toastr.error("Unable to like/unlike track. Try again later.", 'Error!');
            }else if(data.status == 2){
                $scope.homeTracks[$index].isOrange  =   false;
                $scope.homeTracks[$index].like_count--;
            }else if(data.status == 3){
                $scope.homeTracks[$index].isOrange  =   true;
                $scope.homeTracks[$index].like_count++;
            }
        });
    };

    if($rootScope.settings.infinite_pagination == 1){
        $(window).scroll(function(){
            var scrollPos       =   $(document).scrollTop();
            var docHeight       =   $('body').prop('scrollHeight') - 600;
            var difference      =   docHeight - scrollPos;

            console.log(scrollPos, docHeight, difference);
            if(difference < 300){
                $scope.getHomeTracks();
            }
        });
    }
});