/**
 * Created by jaskokoyn on 3/14/2015.
 */
euphonizeApp.controller( "trackController", function($scope, $rootScope, $http, $stateParams, trackObj){
    $scope.track                =   trackObj.track;
    $scope.comment              =   null;
    $scope.isProcessingComment  =   false;
    $scope.comments             =  trackObj.comments;

    $scope.submitComment        =   function(isValid){
        if(!isValid){
            toastr.warning( "Unable to submit comment. Please make sure your comment is between 5 - 500 characters long.", "Uh Oh!");
            return null;
        }

        $scope.isProcessingComment  =   true;

        $http({
            method: 'POST',
            url: 'actions/submit-comment.php',
            data: {
                comment: $scope.comment,
                tid: $stateParams.tid
            }
        }).success(function(data, status, headers, config){
            console.log(data);

            if(data.status == 1){
                toastr.warning( "Unable to submit comment. Please login or make sure your comment is between 5 - 500 characters long.", "Uh Oh!");
                return null;
            }

            $scope.isProcessingComment  =   false;
            $scope.comment              =   null;
            $scope.track.comment_count++;
            $scope.comments.unshift(data.comment);
        });
    };

    $scope.updateTrackLikes     =   function(){
        $http({
            method: 'POST',
            url: 'actions/update-track-likes.php',
            data: $scope.track
        }).success(function(data, status, headers, config){
            if(data.status == 1){
                return toastr.error("Unable to like/unlike track. Try again later.", 'Error!');
            }else if(data.status == 2){
                $scope.track.isOrange  =   false;
                $scope.track.like_count--;
            }else if(data.status == 3){
                $scope.track.isOrange  =   true;
                $scope.track.like_count++;
            }
        });
    };

    $scope.reportComment        =   function(commentObj){
        toastr.info("Reporting Comment", "Please wait");
        $http({
            method: 'POST',
            url: 'actions/report-comment.php',
            data: commentObj
        }).success(function(data,status,headers,config){
            toastr.success("This comment has been reported", "Success");
        });
    };

    $scope.loadComments         =   function(){
        $http({
            method: 'POST',
            url: 'actions/load-track-comments.php',
            data: {
                tid: $stateParams.tid,
                offset: $scope.comments.length
            }
        }).success(function(data,status,headers,config){
            if(data.comments.length === 0){
                toastr.warning("No more comments to load.");
                return null;
            }
            data.comments.forEach(function(ele,ind,arr){
                $scope.comments.push(ele);
            });
        });
    };
});