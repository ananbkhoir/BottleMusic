/**
 * Created by jaskokoyn on 3/17/2015.
 */
euphonizeApp.controller("profileFollowersController", function($scope, $rootScope, $http, $stateParams, followingObj){
    $scope.following                =   followingObj.following;

    $scope.loadMoreFollowers        =   function(){
        $http({
            method: 'POST',
            url: 'actions/get-user-followers.php',
            data: {
                username: $stateParams.username,
                offset: $scope.following.length
            }
        }).success(function(data,headers,status,config){
            if(data.following.length == 0){
                toastr.warning("No more friends to load.", "Whoa!");
                return null;
            }
            data.following.forEach(function(ele,ind,arr){
               $scope.following.push(ele);
            });
        });
    };
});