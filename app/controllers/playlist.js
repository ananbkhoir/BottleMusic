/**
 * Created by jaskokoyn on 3/19/2015.
 */
euphonizeApp.controller("playlistController", function($scope, $rootScope, $http, playlistObj){
    $scope.playlistObj              =   playlistObj.playlist;
    $scope.tracks                   =   playlistObj.tracks;
    $scope.isOrange                 =   playlistObj.isOrange;

    $scope.toggleLike                   =   function(){
        $http({
            method: 'POST',
            url: 'actions/update-playlist-likes.php',
            data: $scope.playlistObj
        }).success(function(data, status, headers, config){
            console.log(data);
            if(data.status == 1){
                toastr.error("Unable to like/unlike playlist. Try again later.", 'Error!');
                return null;
            }else if(data.status == 2){
                $scope.isOrange  =   false;
            }else if(data.status == 3){
                $scope.isOrange  =   true;
            }
        });
    };

    $scope.getGenre     =   function(gid){
        for(i = 0; i < genresArr.length; i++)
            if(gid == genresArr[i].id) return genresArr[i].genre_name;
        return null;
    };

    $scope.playPlaylist                 =   function(playlist){
        $http({
            method: 'POST',
            url: 'actions/get-playlist-tracks.php',
            data: playlist
        }).success(function(data, status, headers, config){
            $rootScope.playlistTracks   =   data.tracks;
            if(data.tracks[0]){
                $rootScope.playTrack(data.tracks[0], true, 0);
            }
        });
    };
});