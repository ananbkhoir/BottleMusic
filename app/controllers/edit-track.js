/**
 * Created by jaskokoyn on 3/4/2015.
 */
euphonizeApp.controller("editTrackController", function($scope, $filter, $upload, $stateParams, trackObj){
    $("#tagsInput").tagsinput();

    $scope.track        =   trackObj.track;
    $scope.isCC         =   $scope.track.license != 1;
    $scope.editStatus   =   false;
    $scope.editMessage  =   "";
    $scope.editAlert    =   "alert-info";
    $scope.trackImg     =   !$scope.track.track_img ? 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image' : 'uploads/' + $scope.track.track_img;
    $scope.genres       =   genresArr;

    $("#tagsInput").tagsinput('add', $scope.track.tags);

    $scope.updateLicense    =   function(lType){
        $scope.track.license    =   lType;
        $scope.isCC                 =   (lType == 1) ? false : true;
    };

    $scope.updateLicenseExtra   =   function(evt){
        var item    =   evt.currentTarget;

        $(item).toggleClass("license-item-active");

        if( $(item).hasClass("license-item-active") && $(item).data('license-strict') == 1 &&
            $(item).next().hasClass("license-item-active") && $(item).next().data('license-strict') == 1){
            $(item).next().removeClass("license-item-active");
        }

        if( $(item).hasClass("license-item-active") && $(item).data('license-strict') == 1 &&
            $(item).prev().hasClass("license-item-active") && $(item).prev().data('license-strict') == 1){
            $(item).prev().removeClass("license-item-active");
        }
    };

    $scope.clear = function () {
        $scope.trackEdit.release_date = null;
    };

    $scope.open = function($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.opened = true;
    };

    $scope.updateTrack  =   function(){
        $scope.editStatus   =   true;
        $scope.editMessage  =   "Please wait while this track is being updated.";
        $scope.editAlert    =   "alert-info";

        $("#editTrackForm").slideUp('fast', function(){
            var fd              =   new FormData();
            var tags            =   [];
            var licenseExtras   =   [];

            $(".license-group > a").each(function(){
                if($(this).hasClass("license-item-active")){
                    licenseExtras.push( $(this).data('license-extra') );
                }
            });

            fd.append( "tid", $scope.track.id);
            fd.append( "title", $scope.track.title);
            fd.append( "desc", $scope.track.track_desc);
            fd.append( "tags", $("#tagsInput").val());
            fd.append( "purchase_link", $scope.track.purchase_link);
            fd.append( "record_label", $scope.track.record_label);
            fd.append( "release_date", $filter('date')($scope.track.release_date, 'MMMM d yyyy'));
            fd.append( "license", $scope.track.license);
            fd.append( "license_extras", licenseExtras);
            fd.append( "visibility", $scope.track.visibility);
            fd.append( "downloadable", $scope.track.downloadable);
            fd.append( "genre", $scope.track.gid );

            $.ajax({
                url: "actions/update-track.php",
                type: "POST",
                data: fd,
                processData: false,
                contentType: false,
                success: function(response) {
                    console.log(response);
                    var jsonRes         =   JSON.parse(response);
                    $("#editTrackForm").slideDown('fast');
                    $scope.$apply(function(){
                        $scope.editMessage  =   jsonRes.message;

                        if(jsonRes.status == 2){
                            $scope.editAlert            =   "alert-success";
                        }else{
                            $scope.editAlert    =   "alert-warning";
                        }
                    });
                },
                error: function(jqXHR, textStatus, errorMessage) {
                    $scope.$apply(function() {
                        $scope.editMessage  =   "This track could not be updated. Please try again later.";
                        $scope.editAlert    =   "alert-danger";
                    });
                    console.log(jqXHR);
                    console.log(errorMessage); // Optional
                }
            });
        });
    };

    $scope.uploadTrackImg = function (files) {
        if (!files || !files.length) {
            return;
        }
        toastr.info("Your track image is uploading!", 'Please wait!');

        $upload.upload({
            url: 'actions/upload-track-img.php',
            fields: {
                tid: $stateParams.tid
            },
            formDataAppender: function(formData, key, val){
                formData.append('tid',$stateParams.tid);
            },
            file: files[0]
        }).success(function (data, status, headers, config) {
            if(data.status == 2){
                $scope.trackImg =   data.img;
                toastr.success(data.message, 'Success!');
            }else{
                toastr.warning(data.message, 'Uh oh!');
            }
        });
    };
});