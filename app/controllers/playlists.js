/**
 * Created by jaskokoyn on 3/10/2015.
 */
euphonizeApp.controller('playlistsController', function($scope, $rootScope, $http){
    $scope.isSearching          =   false;
    $scope.search               =   {
        term: "",
        sort: 1,
        offset: 0
    };
    $scope.discoverPlaylists    =   [];
    $scope.isLoadingMore        =   false;
    $scope.searchInProgress     =   false;
    $scope.stopWarning          =   false;

    $scope.getDiscoverPlaylists =   function(isSearching, isLoading){
        $scope.isSearching      =   isSearching;
        $scope.isLoading        =   isLoading;
        $scope.searchInProgress =   true;

        if($scope.isSearching){
            $scope.search.offset    =   0;
            $scope.stopWarning          =   false;
        }

        $http({
            method: 'POST',
            url: 'actions/get-discover-playlists.php',
            data: $scope.search
        }).success(function(data,status,headers,config){
            $scope.searchInProgress     =   false;

            if($scope.isLoading){
                data.playlists.forEach(function(ele,ind,arr){
                    $scope.discoverPlaylists.push(ele);
                });
            }else{
                $scope.discoverPlaylists    =   data.playlists;
            }

            $scope.isSearching          =   false;
            $scope.isLoading            =   false;
            $scope.search.offset        =   $scope.discoverPlaylists.length;

            if(data.playlists.length === 0 && !$scope.stopWarning){
                toastr.warning("No more playlists to load", "Whoa!");
                $scope.stopWarning          =   true;
            }
        });
    };

    $scope.toggleLike                   =   function(playlist, $index){
        $http({
            method: 'POST',
            url: 'actions/update-playlist-likes.php',
            data: playlist
        }).success(function(data, status, headers, config){
            console.log(data);
            if(data.status == 1){
                toastr.error("Unable to like/unlike playlist. Try again later.", 'Error!');
                return null;
            }else if(data.status == 2){
                $scope.discoverPlaylists[$index].isOrange  =   false;
                $scope.discoverPlaylists[$index].like_count--;
            }else if(data.status == 3){
                $scope.discoverPlaylists[$index].isOrange  =   true;
                $scope.discoverPlaylists[$index].like_count++;
            }
        });
    };

    $scope.playPlaylist                 =   function(playlist){
        $http({
            method: 'POST',
            url: 'actions/get-playlist-tracks.php',
            data: playlist
        }).success(function(data, status, headers, config){
            $rootScope.playlistTracks   =   data.tracks;
            if(data.tracks[0]){
                $rootScope.playTrack(data.tracks[0], true, 0);
            }
        });
    };

    $scope.getDiscoverPlaylists();

    if($rootScope.settings.infinite_pagination == 1){
        $(window).scroll(function(){
            var scrollPos       =   $(document).scrollTop();
            var docHeight       =   $('body').prop('scrollHeight') - 600;
            var difference      =   docHeight - scrollPos;

            if(difference < 300 && !$scope.searchInProgress){
                $scope.getDiscoverPlaylists(false, true);
            }
        });
    }
});