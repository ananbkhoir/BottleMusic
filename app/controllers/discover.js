/**
 * Created by jaskokoyn on 3/10/2015.
 */
euphonizeApp.controller("discoverController", function($scope, $rootScope, $http){
    $scope.genres               =   genresArr;
    $scope.isSearching          =   false;
    $scope.isLoadingMore        =   false;
    $scope.search               =   {
        term: "",
        genre: 0,
        sort: 1,
        offset: 0
    };
    $scope.discoverTracks       =   [];
    $scope.searchInProgress     =   false;
    $scope.stopWarning          =   false;

    $scope.getGenre     =   function(gid){
        for(i = 0; i < genresArr.length; i++)
            if(gid == genresArr[i].id) return genresArr[i].genre_name;
        return null;
    };

    $scope.getDiscoverTracks    =   function(isSearching, isLoading){
        $scope.isSearching      =   isSearching;
        $scope.isLoading        =   isLoading;
        $scope.searchInProgress =   true;

        if($scope.isSearching){
            $scope.search.offset    =   0;
            $scope.stopWarning          =   false;
        }

        $http({
            method: 'POST',
            url: 'actions/get-discover-tracks.php',
            data: $scope.search
        }).success(function(data,status,headers,config){
            $scope.searchInProgress     =   false;
            if($scope.isLoading){
                data.tracks.forEach(function(ele,ind,arr){
                    $scope.discoverTracks.push(ele);
                });
            }else{
                $scope.discoverTracks    =   data.tracks;
            }

            $scope.isSearching          =   false;
            $scope.isLoading            =   false;
            $scope.search.offset        =   $scope.discoverTracks.length;

            if(data.tracks.length === 0 && !$scope.stopWarning){
                toastr.warning("No more tracks to load", "Whoa!");
                $scope.stopWarning          =   true;
            }
        });
    };

    $scope.updateTrackLikes =   function(trackObj, $index){
        $http({
            method: 'POST',
            url: 'actions/update-track-likes.php',
            data: trackObj
        }).success(function(data, status, headers, config){
            if(data.status == 1){
                return toastr.error("Unable to like/unlike track. Try again later.", 'Error!');
            }else if(data.status == 2){
                $scope.discoverTracks[$index].isOrange  =   false;
                $scope.discoverTracks[$index].like_count--;
            }else if(data.status == 3){
                $scope.discoverTracks[$index].isOrange  =   true;
                $scope.discoverTracks[$index].like_count++;
            }
        });
    };

    $scope.getDiscoverTracks();

    if($rootScope.settings.infinite_pagination == 1){
        $(window).scroll(function(){
            var scrollPos       =   $(document).scrollTop();
            var docHeight       =   $('body').prop('scrollHeight') - 600;
            var difference      =   docHeight - scrollPos;

            if(difference < 300 && !$scope.searchInProgress){
                $scope.getDiscoverTracks(false, true);
            }
        });
    }
});