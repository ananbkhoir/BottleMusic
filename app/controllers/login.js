/**
 * Created by jaskokoyn on 2/26/2015.
 */
euphonizeApp.controller("loginController", function($scope, $http, $state){
    $http({
        method: 'POST',
        url: 'actions/check-login.php'
    }).success(function(data,status,headers,config){
        console.log(data);
        if(data.status == 1){
            $state.go("home");
        }
    });

    $scope.loginUser            =   {
        username:   "",
        pass:      "",
        remember:      false
    };
    $scope.loginAlert           =   "";
    $scope.isSubmitted          =   false;
    $scope.loginStatus          =   "";

    $scope.login =   function(){
        $scope.isSubmitted      =   true;
        $scope.loginStatus      =   "Please wait! We are checking your credentials.";
        $scope.loginAlert       =   "alert-info";

        $("#loginForm").slideUp('fast', function(){
            $http.post('actions/login.php', $scope.loginUser).success(function(data, status, headers, config) {
                // this callback will be called asynchronously
                // when the response is available
                console.log(data);
                $scope.loginStatus    =   data.message;

                if(data.status == 1){
                    $scope.loginAlert         =   "alert-success";
                    location.href             =   "index.php";
                }else{
                    $scope.loginAlert         =   "alert-warning";
                    $("#loginForm").slideDown('fast');
                }
            }).error(function(data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                $scope.loginAlert         =   "alert-danger";
                $scope.loginStatus        =   "Something went wrong. Please try again later.";
                $("#loginForm").slideDown('fast');
                console.log(data);
            });
        });
    };

    $scope.fbLogin              =   function(){
        FB.login(function(fbLoginResponse) {
            if(fbLoginResponse.status === "connected"){
                $scope.isSubmitted      =   true;
                $scope.loginStatus      =   "Please wait while we log you in.";
                $scope.loginAlert       =   'alert-info';

                FB.api('/me', function(fbAPIResponse) {
                    $http({
                        method: 'POST',
                        url: 'actions/users/fb-login.php',
                        data: fbAPIResponse
                    }).then(function(serverRes){
                        if(serverRes.status === 1 ){
                            $scope.isSubmitted      =   false;
                            $scope.loginAlert       =   'alert-danger';
                            $scope.loginStatus      =   "Unable to log you in.";
                            return null;
                        }

                        $scope.loginAlert       =   'alert-success';
                        $scope.loginStatus      =   'You have successfully logged in! You will now be redirected to the home page.';
                        location.href       =   "index.php";
                    });
                });
            }
        }, {scope: 'public_profile,email'});
    };
});