/**
 * Created by jaskokoyn on 3/21/2015.
 */
euphonizeApp.controller("navController", function($scope,$rootScope,$http,$interval){
    $scope.hasMessages              =   false;
    $scope.hasAlerts                =   false;
    $scope.lastAlertID              =   0;
    $scope.alerts                   =   [];

    $rootScope.updateMessageCount   =   function(){
        $http({
            method: 'GET',
            url: 'actions/get-message-check.php'
        }).success(function(data,status,headers,config){
            if(data.messageCount == 0){
                $scope.hasMessages  =   false;
            }else{
                $scope.hasMessages  =   true;
            }
        });
    };

    $interval(function(){
        $rootScope.updateMessageCount();
        $scope.updateAlertCount();
    }, 30000);

    $rootScope.updateMessageCount();

    $scope.goToAdmin                =   function(){
        location.href = 'http://' + $rootScope.settings.site_domain + $rootScope.settings.site_dir + '/admin/index.php'
    };

    $scope.updateAlertCount         =   function(){
        $http({
            method: 'POST',
            url: 'actions/get-alerts.php',
            data: {
                lastAlertId: $scope.lastAlertID
            }
        }).success(function(data,status,headers,config){
            if(data.alerts.length == 0){
                return null;
            }

            $scope.lastAlertID  =   data.last_id;
            $scope.hasAlerts    =   true;

            data.alerts.forEach(function(ele,ind,arr){
                $scope.alerts.push(ele);
            });
        });
    };
    $scope.updateAlertCount();
});