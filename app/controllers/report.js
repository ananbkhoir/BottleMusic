/**
 * Created by jaskokoyn on 3/19/2015.
 */
euphonizeApp.controller("reportController", function($scope, $rootScope, $state, $stateParams, $http, reportObj){
    if(reportObj.status == 1){
        $state.go("home");
        return null;
    }
    $scope.r_type           =   $stateParams.type;
    $scope.reportObj        =   reportObj.track;
    $scope.report           =   {
        desc:   null,
        num:    null,
        email:  null,
        tid:    $stateParams.id
    };

    $scope.submitReport     =   function(){
        $("#reportForm").slideUp('fast', function(){
            $('#reportStatus').html(
                '<div class="alert alert-info">Please wait while your report is processed.</div>'
            );

            $http({
                method: "POST",
                url:    "actions/report-track.php",
                data:   $scope.report
            }).success(function(data,status,headers,config){
                if(data.status == 1){
                    $("#reportForm").slideDown('fast');
                    $("#reportStatus").html(
                        '<div class="alert alert-danger">Your report could not be submitted. Please fill in all fields.</div>'
                    );
                    return null;
                }

                $("#reportStatus").html(
                    '<div class="alert alert-success">Report has been submitted.</div>'
                );
            });
        });

    };
});