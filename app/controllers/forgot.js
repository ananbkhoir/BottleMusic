/**
 * Created by jaskokoyn on 4/7/2015.
 */
euphonizeApp.controller("forgotController", function($rootScope, $scope, $http, $state){
    $http({
        method: 'POST',
        url: 'actions/check-login.php'
    }).success(function(data,status,headers,config){
        console.log(data);
        if(data.status == 1){
            $state.go("home");
        }
    });

    $scope.forgotUsername       =   null;
    $scope.isSubmitted          =   false;
    $scope.forgotStatus         =   null;
    $scope.forgotAlert          =   null;

    $scope.forgotPass           =   function(){
        $scope.isSubmitted      =   true;
        $scope.forgotStatus      =   "Please wait! We are checking your credentials.";
        $scope.forgotAlert       =   "alert-info";

        $("#forgotForm").slideUp('fast', function(){
            $http.post('actions/forgot-pass.php', { username: $scope.forgotUsername }).success(function(data, status, headers, config) {
                console.log(data);
                $scope.forgotStatus    =   data.message;

                if(data.status == 1){
                    $scope.forgotAlert         =   "alert-success";
                }else{
                    $scope.forgotAlert         =   "alert-warning";
                    $("#forgotForm").slideDown('fast');
                }
            }).error(function(data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                $scope.loginAlert         =   "alert-danger";
                $scope.forgotStatus        =   "Something went wrong. Please try again later.";
                $("#forgotForm").slideDown('fast');
                console.log(data);
            });
        });
    };
});