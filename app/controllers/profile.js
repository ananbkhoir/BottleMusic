/**
 * Created by jaskokoyn on 2/26/2015.
 */
euphonizeApp.controller( "profileController", function($scope, $rootScope, $http, $state, profileObj){
    $scope.profileObj   =   profileObj.user;
    $scope.tracks       =   profileObj.tracks;
    $scope.countries    =   countryArr;
    $scope.isProfile    =   false;
    $scope.isProfileF1  =   false;
    $scope.isProfileF2  =   false;
    $scope.isProfileP   =   false;


    if($state.current.name  ==  "profile"){
        $scope.isProfile    =   true;
    }else if($state.current.name  ==  "profile.following"){
        $scope.isProfileF1  =   true;
    }else if($state.current.name  ==  "profile.followers"){
        $scope.isProfileF2  =   true;
    }else if($state.current.name  ==  "profile.playlists"){
        $scope.isProfileP   =   true;
    }

    $scope.getGenre     =   function(gid){
        for(i = 0; i < genresArr.length; i++)
            if(gid == genresArr[i].id) return genresArr[i].genre_name;
        return null;
    };

    $scope.updateTrackLikes =   function(trackObj, $index){
        $http({
            method: 'POST',
            url: 'actions/update-track-likes.php',
            data: trackObj
        }).success(function(data, status, headers, config){
            if(data.status == 1){
                return toastr.error("Unable to like/unlike track. Try again later.", 'Error!');
            }else if(data.status == 2){
                $scope.tracks[$index].isOrange  =   false;
                $scope.tracks[$index].like_count--;
            }else if(data.status == 3){
                $scope.tracks[$index].isOrange  =   true;
                $scope.tracks[$index].like_count++;
            }
        });
    };

    $scope.toggleFollow =   function(){
        $http({
            method: 'POST',
            url: 'actions/toggle-follow.php',
            data: $scope.profileObj
        }).success(function(data, status, headers, config){
            console.log(data);
            if(data.status == 1){
                toastr.error("Unable to follow/unfollow user. Try again later.", 'Error!');
                return null;
            }else if(data.status == 2){
                $scope.profileObj.isOrange  =   false;
            }else if(data.status == 3){
                $scope.profileObj.isOrange  =   true;
            }
        });
    };

    $scope.reportUser   =   function(){
        toastr.info("Currently reporting user.", "Please wait");
        $http({
            method: 'POST',
            url: 'actions/report-user.php',
            data: {
                uid: $scope.profileObj.id
            }
        }).success(function(data,status,headers,config){
            console.log(data);
            toastr.success("User reported! Admins will check your report shortly.", "Success");
        });
    }
});