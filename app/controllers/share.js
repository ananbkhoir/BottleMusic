/**
 * Created by jaskokoyn on 4/3/2015.
 */
euphonizeApp.controller("shareController", function($rootScope, $scope){
    $scope.shareObj                 =   {};
    $rootScope.updateShareObj       =   function(shareObj){
        $scope.shareObj.name        =   shareObj.title;
        $scope.shareObj.url         =   shareObj.track_url;
        $scope.shareObj.id          =   shareObj.id;
        $scope.shareObj.img         =   shareObj.track_img;
        $scope.shareObj.desc        =   shareObj.track_desc;
    };

    $scope.shareFB                  =   function(){
        FB.ui({
            method: 'feed',
            name: $scope.shareObj.name,
            link: 'http://' +  $rootScope.settings.site_domain + '/track/' + $scope.shareObj.url + '-' + $scope.shareObj.id,
            picture: 'http://' +  $rootScope.settings.site_domain + '/uploads/' + $scope.shareObj.img,
            caption: $rootScope.settings.site_name,
            description: $scope.shareObj.desc
        });
    };

    $scope.shareTwitter     =   function(){
        window.open(
            "https://twitter.com/share?url=" + 'http://' +  $rootScope.settings.site_domain + '/track/' + $scope.shareObj.url + '-' + $scope.shareObj.id +
            '&text=' + encodeURI("Check out this track " + $scope.shareObj.name),
            "_blank",
            "width=500, height=300"
        );
    };
});