/**
 * Created by jaskokoyn on 3/21/2015.
 */
euphonizeApp.controller("inboxController", function($scope, $rootScope, $http, $interval, $state, userMessagesObj){
    if(userMessagesObj.status == 1){
        $state.go("home");
        return null;
    }


    $scope.isEmpty                          =   false;
    $scope.isDisabled                       =   false;
    $scope.noMessages                       =   true;
    $scope.isLoading                        =   false;
    $scope.hasMessages                      =   false;
    $scope.message                          =   {
        message: null,
        user: null
    };
    $scope.inboxMessages                    =   [];
    $scope.senders                          =   userMessagesObj.users;
    $scope.chatMessage                      =   null;
    $scope.currentUsername                  =   null;
    $rootScope.messageTimer                 =   null;

    $scope.submitMessage                    =   function(isValid){
        if(!isValid){
            toastr.error( "Please make sure to fill in all required fields.", "Uh oh!" );
            return null;
        }

        $scope.isDisabled                   =   true;

        $http({
            method: 'POST',
            url: 'actions/submit-message.php',
            data: $scope.message
        }).success(function(data,status,headers,config){
            $scope.isDisabled               =   false;
            if(data.status == 1){
                toastr.error("Unable to send message. Try again later.", "Uh Oh!");
                return null;
            }

            $scope.getUserMessages($scope.message.user);
        });
    };

    $scope.getUserMessages                  =   function(username){
        $scope.isEmpty                      =   false;
        $scope.noMessages                   =   false;
        $scope.isLoading                    =   true;

        $http({
            method: 'POST',
            url: 'actions/get-user-messages.php',
            data: {
                username: username
            }
        }).success(function(data,status,headers,config){
            $scope.isLoading                =   false;
            $scope.hasMessages              =   true;
            $scope.inboxMessages            =   data.messages;
            $scope.currentUsername           =   username;
            setTimeout(function(){
                $(".msgCtr").animate({ scrollTop: $('.msgCtr')[0].scrollHeight}, 1000);
            }, 500);
        });
    };

    $scope.sendChatMessage                  =   function(isValid){
        if(!isValid){
            toastr.error( "Please enter a message", "Uh oh!" );
            return null;
        }

        $scope.isDisabled                   =   true;

        $http({
            method: 'POST',
            url: 'actions/send-chat-message.php',
            data: {
                message: $scope.chatMessage,
                username: $scope.currentUsername
            }
        }).success(function(data,status,headers,config){
            $scope.isDisabled               =   false;
            if(data.status == 1){
                toastr.error("Unable to send message. Try again later.", "Uh Oh!");
                return null;
            }
            $scope.chatMessage              =   null;
        });
    };

    $rootScope.messageTimer                 =   $interval(function(){
        if(!$scope.currentUsername || $scope.isEmpty){
            return null;
        }

        $http({
            method: 'POST',
            url: 'actions/get-user-messages.php',
            data: {
                username: $scope.currentUsername
            }
        }).success(function(data,status,headers,config){
            $rootScope.updateMessageCount();
            $scope.inboxMessages            =   data.messages;
        });
    }, 1000);
});