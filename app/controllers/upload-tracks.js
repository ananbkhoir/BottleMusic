/**
 * Created by jaskokoyn on 2/27/2015.
 */
euphonizeApp.controller("uploadTracksController", function($rootScope, $scope, $http, $state){
    $http({
        method: 'POST',
        url: 'actions/check-login.php'
    }).success(function(data,status,headers,config){
        console.log(data);
        if(data.status == 2){
            $state.go("home");
        }
    });

    $scope.tracks       =   [];

    var trackDropzone   =   new Dropzone("#trackDropzone", {
        paramName: 'track',
        url: "actions/upload-track.php",
        acceptedFiles: ".mp3",
        maxFilesize: 15,
        previewsContainer: "#trackPreviewCtr",
        previewTemplate:
            '<div class="dz-preview dz-file-preview">' +
            '<div class="dz-details">' +
            '<strong data-dz-name></strong><span class="dz-size pull-right" data-dz-size></span>' +
            '</div>' +
            '<div class="dz-progress">' +
            '<div class="progress">' +
            '<div class="progress-bar progress-bar-info progress-bar-striped active" data-dz-uploadprogress></div>' +
            '</div>' +
            '</div>' +
            '</div>'
    });

    trackDropzone.on( "error", function(file, errorMessage, xhr){
        $(file.previewElement).remove();
        toastr.error(errorMessage, 'Error!');
        console.log(errorMessage);
        console.log(xhr);
    });

    trackDropzone.on( "success", function(file, res){
        $scope.$apply(function() {
            var resObj  =   JSON.parse(res);
            $(file.previewElement).remove();

            if(resObj.status == 1){
                return toastr.error(resObj.message, 'Error!');
            }
            toastr.success(resObj.message, 'Success!');
            $scope.tracks.push(resObj.track);
        });
    });
});