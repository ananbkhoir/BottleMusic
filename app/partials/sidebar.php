<h2 class="red-header">Follow us</h2>
<ul class="social-media-icons">
    <li class="facebook">
        <a href="//www.facebook.com/{{ settings.facebook }}" target="_blank" ng-show="settings.facebook">
            <i class="fa fa-facebook"></i>
            <small>Like Us</small>
        </a>
    </li>
    <li class="twitter">
        <a href="//twitter.com/{{ settings.twitter }}" target="_blank" ng-show="settings.twitter">
            <i class="fa fa-twitter"></i>
            <small>Follow Us</small>
        </a>
    </li>
    <li class="google">
        <a href="//plus.google.com/{{ settings.gp }}" target="_blank" ng-show="settings.gp">
            <i class="fa fa-google-plus"></i>
            <small>Follow Us</small>
        </a>
    </li>
    <li class="youtube">
        <a href="//youtube.com/user/{{ settings.youtube }}" target="_blank" ng-show="settings.youtube">
            <i class="fa fa-youtube"></i>
            <small>Subscribe</small>
        </a>
    </li>
    <li class="soundcloud">
        <a href="http://pinterest.com/{{ settings.pinterest }}" target="_blank" ng-show="settings.pinterest">
            <i class="fa fa-pinterest-square"></i>
            <small>Follow Us</small>
        </a>
    </li>
    <li class="instagram">
        <a href="//instagram.com/{{ settings.instagram }}" target="_blank" ng-show="settings.instagram">
            <i class="fa fa-instagram"></i>
            <small>Follow Us</small>
        </a>
    </li>
</ul>
<hr>
<div ng-bind-html="settings.sidebar_ad"></div>