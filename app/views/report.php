<div class="container white-bg">
    <div class="row">
        <div class="col-md-12">
            <h1 class="blue-header">Report {{ reportObj.title }}</h1>
            <p>
                Please fill out the form with all details. We will get back to
                you ASAP.
            </p>
            <div id="reportStatus"></div>
            <form ng-submit="submitReport()" name="reportForm" novalidate id="reportForm">
                <div class="form-group">
                    <label>Details</label>
                    <textarea class="form-control" rows="5" ng-model="report.desc"></textarea>
                </div>
                <div class="form-group">
                    <label>Contact E-mail</label>
                    <input type="text" class="form-control" ng-model="report.email">
                </div>
                <div class="form-group">
                    <label>Contact Number</label>
                    <input type="text" class="form-control" ng-model="report.num">
                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" ng-model="ag1"> I have a good faith belief that use of the copyrighted work described above is not authorized by the copyright owner, its agent or the law. *
                    </label>
                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" ng-model="ag2"> I confirm that the information in this notification is accurate. *
                    </label>
                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" ng-model="ag3"> I acknowledge that {{ settings.site_name }} may suspend or disable my account if I make false, malicious or bad faith allegations of copyright infringement, and that I may also be liable for damages (including costs and attorney’s fees) incurred by {{ settings.site_name }}, the alleged infringer, or the copyright owner or its authorized licensee as the result of any such allegations. *
                    </label>
                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" ng-model="ag4"> I confirm that I am the copyright owner or am authorised to act on behalf of the owner of an exclusive right that is allegedly infringed. Where I am making notice pursuant to the United States Digital Millennium Copyright Act U.S.C. 17 §512(c), I make this statement under penalty of perjury. *
                    </label>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-success btn-block"
                            ng-disabled="!ag1 || !ag2 || !ag3 || !ag4">Submit Report</button>
                </div>
            </form>
        </div>
    </div>
</div>