<div class="container white-bg">
    <div class="row">
        <div class="col-md-9">
            <h2 class="blue-header">{{ playlistObj.playlist_name }}</h2>
            <div class="playlistSearchCtr">
                <div class="playlistSearchBtnCtr pull-right">
                    <button type="button" class="btn btn-default"
                            ng-click="toggleLike()">
                        <i class="fa fa-thumbs-o-up" ng-class="{ orange: isOrange != '0'  }"></i>
                    </button>
                    <button type="button" class="btn btn-default"
                            ng-click="playPlaylist(playlistObj)"><i class="fa fa-indent"></i></button>
                    <a href="playlist/{{ playlistObj.id }}" class="btn btn-default"><i class="fa fa-eye"></i></a>
                </div>
                <h4>
                    <i class="glyphicon glyphicon-equalizer"></i>
                    <a href="playlist/{{ playlistObj.id }}">{{ playlistObj.playlist_name }}</a>
                    <span class="playlistSearchTrackCount">({{ tracks.length }} tracks)</span>
                </h4>
                <span class="playlistSearchDetails">Created by <a href="profile/{{ playlistObj.username }}">{{ playlistObj.username }}</a></span>
            </div>
            <div class="trackCtr" ng-repeat="track in tracks">
                <div class="trackImg">
                    <a href="track/{{ track.track_url }}-{{ track.id }}">
                        <img class="img-rounded" ng-src="uploads/{{ track.track_img }}">
                    </a>
                </div>
                <div class="trackInfoCtr">
                    <h3 class="trackTitle">
                        {{ track.title }}<span class="trackDatePosted pull-right">{{ track.release_date }}</span>
                    </h3>
                    <h4 class="trackUser">
                        {{ track.artist }}<span class="pull-right label label-info">{{ getGenre(track.gid) }}</span>
                    </h4>
                    <div class="trackBtns">
                        <button type="button" class="btn btn-default"
                                ng-click="playTrack(track)"><i class="fa fa-play"></i> {{ track.play_count }}</button>
                        </button>
                        <a href="track/{{ track.track_url }}-{{ track.id }}" class="btn btn-default hidden-xs"><i class="fa fa-comments-o"></i> {{ track.comment_count }}</a>
                        <button type="button" class="btn btn-default hidden-xs" data-toggle="modal"
                                data-target="#addPlaylistTrackModal" ng-click="checkTrackPlaylist(track)"><i class="fa fa-plus"></i> Playlist</button>
                        <button type="button" class="btn btn-default hidden-xs"><i class="fa fa-share-square-o"></i> Share</button>
                        <a ng-click="downloadTrack(track.id)" class="btn btn-green pull-right"
                           style="margin-left: 5px;" ng-show="track.downloadable == '1'"><i class="fa fa-cloud-download"></i></a>
                        <a href="{{ track.purchase_link }}" class="btn btn-blue pull-right" target="_blank"
                           ng-show="track.purchase_link"><i class="fa fa-dollar"></i> Buy</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <ng-include src="'app/partials/sidebar.php'"></ng-include>
        </div>
    </div>
</div>