<div class="container white-bg">
    <h1 class="green-header">
        Inbox
        <button type="button" class="btn btn-success pull-right"
                ng-click="isEmpty = true"><i class="fa fa-pencil"></i> Compose</button>
    </h1>
    <div class="row">
        <div class="col-md-3">
            <table class="table table-hover inbox">
                <tbody>
                <tr ng-repeat="sender in senders" ng-click="getUserMessages(sender.from_user)">
                    <td><strong>{{ sender.from_user }}</strong></td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="col-md-9">
            <div class="msgCtr" ng-hide="isEmpty" scroll-glue>
                <div class="emptyMessage" ng-show="noMessages">
                    You currently don't have a message selected. Click a
                    message to the left to view it.
                </div>
                <div class="text-center" ng-show="isLoading"><i class="fa fa-spin fa-spinner fa-5x"></i></div>
                <div ng-show="hasMessages">
                    <div class="userMessageCtr" ng-repeat="m in inboxMessages">
                        <div class="userMessageUsername">{{ m.from_user }}</div>
                        <p>{{ m.message }}</p>
                    </div>
                </div>
            </div>
            <form name="sendMessageForm" ng-submit="sendChatMessage(sendMessageForm.$valid)" ng-show="hasMessages">
                <div class="input-group">
                    <input type="text" class="form-control" ng-model="chatMessage" required
                           ng-disabled="isDisabled">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="submit" ng-disabled="isDisabled">Go!</button>
                    </span>
                </div><!-- /input-group -->
            </form>
            <div ng-show="isEmpty">
                <h3>Compose Message</h3>
                <form name="composeMessageForm" ng-submit="submitMessage(composeMessageForm.$valid)" novalidate>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Username" required
                                ng-model="message.user" ng-disabled="isDisabled">
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" rows="4" placeholder="Message" required
                                  ng-minlength="1" ng-maxlength="500" ng-model="message.message"
                                  ng-disabled="isDisabled"></textarea>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-block btn-success" ng-disabled="isDisabled"><i class="fa fa-pencil"></i> Send Message</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>