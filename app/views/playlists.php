<div class="container white-bg">
    <div class="row">
        <div class="col-md-3">
            <div class="well">
                <h4>Search</h4>
                <form novalidate ng-submit="getDiscoverPlaylists(true, false)">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search for..."
                               ng-model="search.term">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                        </span>
                    </div>
                </form>

                <h4>Sort</h4>
                <!-- menu -->
                <div class="list-group genre-list-group" id="store-menu">
                    <a class="list-group-item" href ng-click="search.sort = 1; getDiscoverPlaylists(true, false)"
                       ng-class="{ active: search.sort === 1 }"><i class="fa fa-angle-right"></i> New</a>
                    <a class="list-group-item" href ng-click="search.sort = 2; getDiscoverPlaylists(true, false)"
                       ng-class="{ active: search.sort === 2 }"><i class="fa fa-angle-right"></i> Popular</a>
                </div>
            </div>
            <hr>
            <div ng-bind-html="settings.sidebar_ad"></div>
        </div>
        <div class="col-md-9">
            <h3 class="blue-header">Playlists</h3>
            <div ng-show="isSearching" class="text-center">
                <i class="fa fa-spinner fa-spin fa-5x"></i>
            </div>
            <div ng-hide="isSearching">
                <div class="playlistSearchCtr" ng-repeat="discoverPlaylist in discoverPlaylists">
                    <div class="playlistSearchBtnCtr pull-right">
                        <button type="button" class="btn btn-default"
                                ng-click="toggleLike(discoverPlaylist, $index)">
                            <i class="fa fa-thumbs-o-up" ng-class="{ orange: discoverPlaylist.isOrange != '0'  }"></i>
                        </button>
                        <button type="button" class="btn btn-default"
                                ng-click="playPlaylist(discoverPlaylist)"><i class="fa fa-indent"></i></button>
                        <a href="playlist/{{ discoverPlaylist.id }}" class="btn btn-default"><i class="fa fa-eye"></i></a>
                    </div>
                    <h4>
                        <i class="glyphicon glyphicon-equalizer"></i>
                        <a href="playlist/{{ discoverPlaylist.id }}">{{ discoverPlaylist.playlist_name }}</a>
                        <span class="playlistSearchTrackCount">({{ discoverPlaylist.track_count }} tracks)</span>
                    </h4>
                    <span class="playlistSearchDetails">
                        Created by <a href="profile/{{ discoverPlaylist.username }}">{{ discoverPlaylist.username }}</a>
                    </span>
                </div>
                <hr>
                <button type="button" class="btn btn-success center-block"
                        ng-click="getDiscoverPlaylists(false, true)">
                    <i class="fa" ng-class="{ 'fa-smile-o': !isLoading, 'fa-spinner': isLoading, 'fa-spin': isLoading }"></i> Load More
                </button>
            </div>
        </div>
    </div>
</div>