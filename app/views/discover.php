<div class="container white-bg">
    <div class="row">
        <div class="col-md-3">
            <div class="well">
                <h4>Search</h4>
                <form novalidate ng-submit="getDiscoverTracks(true, false)">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search for..."
                               ng-model="search.term">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                        </span>
                    </div>
                </form>

                <h4>Genres</h4>
                <div class="list-group genre-list-group" id="store-menu">
                    <a class="list-group-item" href ng-click="search.genre = 0; getDiscoverTracks(true, false)"
                        ng-class="{ active: search.genre === 0 }"><i class="fa fa-angle-right"></i>  All</a>
                    <a class="list-group-item" href ng-repeat="genre in genres"
                       ng-click="search.genre = genre.id; getDiscoverTracks(true, false)"
                       ng-class="{ active: search.genre === genre.id }"><i class="fa fa-angle-right"></i>  {{ genre.genre_name }}</a>
                </div>

                <h4>Sort</h4>
                <div class="list-group genre-list-group" id="store-menu">
                    <a class="list-group-item" href ng-click="search.sort = 1; getDiscoverTracks(true, false)"
                       ng-class="{ active: search.sort === 1 }"><i class="fa fa-angle-right"></i> New</a>
                    <a class="list-group-item" href ng-click="search.sort = 2; getDiscoverTracks(true, false)"
                       ng-class="{ active: search.sort === 2 }"><i class="fa fa-angle-right"></i> Popular</a>
                </div>
            </div>
            <hr>
            <div ng-bind-html="settings.sidebar_ad"></div>
        </div>
        <div class="col-md-9">
            <h3 class="blue-header">Discover</h3>
            <div ng-show="isSearching" class="text-center"><i class="fa fa-spinner fa-spin fa-5x"></i></div>
            <div ng-hide="isSearching">
                <div class="trackCtr" ng-repeat="track in discoverTracks">
                    <div class="trackImg">
                        <a href="track/{{ track.track_url }}-{{ track.id }}">
                            <img class="img-rounded" ng-src="uploads/{{ track.track_img }}">
                        </a>
                    </div>
                    <div class="trackInfoCtr">
                        <h3 class="trackTitle">
                            <a href="track/{{ track.track_url }}-{{ track.id }}">{{ track.title }}</a>
                            <span class="trackDatePosted pull-right">{{ track.release_date }}</span>
                        </h3>
                        <h4 class="trackUser">
                            <a href="profile/{{ track.username }}">{{ track.artist }}</a>
                            <span class="pull-right label label-info">{{ getGenre(track.gid) }}</span>
                        </h4>
                        <div class="trackBtns">
                            <button type="button" class="btn btn-default"
                                    ng-click="playTrack(track)"><i class="fa fa-play"></i> {{ track.play_count }}</button>
                            <button type="button" class="btn btn-default" ng-click="updateTrackLikes(track, $index)">
                                <i class="fa fa-heart" ng-class="{ orange: track.isOrange != '0'  }"></i>
                                {{ track.like_count }}
                            </button>
                            <a href="track/{{ track.track_url }}-{{ track.id }}" class="btn btn-default hidden-xs"><i class="fa fa-comments-o"></i> {{ track.comment_count }}</a>
                            <button type="button" class="btn btn-default hidden-xs" data-toggle="modal"
                                    data-target="#addPlaylistTrackModal" ng-click="checkTrackPlaylist(track)"><i class="fa fa-plus"></i> Playlist</button>
                            <button type="button" class="btn btn-default hidden-xs" data-toggle="modal" data-target="#shareModal"
                                    ng-click="updateShareObj(track)"><i class="fa fa-share-square-o"></i> Share</button>
                            <a ng-click="downloadTrack(track.id)" class="btn btn-green pull-right"
                               style="margin-left: 5px;" ng-show="track.downloadable == '1'"><i class="fa fa-cloud-download"></i></a>
                            <a href="{{ track.purchase_link }}" class="btn btn-blue pull-right" target="_blank"
                               ng-show="track.purchase_link"><i class="fa fa-dollar"></i> Buy</a>
                        </div>
                    </div>
                </div>
                <hr>
                <button type="button" class="btn btn-success center-block"
                        ng-click="getDiscoverTracks(false, true)">
                    <i class="fa" ng-class="{ 'fa-smile-o': !isLoading, 'fa-spinner': isLoading, 'fa-spin': isLoading }"></i> Load More
                </button>
            </div>
        </div>
    </div>
</div>