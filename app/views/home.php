<div class="container white-bg">
    <div class="row">
        <div class="col-md-9">
            <h2 class="blue-header">Latest Activity</h2>
            <div ng-show="homeTracks.length === 0">
                <p class="text-center">
                    <em>
                        Your activity seems to be empty. Try following some users to see what's new.
                    </em>
                </p>
            </div>
            <div class="trackCtr" ng-repeat="track in homeTracks">
                <div class="trackImg">
                    <a href="track/{{ track.track_url }}-{{ track.id }}">
                        <img class="img-rounded" ng-src="uploads/{{ track.track_img }}">
                    </a>
                </div>
                <div class="trackInfoCtr">
                    <h3 class="trackTitle">
                        <a href="track/{{ track.track_url }}-{{ track.id }}">{{ track.title }}</a>
                        <span class="trackDatePosted pull-right">{{ track.release_date }}</span>
                    </h3>
                    <h4 class="trackUser">
                        <a href="profile/{{ track.username }}">{{ track.artist }}</a>
                        <span class="pull-right label label-info">{{ getGenre(track.gid) }}</span>
                    </h4>
                    <div class="trackBtns">
                        <button type="button" class="btn btn-default"
                                ng-click="playTrack(track)"><i class="fa fa-play"></i> {{ track.play_count }}</button>
                        <button type="button" class="btn btn-default" ng-click="updateTrackLikes(track, $index)">
                            <i class="fa fa-heart" ng-class="{ orange: track.isOrange != '0'  }"></i>
                            {{ track.like_count }}
                        </button>
                        <a href="track/{{ track.track_url }}-{{ track.id }}" class="btn btn-default hidden-xs"><i class="fa fa-comments-o"></i> {{ track.comment_count }}</a>
                        <button type="button" class="btn btn-default hidden-xs" data-toggle="modal"
                                data-target="#addPlaylistTrackModal" ng-click="checkTrackPlaylist(track)"><i class="fa fa-plus"></i> Playlist</button>
                        <button type="button" class="btn btn-default hidden-xs" data-toggle="modal" data-target="#shareModal"
                                ng-click="updateShareObj(track)"><i class="fa fa-share-square-o"></i> Share</button>
                        <a ng-click="downloadTrack(track.id)" class="btn btn-green pull-right"
                           style="margin-left: 5px;" ng-show="track.downloadable == '1'"><i class="fa fa-cloud-download"></i></a>
                        <a href="{{ track.purchase_link }}" class="btn btn-blue pull-right" target="_blank"
                           ng-show="track.purchase_link"><i class="fa fa-dollar"></i> Buy</a>
                    </div>
                </div>
            </div>
            <hr>
            <div class="text-center center-block">
                <button type="button" class="btn btn-success"
                        ng-click="getHomeTracks()"><i class="fa fa-star"></i> Load More Tracks</button>
            </div>
        </div>
        <div class="col-md-3">
            <ng-include src="'app/partials/sidebar.php'"></ng-include>
        </div>
    </div>
</div>