<div class="profileBody white-bg">
    <div class="row">
        <div class="col-md-12">
            <h1 class="blue-header">Playlists</h1>
            <div class="playlistSearchCtr" ng-repeat="profilePlaylist in profilePlaylists">
                <div class="playlistSearchBtnCtr pull-right">
                    <button type="button" class="btn btn-default"
                            ng-click="toggleLike(profilePlaylist, $index)">
                        <i class="fa fa-thumbs-o-up" ng-class="{ orange: profilePlaylist.isOrange != '0'  }"></i>
                    </button>
                    <button type="button" class="btn btn-default"
                            ng-click="playPlaylist(profilePlaylist)"><i class="fa fa-indent"></i></button>
                    <a href="playlist/{{ profilePlaylist.id }}" class="btn btn-default"><i class="fa fa-eye"></i></a>
                </div>
                <h4>
                    <i class="glyphicon glyphicon-equalizer"></i>
                    <a href="playlist/{{ profilePlaylist.id }}">{{ profilePlaylist.playlist_name }}</a>
                    <span class="playlistSearchTrackCount">({{ profilePlaylist.track_count }} tracks)</span>
                </h4>
                <span class="playlistSearchDetails">
                    Created by <a href="profile/{{ profileObj.username }}">{{ profileObj.display_name }}</a>
                </span>
            </div>
        </div>
    </div>
</div>