<div class="container white-bg">
    <div class="row">
        <div class="col-md-12">
            <h1 class="text-center" ng-hide="isValidated">
                Please wait while we validate your email <br>
                <i class="fa fa-spinner fa-spin text-primary fa-3x"></i>
            </h1>
            <h1 class="text-center" ng-show="isValidated">
                {{ validMessage }}<br>
                <i class="fa fa-3x"
                    ng-class="{ 'fa-check': isSuccess, 'text-success': isSuccess, 'fa-remove': isError, 'text-danger': isError }"></i>
            </h1>
        </div>
    </div>
</div>