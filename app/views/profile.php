<div class="container">
    <div class="profileHeader" style="background-image: url(uploads/{{ profileObj.header_img}});">
        <div class="profileHeaderOverlay"></div>
        <img class="img-circle" ng-src="uploads/{{ profileObj.profile_img }}">
        <div class="profileHeaderInfo">
            <button type="button" class="btn btn-default btn-follow" ng-click="toggleFollow()">
                <i class="fa fa-paw" ng-class="{ orange: profileObj.isOrange != '0' }"></i>
            </button>
            <button type="button" class="btn btn-default btn-flag" ng-click="reportUser()"><i class="fa fa-flag-checkered"></i></button>
            <h3>
                {{ profileObj.display_name }}
                <i class="fa fa-star pro-icon" ng-show="profileObj.isPro == '2'"></i>
            </h3>
            <p>{{ sanitizeText(profileObj.intro_text) }}</p>
            <div class="profileHeaderBasicDetails">
                <img class="img-responsive" ng-src="assets/main/img/flags/{{ profileObj.location | lowercase}}.png"
                     ng-show="profileObj.location">
                <span ng-show="profileObj.location">{{ countries[profileObj.location] }} &#9679;</span>
                <a href="{{ profileObj.website }}" target="_blank">{{ profileObj.website }}</a>
                <div class="profileSocialIcons">
                    <a href="http://twitter.com/{{ profileObj.twitter }}" ng-show="profileObj.twitter" target="_blank"><i class="fa fa-twitter"></i></a>
                    <a href="http://facebook.com/{{ profileObj.facebook }}" ng-show="profileObj.facebook" target="_blank"><i class="fa fa-facebook"></i></a>
                    <a href="http://plus.google.com/{{ profileObj.gplus }}" ng-show="profileObj.gplus" target="_blank"><i class="fa fa-google-plus"></i></a>
                    <a href="http://vk.com/{{ profileObj.vk }}" ng-show="profileObj.vk" target="_blank"><i class="fa fa-vk"></i></a>
                    <a href="http://soundcloud.com/{{ profileObj.soundcloud }}" ng-show="profileObj.soundcloud" target="_blank"><i class="fa fa-soundcloud"></i></a>
                </div>
            </div>
        </div>
    </div>
    <div class="profileNav">
        <a ui-sref="profile" ng-class="{ active: isProfile }">Overview</a>
        <a ui-sref="profile.following" ng-class="{ active: isProfileF1 }">Following</a>
        <a ui-sref="profile.followers" ng-class="{ active: isProfileF2 }">Followers</a>
        <a ui-sref="profile.playlists" ng-class="{ active: isProfileP }">Playlists</a>
    </div>
    <div ui-view>
        <div class="profileBody white-bg">
            <div class="row">
                <div class="col-md-8">
                    <h2 class="green-header">My Tracks</h2>
                    <hr>
                    <div class="trackCtr" ng-repeat="track in tracks">
                        <div class="trackImg">
                            <a href="track/{{ track.track_url }}-{{ track.id }}">
                                <img class="img-rounded" ng-src="uploads/{{ track.track_img }}">
                            </a>
                        </div>
                        <div class="trackInfoCtr">
                            <h3 class="trackTitle">
                                <a href="track/{{ track.track_url }}-{{ track.id }}">{{ track.title }}</a>
                                <span class="trackDatePosted pull-right">{{ track.release_date }}</span>
                            </h3>
                            <h4 class="trackUser">
                                <a href="profile/{{ track.username }}">{{ track.artist }}</a>
                                <span class="pull-right label label-info">{{ getGenre(track.gid) }}</span>
                            </h4>
                            <div class="trackBtns">
                                <button type="button" class="btn btn-default"
                                        ng-click="playTrack(track)"><i class="fa fa-play"></i> {{ track.play_count }}</button>
                                <button type="button" class="btn btn-default" ng-click="updateTrackLikes(track, $index)">
                                    <i class="fa fa-heart" ng-class="{ orange: track.isOrange != '0'  }"></i>
                                    {{ track.like_count }}
                                </button>
                                <a href="track/{{ track.track_url }}-{{ track.id }}" class="btn btn-default hidden-xs"><i class="fa fa-comments-o"></i> {{ track.comment_count }}</a>
                                <button type="button" class="btn btn-default hidden-xs" data-toggle="modal"
                                        data-target="#addPlaylistTrackModal" ng-click="checkTrackPlaylist(track)"><i class="fa fa-plus"></i> Playlist</button>
                                <button type="button" class="btn btn-default hidden-xs" data-toggle="modal" data-target="#shareModal"
                                        ng-click="updateShareObj(track)"><i class="fa fa-share-square-o"></i> Share</button>
                                <a ng-click="downloadTrack(track.id)" class="btn btn-green pull-right"
                                   style="margin-left: 5px;" ng-show="track.downloadable == '1'"><i class="fa fa-cloud-download"></i></a>
                                <a href="{{ track.purchase_link }}" class="btn btn-blue pull-right" target="_blank"
                                   ng-show="track.purchase_link"><i class="fa fa-dollar"></i> Buy</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <h2 class="blue-header">About</h2>
                    <p>{{ sanitizeText(profileObj.about) }}</p>
                    <div ng-bind-html="settings.sidebar_ad"></div>
                </div>
            </div>
        </div>
    </div>
</div>