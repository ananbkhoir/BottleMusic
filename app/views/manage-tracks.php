<div class="container white-bg">
    <div class="row">
        <div class="col-md-9">
            <h2 class="blue-header">My Tracks</h2>
            <div class="trackOverview" ng-repeat="track in tracks">
                <a href="edit-track/{{track.id}}" class="btn btn-blue pull-right btn-sm">
                    <i class="fa fa-cogs"></i>
                </a>
                <span>{{ track.title }}</span><br>
                <small>{{ track.original_name }}</small>
            </div>
        </div>
        <div class="col-md-3">
            <ng-include src="'app/partials/sidebar.php'"></ng-include>
        </div>
    </div>
</div>