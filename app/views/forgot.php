<div class="container">
    <div class="row">
        <div class="col-md-offset-2 col-md-8 top-push-med">
            <div class="colorful-header"><div></div><div></div><div></div><div></div></div>
            <div class="login-ctr">
                <h1 class="text-center">Forgot Password</h1>
                <p class="text-center">
                    Please enter your username and we will send an e-mail
                    with a new password.
                </p>
                <div ng-show="isSubmitted">
                    <div class="alert text-center {{ forgotAlert }}">
                        <strong>{{ forgotStatus }}</strong>
                    </div>
                </div>
                <form id="forgotForm" name="forgotForm" class="login-form" ng-submit="forgotPass()" novalidate>
                    <div class="form-group">
                        <label>Username</label>
                        <input type="text" class="form-control" placeholder="username" name="username"
                               ng-model="forgotUsername" required>
                        <i class="fa fa-user"></i>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-block btn-red">
                            <i class="fa fa-save"></i> Submit
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>