<div class="container white-bg">
    <div class="row">
        <div class="col-md-9">
            <div class="trackCtr">
                <div class="trackImg text-center">
                    <a href="track/{{ track.track_url }}-{{ track.id }}">
                        <img class="img-rounded" ng-src="uploads/{{ track.track_img }}">
                    </a>
                    <small><a href="report/{{track.id}}" style="color: #424242;">Report Track</a></small>
                </div>
                <div class="trackInfoCtr">
                    <h3 class="trackTitle">
                        {{ track.title }}<span class="trackDatePosted pull-right">{{ track.release_date }}</span>
                    </h3>
                    <h4 class="trackUser">
                        <a href="profile/{{ track.username }}">{{ track.artist }}</a>
                        <span class="pull-right label label-info">{{ getGenre(track.gid) }}</span>
                    </h4>
                    <div class="trackBtns">
                        <button type="button" class="btn btn-default"
                                ng-click="playTrack(track)"><i class="fa fa-play"></i> {{ track.play_count }}</button>
                        <button type="button" class="btn btn-default" ng-click="updateTrackLikes()">
                            <i class="fa fa-heart" ng-class="{ orange: track.isOrange != '0'  }"></i>
                            {{ track.like_count }}
                        </button>
                        <a href="track/{{ track.track_url }}-{{ track.id }}" class="btn btn-default hidden-xs"><i class="fa fa-comments-o"></i> {{ track.comment_count }}</a>
                        <button type="button" class="btn btn-default hidden-xs" data-toggle="modal"
                                data-target="#addPlaylistTrackModal" ng-click="checkTrackPlaylist(track)"><i class="fa fa-plus"></i> Playlist</button>
                        <button type="button" class="btn btn-default hidden-xs" data-toggle="modal" data-target="#shareModal"
                                ng-click="updateShareObj(track)"><i class="fa fa-share-square-o"></i> Share</button>
                        <a ng-click="downloadTrack(track.id)" class="btn btn-green pull-right"
                           style="margin-left: 5px;" ng-show="track.downloadable == '1'"><i class="fa fa-cloud-download"></i></a>
                        <a href="{{ track.purchase_link }}" class="btn btn-blue pull-right" target="_blank"
                           ng-show="track.purchase_link"><i class="fa fa-dollar"></i> Buy</a>
                    </div>
                </div>
            </div>
            <h4>Description</h4>
            <p>{{ track.track_desc }}</p>
            <ul class="list-unstyled">
                <li ng-show="track.record_label"><strong>Record Label</strong>: {{ track.record_label }}</li>
                <li ng-show="track.tags"><strong>Tags</strong>: {{ track.tags }}</li>
            </ul>
            <h3 class="red-header">Comments</h3>
            <hr>
            <div class="singleComment" ng-repeat="comment in comments">
                <div class="singleCommentImg">
                    <img src="uploads/{{ comment.profile_img }}" class="img-circle">
                </div>
                <div class="singleCommentMain">
                    <strong>
                        {{ comment.display_name }}
                        <small><a href="#" ng-click="reportComment(comment)" style="color: #424242;"><i class="fa fa-flag-o"></i></a></small>
                    </strong>
                    <small class="pull-right"><em>{{ (comment.time_created * 1000) | date: 'MMMM d yyyy' }}</em></small>
                    <br>
                    <p>{{ comment.comment }}</p>
                </div>
            </div>
            <hr>
            <div class="text-center center-block">
                <button type="button" class="btn btn-success" ng-click="loadComments()"><i class="fa fa-star"></i> Load More Comments</button>
            </div>
            <h3 class="green-header">Add Comment</h3>
            <form name="commentForm" ng-submit="submitComment(commentForm.$valid)" novalidate>
                <div class="form-group">
                    <textarea class="form-control" ng-model="comment" ng-disabled="isProcessingComment"
                              ng-minlength="5" ng-maxlength="500" required></textarea>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-blue"
                            ng-disabled="isProcessingComment"><i class="fa fa-plus"></i> Submit Comment</button>
                </div>
            </form>
        </div>
        <div class="col-md-3">
            <ng-include src="'app/partials/sidebar.php'"></ng-include>
        </div>
    </div>
</div>