<div class="container white-bg">
    <div class="row">
        <div class="col-md-4">
            <div ng-show="isLoggedIn">
                <h3 class="blue-header">My Account</h3>
                <strong>Status: </strong>
                <span ng-show="isPro">Pro</span>
                <span ng-hide="isPro">Regular</span>
                <br>
                <span ng-show="isPro"><strong>Pro Membership Expires: </strong> {{ expires }}</span>
                <hr>
            </div>
            <h4 class="green-header">Purchase Pro Membership</h4>
            <form action="https://www.paypal.com/cgi-bin/webscr" method="post" ng-show="isLoggedIn">
                <input type="hidden" name="cmd" value="_xclick">
                <input type="hidden" name="custom" ng-value="uid" ng-model="uid">
                <input type="hidden" name="charset" value="utf-8">
                <input type="hidden" name="return" ng-value="'http://' + settings.site_domain + '/pro'">
                <input type="hidden" name="business" ng-value="settings.paypal_email">
                <input type="hidden" name="item_name" ng-value="settings.site_name + ' Pro Membership'">
                <input type="hidden" name="item_number" value="1">
                <input type="hidden" name="currency_code" ng-value="settings.currency_code">
                <div class="form-group">
                    <select class="form-control" name="amount">
                        <option ng-value="settings.monthly_price">Monthly - ${{settings.monthly_price}}</option>
                        <option ng-value="settings.yearly_price">Yearly - ${{settings.yearly_price}}</option>
                    </select>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-block btn-success">
                        <i class="fa fa-usd"></i> Purchase
                    </button>
                </div>
            </form>
            <div ng-hide="isLoggedIn" class="text-center">
                <strong>
                    You must be logged in to purchase pro membership.
                </strong>
            </div>
        </div>
        <div class="col-md-8">
            <table class="table table-striped table-hover pricingTable">
                <thead>
                <tr>
                    <td>Features</td>
                    <td>Free</td>
                    <td>Pro</td>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Storage Size</td>
                    <td>{{ settings.regular_storage_size }} mb</td>
                    <td>{{ settings.pro_storage_size }} mb</td>
                </tr>
                <tr>
                    <td>Track Size</td>
                    <td>{{ settings.regular_track_size }} mb</td>
                    <td>{{ settings.pro_track_size }} mb</td>
                </tr>
                <tr>
                    <td>Badge</td>
                    <td>No</td>
                    <td>Yes</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>