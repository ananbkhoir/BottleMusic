<div class="container">
    <div class="row">
        <div class="col-md-offset-2 col-md-8 top-push-med">
            <div class="colorful-header"><div></div><div></div><div></div><div></div></div>
            <div class="login-ctr">
                <h1 class="text-center">Login</h1>
                <p class="text-center"></p>
                <div ng-show="isSubmitted">
                    <div class="alert text-center {{ loginAlert }}">
                        <strong>{{ loginStatus }}</strong>
                    </div>
                </div>
                <form id="loginForm" name="loginForm" class="login-form" ng-submit="login()" novalidate>
                    <div class="form-group">
                        <label>Username</label>
                        <input type="text" class="form-control" placeholder="username" name="username"
                               ng-model="loginUser.username" required>
                        <i class="fa fa-user"></i>
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" class="form-control" placeholder="password"
                               ng-model="loginUser.pass" required>
                        <i class="fa fa-key"></i>
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox" ng-model="loginUser.remember"> Remember Me</label>
                        <a ui-sref="forgotPassword" class="pull-right">
                            Forgot Password?
                        </a>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-block btn-red">
                            <i class="fa fa-sign-in"></i> Login
                        </button>
                        <button type="button" class="btn btn-facebook btn-block" ng-click="fbLogin()">
                            <i class="fa fa-facebook"></i> Login with Facebook
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>