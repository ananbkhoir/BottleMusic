<div class="container">
    <div class="row">
        <div class="col-md-offset-2 col-md-8 top-push-med">
            <div class="colorful-header"><div></div><div></div><div></div><div></div></div>
            <div class="register-ctr">
                <h1 class="text-center">Register</h1>
                <p class="text-center">
                    By signing up, you'll be able to upload, share, and
                    download music made by various artists!
                </p>
                <div ng-show="isSubmitted">
                    <div class="alert text-center {{ regAlert }}">
                        <strong>{{ regStatus }}</strong>
                    </div>
                </div>

                <form id="registerForm" class="register-form" ng-submit="register()" novalidate>
                    <div class="form-group">
                        <label>Username</label>
                        <input type="text" class="form-control" placeholder="username" name="username"
                               ng-model="regUser.username" required>
                        <i class="fa fa-user"></i>
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" class="form-control" placeholder="password"
                               ng-model="regUser.pass1" required>
                        <i class="fa fa-key"></i>
                    </div>
                    <div class="form-group">
                        <label>Confirm Password</label>
                        <input type="password" class="form-control" placeholder="confirm password"
                               ng-model="regUser.pass2" required>
                        <i class="fa fa-key"></i>
                    </div>
                    <div class="form-group">
                        <label>E-mail</label>
                        <input type="text" class="form-control" placeholder="e-mail address"
                               ng-model="regUser.email" required>
                        <i class="fa fa-at"></i>
                    </div>
                    <div class="form-group" ng-show="settings.captcha_enabled == 1">
                        <div vc-recaptcha key="settings.recaptcha_site_key"
                             on-create="setWidgetId(widgetId)"
                             on-success="setResponse(response)"></div>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" ng-model="regUser.agree"> I agree to the <a href="#" data-toggle="modal" data-target="#termsModal">Terms & Conditions</a>
                        </label>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-block btn-red" ng-disabled="isCaptcha">
                            <i class="fa fa-plus"></i> Create Account
                        </button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>