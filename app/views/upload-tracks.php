<div class="container white-bg">
    <div class="row">
        <div class="col-md-5">
            <h2 class="red-header">Upload Tracks</h2>
            <form class="dropzone" id="trackDropzone">
                <input name="file" type="file" multiple />
                <i class="fa fa-cloud-upload"></i>
            </form>
            <div id="trackPreviewCtr"></div>
        </div>
        <div class="col-md-7">
            <h2 class="blue-header">Manage Tracks</h2>
            <div class="trackOverview" ng-repeat="track in tracks">
                <a href="edit-track/{{track.tid}}" class="btn btn-blue pull-right btn-sm">
                    <i class="fa fa-cogs"></i>
                </a>
                <span>{{ track.title }}</span><br>
                <small>{{ track.original_name }}</small>
            </div>
        </div>
    </div>
</div>