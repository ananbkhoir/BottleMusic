<div class="profileBody white-bg">
    <div class="row">
        <div class="col-md-12">
            <h1 class="green-header">Following</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4 col-md-2" ng-repeat="f in following">
            <a href="profile/{{ f.following }}" class="userFollow">
                <img class="img-responsive" ng-src="uploads/{{ f.img }}">
                <h4 class="text-center">{{ f.following }}</h4>
            </a>
        </div>
    </div>
    <div class="row">
        <hr>
        <button type="button" class="btn btn-success center-block"
                ng-click="loadMoreFollowers()"><i class="fa fa-star"></i> Load More</button>
    </div>
</div>