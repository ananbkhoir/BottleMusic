<div class="container white-bg">
    <div class="row">
        <div class="col-md-8">
            <h2 class="green-header">Edit Track {{ track.title }}</h2>
            <div ng-show="editStatus" class="alert text-center {{editAlert}}">
                <strong>{{editMessage}}</strong>
            </div>
            <form name="editTrackForm" id="editTrackForm" ng-submit="updateTrack()" novalidate
                  style="margin-top:10px;">
                <div class="pho-tabs-wrapper">
                    <!-- Nav tabs -->
                    <ul class="pho-tabs">
                        <li class="active" data-tab-target="#trackGeneralTab">General</li>
                        <li data-tab-target="#trackMetadataTab">Metadata</li>
                        <li data-tab-target="#trackPermissionsTab">Permissions</li>
                    </ul>

                    <div class="pho-tabpanels-ctr">
                        <div id="trackGeneralTab" class="pho-tab-panel active">
                            <div class="form-group">
                                <label>Title</label>
                                <input type="text" class="form-control" ng-model="track.title">
                            </div>
                            <div class="form-group">
                                <label>Description</label>
                                <textarea class="form-control" ng-model="track.track_desc"></textarea>
                            </div>
                            <div class="form-group">
                                <label>Tags</label>
                                <input class="form-control" type="text" id="tagsInput">
                            </div>
                            <div class="form-group">
                                <label>Genre</label>
                                <select class="form-control" ng-model="track.gid">
                                    <option value="0">---</option>
                                    <option ng-repeat="genre in genres" value="{{ genre.id }}"
                                            ng-selected="track.gid == genre.id">{{ genre.genre_name }}</option>
                                </select>
                            </div>
                        </div>
                        <div id="trackMetadataTab" class="pho-tab-panel">
                            <div class="form-group">
                                <label>Purchase Link</label>
                                <input type="text" class="form-control" ng-model="track.purchase_link">
                            </div>
                            <div class="form-group">
                                <label>Record Label</label>
                                <input type="text" class="form-control" ng-model="track.record_label">
                            </div>
                            <div class="form-group">
                                <label>Release Date</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" datepicker-popup="MMMM d yyyy"
                                           is-open="opened" datepicker-options="dateOptions" ng-required="true"
                                           close-text="Close" ng-model="track.release_date">
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default"
                                                ng-click="open($event)"><i class="glyphicon glyphicon-calendar"></i></button>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Licenses</label><br>
                                <label class="radio-inline">
                                    <input type="radio" name="inlineLicensePicker" ng-model="track.license"
                                           value="1" ng-change="updateLicense(1)"> All Rights Reserved
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="inlineLicensePicker" ng-model="track.license"
                                           value="2" ng-change="updateLicense(2)"> Creative Commons
                                </label>
                                <div class="list-group license-group" style="margin-top: 15px;" ng-show="isCC">
                                    <a href class="list-group-item" ng-click="updateLicenseExtra($event)"
                                       data-license-extra="1"
                                        ng-class="{'license-item-active': track.license_extra.indexOf('1') > -1 }">
                                        <h4 class="list-group-item-heading">Attribution</h4>
                                        <p class="list-group-item-text">
                                            Allow others to copy, distribute, display and perform your
                                            copyrighted work but only if they give credit the way you
                                            request.
                                        </p>
                                    </a>
                                    <a href class="list-group-item" ng-click="updateLicenseExtra($event)"
                                       data-license-extra="2"
                                       ng-class="{'license-item-active': track.license_extra.indexOf('2') > -1 }">
                                        <h4 class="list-group-item-heading">Noncommercial</h4>
                                        <p class="list-group-item-text">
                                            Allow others to distribute, display and perform your work - and
                                            dervative works based upon it - but for noncommercial purposes only.
                                        </p>
                                    </a>
                                    <a href class="list-group-item" ng-click="updateLicenseExtra($event)"
                                       data-license-strict="1" data-license-extra="3"
                                       ng-class="{'license-item-active': track.license_extra.indexOf('3') > -1 }">
                                        <h4 class="list-group-item-heading">No Derivative Works</h4>
                                        <p class="list-group-item-text">
                                            Allow others to copy, distribute, display and perform only verbatim
                                            copies of your work, not derivative works based upon it.
                                        </p>
                                    </a>
                                    <a href class="list-group-item" ng-click="updateLicenseExtra($event)"
                                       data-license-strict="1" data-license-extra="4"
                                       ng-class="{'license-item-active': track.license_extra.indexOf('4') > -1 }">
                                        <h4 class="list-group-item-heading">Share Alike</h4>
                                        <p class="list-group-item-text">
                                            Allow others to distribute derivative works only under a license
                                            identical to the license that governs your work.
                                        </p>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div id="trackPermissionsTab" class="pho-tab-panel">
                            <div class="form-group">
                                <label>Visibility</label>
                                <select class="form-control" ng-model="track.visibility">
                                    <option value="1">Enable</option>
                                    <option value="2">Disable</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Downloadable</label>
                                <select class="form-control" ng-model="track.downloadable">
                                    <option value="1">Enable</option>
                                    <option value="2">Disable</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-green"><i class="fa fa-save"></i> Update</button>
            </form>
        </div>
        <div class="col-md-4">
            <h2 class="red-header">Track Image</h2>
            <div class="thumbnail">
                <img ng-src="{{ trackImg }}">
            </div>
            <button class="btn btn-red btn-block" ng-file-select
                    ng-file-change="uploadTrackImg($files)" ng-accept="'.png,.jpg,.jpeg'">Change Track Image</button>
        </div>
    </div>
</div>