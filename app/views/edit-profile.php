<div class="container white-bg">
    <div class="row">
        <div class="col-md-8">
            <h1 class="green-header">Edit Profile</h1>
            <div ng-show="editStatus" class="alert text-center {{editAlert}}">
                <strong>{{editMessage}}</strong>
            </div>
            <form name="editProfileForm" id="editProfileForm" ng-submit="updateProfile()" novalidate
                  style="margin-top:10px;">
                <div class="pho-tabs-wrapper">
                    <ul class="pho-tabs">
                        <li class="active" data-tab-target="#profileGeneralTab">General</li>
                        <li data-tab-target="#profileSocialTab">Social Media</li>
                    </ul>
                    <div class="pho-tabpanels-ctr">
                        <div id="profileGeneralTab" class="pho-tab-panel active">
                            <div class="form-group">
                                <label>Display Name</label>
                                <input type="text" class="form-control" ng-model="profile.display_name">
                            </div>
                            <div class="form-group">
                                <label>Introduction Text</label>
                                <input type="text" class="form-control" ng-model="profile.intro_text">
                            </div>
                            <div class="form-group">
                                <label>Country</label>
                                <select class="form-control" ng-model="profile.location">
                                    <option ng-repeat="(code,country) in countries" value="{{code}}"
                                        ng-selected="profile.location == code">{{country}}</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>About</label>
                                <textarea class="form-control" ng-model="profile.about" rows="5"></textarea>
                            </div>
                        </div>
                        <div id="profileSocialTab" class="pho-tab-panel">
                            <div class="form-group">
                                <label>Website URL</label>
                                <input type="text" class="form-control" ng-model="profile.website">
                            </div>
                            <div class="form-group">
                                <label>Facebook <small><em>(Profile ID)</em></small></label>
                                <input type="text" class="form-control" ng-model="profile.facebook">
                            </div>
                            <div class="form-group">
                                <label>Twitter <small><em>(Profile ID)</em></small></label>
                                <input type="text" class="form-control" ng-model="profile.twitter">
                            </div>
                            <div class="form-group">
                                <label>Google Plus <small><em>(Profile ID)</em></small></label>
                                <input type="text" class="form-control" ng-model="profile.gplus">
                            </div>
                            <div class="form-group">
                                <label>Youtube <small><em>(Profile ID)</em></small></label>
                                <input type="text" class="form-control" ng-model="profile.youtube">
                            </div>
                            <div class="form-group">
                                <label>VK <small><em>(Profile ID)</em></small></label>
                                <input type="text" class="form-control" ng-model="profile.vk">
                            </div>
                            <div class="form-group">
                                <label>Soundcloud <small><em>(Profile ID)</em></small></label>
                                <input type="text" class="form-control" ng-model="profile.soundcloud">
                            </div>
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-green"><i class="fa fa-save"></i> Update</button>
            </form>
            <h1 class="blue-header">Change Password</h1>
            <div ng-show="editPasswordStatus" class="alert text-center {{editPasswordAlert}}">
                <strong>{{editPasswordMessage}}</strong>
            </div>
            <form name="changePasswordForm" id="changePasswordForm" ng-submit="updatePassword()" novalidate
                  style="margin-top: 10px;">
                <div class="form-group">
                    <label>New Password</label>
                    <input type="password" class="form-control" ng-model="pass.newPass1">
                </div>
                <div class="form-group">
                    <label>Confirm New Password</label>
                    <input type="password" class="form-control" ng-model="pass.newPass2">
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-green"><i class="fa fa-save"></i> Update</button>
                </div>
            </form>
        </div>
        <div class="col-md-4">
            <h2 class="red-header">Profile Image</h2>
            <div class="thumbnail">
                <img ng-src="uploads/{{ profileImg }}">
            </div>
            <button class="btn btn-red btn-block" ng-file-select
                    ng-file-change="uploadImg($files, 'profile')" ng-accept="'.png,.jpg,.jpeg'">Change Profile Image</button>
            <hr>
            <h2 class="blue-header">Header Image</h2>
            <div class="thumbnail">
                <img ng-src="uploads/{{ headerImg }}">
            </div>
            <button class="btn btn-red btn-block" ng-file-select
                    ng-file-change="uploadImg($files, 'header')" ng-accept="'.png,.jpg,.jpeg'">Change Header Image</button>
        </div>
    </div>
</div>