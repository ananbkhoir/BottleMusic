/**
 * Created by jaskokoyn on 2/25/2015.
 */
var euphonizeApp    =   angular.module( "euphonizeApp", [ 'ngAnimate', 'ngSanitize', 'ui.router', 'ngProgressLite', 'ui.bootstrap', 'angularFileUpload', 'luegg.directives', 'vcRecaptcha'] );

euphonizeApp.run(function($rootScope, $http, $location, $interval, ngProgressLite) {
    // Playlists
    $rootScope.userPlaylists    =   [];
    $rootScope.downloadTrack    =   function(tid){
        location.href           =   "download.php?id=" + tid;
        return null;
    };
    $rootScope.settings         =   settings;
    $rootScope.sanitizeText     =   function(t){
        return $('<textarea />').html(t).text();
    };

    $http({
        method: 'GET',
        url: 'actions/get-playlists.php'
    }).success(function(data, status, headers, config){
        $rootScope.userPlaylists    =   data.playlists;
    });

    // Get User Data
    $rootScope.$on('$stateChangeStart', function(){
        ngProgressLite. start();
        $("footer").hide();
        if($rootScope.messageTimer){
            $interval.cancel($rootScope.messageTimer);
        }
    });

    $rootScope.$on('$stateChangeSuccess', function(){
        ngProgressLite.done();
        setTimeout(function(){
            $("footer").show();
        }, 1000);
    });

    $rootScope.$on('$stateChangeError',  function(){
        ngProgressLite.done();
    });
});