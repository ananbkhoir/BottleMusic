/**
 * Created by jaskokoyn on 2/25/2015.
 */
euphonizeApp.config(function($locationProvider, $stateProvider, $urlRouterProvider) {
    $locationProvider.html5Mode(true);

    //
    // For any unmatched url, redirect to /state1
    $urlRouterProvider.otherwise("/");
    //
    // Now set up the states
    $stateProvider
        .state('home', {
            url: "/",
            templateUrl: "app/views/home.php",
            controller: "homeController"
        })
        .state('register', {
            url: "/register",
            templateUrl: "app/views/register.php",
            controller: "registerController"
        })
        .state('login', {
            url: "/login",
            templateUrl: "app/views/login.php",
            controller: "loginController"
        })
        .state('logout', {
            url: "/logout",
            controller: function(){
                location.href = "logout.php";
            }
        })
        .state('editProfile', {
            url: "/edit-profile",
            templateUrl: "app/views/edit-profile.php",
            controller: "editProfileController",
            resolve: {
                profileObj: function($http, $stateParams, $state){
                    return $http({
                        method: 'POST',
                        url: 'actions/get-user-all.php',
                        data: $stateParams
                    }).then (function (data) {
                        return data.data;
                    });
                }
            }
        })
        .state('profile', {
            url: "/profile/:username",
            templateUrl: "app/views/profile.php",
            controller: "profileController",
            resolve: {
                profileObj: function($http, $stateParams){
                    return $http({
                        method: 'POST',
                        url: 'actions/get-user.php',
                        data: $stateParams
                    }).then (function (data) {
                        return data.data;
                    });
                }
            }
        })
        .state('profile.following', {
            url: "/following",
            templateUrl: "app/views/profile.following.php",
            controller: "profileFollowingController",
            resolve: {
                followingObj: function($http, $stateParams){
                    return $http({
                        method: 'POST',
                        url: 'actions/get-user-following.php',
                        data: $stateParams
                    }).then (function (data) {
                        return data.data;
                    });
                }
            }
        })
        .state('profile.followers', {
            url: "/followers",
            templateUrl: "app/views/profile.followers.php",
            controller: "profileFollowersController",
            resolve: {
                followingObj: function($http, $stateParams){
                    return $http({
                        method: 'POST',
                        url: 'actions/get-user-followers.php',
                        data: $stateParams
                    }).then (function (data) {
                        return data.data;
                    });
                }
            }
        })
        .state('profile.playlists', {
            url: "/playlists",
            templateUrl: "app/views/profile.playlists.php",
            controller: "profilePlaylistsController",
            resolve: {
                playlistsObj: function($http, $stateParams){
                    return $http({
                        method: 'POST',
                        url: 'actions/get-user-playlists.php',
                        data: $stateParams
                    }).then (function (data) {
                        return data.data;
                    });
                }
            }
        })
        .state('uploadTracks',{
            url: "/upload-tracks",
            templateUrl: "app/views/upload-tracks.php",
            controller: "uploadTracksController"
        })
        .state('editTrack',{
            url: "/edit-track/:tid",
            templateUrl: "app/views/edit-track.php",
            controller: "editTrackController",
            resolve: {
                trackObj: function($state, $http, $stateParams){
                    return $http({
                        method: 'POST',
                        url: 'actions/get-user-track.php',
                        data: $stateParams
                    }).then (function (data) {
                        return data.data;
                    });
                }
            },
            onEnter: function($state, trackObj){
                if(trackObj.status == 1){
                    $state.go("home");
                }
            }
        })
        .state('discover',{
            url: "/discover",
            templateUrl: "app/views/discover.php",
            controller: "discoverController"
        })
        .state('playlists',{
            url: "/playlists",
            templateUrl: "app/views/playlists.php",
            controller: "playlistsController"
        })
        .state('playlist',{
            url: "/playlist/:pid",
            templateUrl: "app/views/playlist.php",
            controller: "playlistController",
            resolve: {
                playlistObj: function($http, $stateParams){
                    return $http({
                        method: 'POST',
                        url: 'actions/get-playlist-info.php',
                        data: $stateParams
                    }).then (function (data) {
                        return data.data;
                    });
                }
            },
            onEnter: function($state, playlistObj){
                if(playlistObj.status == 1){
                    $state.go("home");
                }
            }
        })
        .state('track',{
            url: "/track/:track_name-:tid",
            templateUrl: "app/views/track.php",
            controller: "trackController",
            resolve: {
                trackObj: function($http, $stateParams){
                    return $http({
                        method: 'POST',
                        url: 'actions/get-track.php',
                        data: $stateParams
                    }).then (function (data) {
                        return data.data;
                    });
                }
            },
            onEnter: function($state, trackObj){
                if(trackObj.status == 1){
                    $state.go("home");
                }
            }
        })
        .state('report',{
            url: "/report/:id",
            templateUrl: "app/views/report.php",
            controller: "reportController",
            resolve: {
                reportObj: function($http, $stateParams){
                    return $http({
                        method: 'POST',
                        url: 'actions/get-report-obj.php',
                        data: $stateParams
                    }).then (function (data) {
                        return data.data;
                    });
                }
            }
        })
        .state('inbox',{
            url: "/inbox",
            templateUrl: "app/views/inbox.php",
            controller: "inboxController",
            resolve: {
                userMessagesObj: function($http, $stateParams){
                    return $http({
                        method: 'POST',
                        url: 'actions/get-inbox-messages.php',
                        data: $stateParams
                    }).then (function (data) {
                        return data.data;
                    });
                }
            }
        })
        .state('validate',{
            url: "/validate/:code",
            templateUrl: "app/views/validate.php",
            controller: "validateController"
        })
        .state('manageTracks',{
            url: "/manage-tracks",
            templateUrl: "app/views/manage-tracks.php",
            controller: "manageTracksController"
        })
        .state('pro',{
            url: "/pro",
            templateUrl: "app/views/pro.php",
            controller: "proController"
        })
        .state('forgotPassword',{
            url: "/forgot",
            templateUrl: "app/views/forgot.php",
            controller: "forgotController"
        })
        .state('resetVerify',{
            url: "/reset-verify/:code",
            templateUrl: "app/views/reset-verify.php",
            controller: "resetVerifyController"
        });
});