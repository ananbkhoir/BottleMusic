<?php
/**
 * Created by PhpStorm.
 * User: jaskokoyn
 * Date: 4/29/2015
 * Time: 2:22 PM
 */
require( 'inc/db.php' );
include( 'inc/func.inc.php' );
header('Content-Type: application/json');

$output                 =   array('status' => 1);
if(!isset($_GET['type'])){
    dj($output);
}

$type                   =   intval($_GET['type']);
$term                   =   isset($_GET['term']) ? secure($_GET['term']) : '';

if($type == 1){ // Users
    $searchQuery        =   $db->prepare("
    SELECT `id`,`email`,`username`,`profile_img`,`header_img`,`display_name`,`intro_text`,`location`,`website`,
       `facebook`,`twitter`,`gplus`,`youtube`,`vk`,`soundcloud`,`about`,`isPro`
    FROM users
    WHERE username = :t OR id = :t
    ");
    $searchQuery->execute(array(
        ":t"            =>  $term
    ));

    if($searchQuery->rowCount() === 0){
        dj($output);
    }
    $output['status']   =   2;
    $output['users']    =   $searchQuery->fetchAll(PDO::FETCH_ASSOC);
    dj($output);
}else if($type == 2){ // Track
    $searchQuery        =   $db->prepare("SELECT * FROM tracks WHERE id = :t OR title = :t");
    $searchQuery->execute(array(
        ":t"            =>  $term
    ));

    if($searchQuery->rowCount() === 0){
        dj($output);
    }
    $output['status']   =   2;
    $output['tracks']   =   $searchQuery->fetchAll(PDO::FETCH_ASSOC);
    dj($output);
}else{
    dj($output);
}