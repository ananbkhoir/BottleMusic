<?php
/**
 * Created by PhpStorm.
 * User: jaskokoyn
 * Date: 3/16/2015
 * Time: 10:46 PM
 */

require( 'inc/db.php' );
include( 'inc/func.inc.php' );

if(!isReady($_GET, array('id'))){
    header("location:index.php");
    exit();
}

$id                         =   intval($_GET['id']);
$getDownloadQuery           =   $db->prepare("SELECT * FROM tracks WHERE id = :id AND downloadable='1'");
$getDownloadQuery->execute(array(
    ":id"                   =>  $id
));

if($getDownloadQuery->rowCount() !== 1){
    header("location:index.php");
    exit();
}

$getDownloadRow             =   $getDownloadQuery->fetch(PDO::FETCH_ASSOC);

header('Content-Type: audio/mpeg');
header('Content-Transfer-Encoding: Binary');
header('Content-disposition: attachment; filename="' . basename($getDownloadRow['original_name']) . '"');
readfile("uploads/" . $getDownloadRow['upl_dir']); // do the double-download-dance (dirty but worky)