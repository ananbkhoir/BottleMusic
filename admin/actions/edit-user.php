<?php
/**
 * Created by PhpStorm.
 * User: jaskokoyn
 * Date: 3/27/2015
 * Time: 2:15 PM
 */

require( '../../inc/db.php' );
include( '../../inc/func.inc.php' );

if(!isset($_SESSION['isAdmin']) || $_SESSION['isAdmin'] !== true){
    header("location:../");
    exit();
}

$output             =   array('status' => 1);

if(!isReady($_POST, array('id'))){
    dj($output);
}

$id                 =   intval($_POST['id']);
$email              =   secure($_POST['email']);
$loc                =   secure($_POST['location']);
$display_name       =   secure($_POST['dn']);
$intro              =   secure($_POST['intro']);
$about              =   secure($_POST['about']);
$site               =   secure($_POST['website']);
$fb                 =   secure($_POST['fb']);
$twitter            =   secure($_POST['twitter']);
$gplus              =   secure($_POST['gp']);
$youtube            =   secure($_POST['yt']);
$vk                 =   secure($_POST['vk']);
$soundcloud         =   secure($_POST['soundcloud']);
$isPro              =   intval($_POST['isPro']);

$originalQuery      =   $db->prepare("SELECT `isPro` FROM users WHERE id = :id");
$originalQuery->execute(array(":id" => $id));
$originalRow        =   $originalQuery->fetch();

if($originalRow['isPro'] == 1 && $isPro == 2){
    $expires            =   time() + (86400 * 30);
    $updateProQuery     =   $db->prepare("UPDATE users SET pro_expires = :pe WHERE id = :id");
    $updateProQuery->execute(array(
        ":pe"           =>  $expires,
        ":id"           =>  $id
    ));
}

$updateUserQuery    =   $db->prepare("UPDATE users SET email = :email, location = :loc,
                                                       display_name = :dn, intro_text = :it,
                                                       about = :about, website = :website,
                                                       facebook = :fb, twitter = :tw,
                                                       gplus = :gp, youtube = :yt,
                                                       vk = :vk, soundcloud = :sc,
                                                       isPro = :ip
                                      WHERE id = :id");
$updateUserQuery->execute(array(
    ":email"            =>  $email,
    ":loc"              =>  $loc,
    ":dn"               =>  $display_name,
    ":it"               =>  $intro,
    ":about"            =>  $about,
    ":website"          =>  $site,
    ":fb"               =>  $fb,
    ":tw"               =>  $twitter,
    ":gp"               =>  $gplus,
    ":yt"               =>  $youtube,
    ":vk"               =>  $vk,
    ":sc"               =>  $soundcloud,
    ":ip"               =>  $isPro,
    ":id"               =>  $id
));
$output['status']       =   2;
dj($output);