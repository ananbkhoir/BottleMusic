<?php
/**
 * Created by PhpStorm.
 * User: jaskokoyn
 * Date: 3/31/2015
 * Time: 11:26 PM
 */

require( '../../inc/db.php' );
include( '../../inc/func.inc.php' );

if(!isset($_SESSION['isAdmin']) || $_SESSION['isAdmin'] !== true){
    header("location:../");
    exit();
}

$output                 =   array('status' => 1);
$footer                 =   $_POST['footer'];
$sidebar                =   $_POST['sidebar'];
$updateSettingsQuery    =   $db->prepare("UPDATE settings SET footer_ad = :fa, sidebar_ad = :sa WHERE id='1'");
$updateSettingsQuery->execute(array(
    ":fa"           =>  $footer,
    ":sa"           =>  $sidebar
));
$output['status']       =   2;
dj($output);