<?php
/**
 * Created by PhpStorm.
 * User: jaskokoyn
 * Date: 3/27/2015
 * Time: 2:52 PM
 */

require( '../../inc/db.php' );
include( '../../inc/func.inc.php' );

if(!isset($_SESSION['isAdmin']) || $_SESSION['isAdmin'] !== true){
    header("location:../");
    exit();
}

$output             =   array();

if(!isReady($_POST, array('uid'))){
    dj($output);
}

$uid                =   intval($_POST['uid']);

$checkUserQuery     =   $db->prepare("SELECT `status` FROM users WHERE id = :id");
$checkUserQuery->execute(array(
    ":id"           =>  $uid
));
$checkUserRow       =   $checkUserQuery->fetch(PDO::FETCH_ASSOC);

if($checkUserRow['status'] == 1){
    $updateUserQuery    =   $db->prepare("UPDATE users SET status='2' WHERE id = :id");
    $updateUserQuery->execute(array(
        ":id"           =>  $uid
    ));
}else{
    $updateUserQuery    =   $db->prepare("UPDATE users SET status='1' WHERE id = :id");
    $updateUserQuery->execute(array(
        ":id"           =>  $uid
    ));
}

dj($output);