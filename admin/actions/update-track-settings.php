<?php
/**
 * Created by PhpStorm.
 * User: jaskokoyn
 * Date: 3/30/2015
 * Time: 10:57 PM
 */

require( '../../inc/db.php' );
include( '../../inc/func.inc.php' );

if(!isset($_SESSION['isAdmin']) || $_SESSION['isAdmin'] !== true){
    header("location:../");
    exit();
}

$output             =   array('status' => 1);

if(!isReady($_POST, array('track_img_size'))){
    dj($output);
}

$track_img_size                 =   intval($_POST['track_img_size']);
$regular_track_size             =   intval($_POST['regular_track_size']);
$pro_track_size                 =   intval($_POST['pro_track_size']);
$regular_storage                =   intval($_POST['regular_storage']);
$pro_storage                    =   intval($_POST['pro_storage']);

$updateSettingsQuery            =   $db->prepare("UPDATE settings SET track_img_size = :tis,
                                                                      regular_track_size = :rts,
                                                                      pro_track_size = :pts,
                                                                      regular_storage_size = :rss,
                                                                      pro_storage_size = :pss
                                                  WHERE id='1'");
$updateSettingsQuery->execute(array(
    ":tis"                          =>  $track_img_size,
    ":rts"                          =>  $regular_track_size,
    ":pts"                          =>  $pro_track_size,
    ":rss"                          =>  $regular_storage,
    ":pss"                          =>  $pro_storage
));

$output['status']   =   2;
dj($output);