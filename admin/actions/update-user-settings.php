<?php
/**
 * Created by PhpStorm.
 * User: jaskokoyn
 * Date: 3/30/2015
 * Time: 10:03 PM
 */

require( '../../inc/db.php' );
include( '../../inc/func.inc.php' );

if(!isset($_SESSION['isAdmin']) || $_SESSION['isAdmin'] !== true){
    header("location:../");
    exit();
}

$output             =   array('status' => 1);

if(!isReady($_POST, array('user_reg'))){
    dj($output);
}

$user_reg               =   intval($_POST['user_reg']);
$user_captcha           =   intval($_POST['user_captcha']);
$site_key               =   secure($_POST['site_key']);
$private_key            =   secure($_POST['private_key']);
$validate_email         =   intval($_POST['validate_email']);
$profile_size           =   intval($_POST['profile_size']);
$header_size            =   intval($_POST['header_size']);
$friend_count           =   intval($_POST['friend_count']);

$updateSettingsQuery    =   $db->prepare("UPDATE settings SET user_registration = :ur,
                                                              captcha_enabled = :ce,
                                                              recaptcha_site_key = :rsk,
                                                              recaptcha_private_key = :rpk,
                                                              validate_email = :ve,
                                                              profile_img_size = :pis,
                                                              profile_header_size = :phs,
                                                              friend_display_count = :fdc
                                          WHERE id='1'");
$updateSettingsQuery->execute(array(
    ":ur"           =>  $user_reg,
    ":ce"           =>  $user_captcha,
    ":rsk"           =>  $site_key,
    ":rpk"           =>  $private_key,
    ":ve"           =>  $validate_email,
    ":pis"           =>  $profile_size,
    ":phs"           =>  $header_size,
    ":fdc"           =>  $friend_count
));

$output['status']   =   2;
dj($output);