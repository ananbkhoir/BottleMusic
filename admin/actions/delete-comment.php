<?php
/**
 * Created by PhpStorm.
 * User: jaskokoyn
 * Date: 3/28/2015
 * Time: 6:19 PM
 */

require( '../../inc/db.php' );
include( '../../inc/func.inc.php' );

if(!isset($_SESSION['isAdmin']) || $_SESSION['isAdmin'] !== true){
    header("location:../");
    exit();
}

$output             =   array();

if(!isReady($_POST, array('cid', 'rid'))){
    dj($output);
}

$cid                =   intval($_POST['cid']);
$rid                =   intval($_POST['rid']);

$delCommentQuery    =   $db->prepare("DELETE FROM comments WHERE id = :id");
$delCommentQuery->execute(array(":id"           =>  $cid));

$updateReportQuery  =   $db->prepare("UPDATE reports SET status='2' WHERE id = :id");
$updateReportQuery->execute(array('id' => $rid));

dj($output);