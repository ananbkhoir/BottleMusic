<?php
/**
 * Created by PhpStorm.
 * User: jaskokoyn
 * Date: 3/31/2015
 * Time: 1:55 PM
 */

require( '../../inc/db.php' );
include( '../../inc/func.inc.php' );

if(!isset($_SESSION['isAdmin']) || $_SESSION['isAdmin'] !== true){
    header("location:../");
    exit();
}

$output                         =   array('status' => 1);
$username                       =   secure($_POST['username']);
$fb                             =   secure($_POST['fb']);
$yt                             =   secure($_POST['yt']);
$instagram                      =   secure($_POST['instagram']);
$gp                             =   secure($_POST['gp']);
$pinterest                      =   secure($_POST['pinterest']);
$app_key                        =   secure($_POST['app_key']);

$updateSettingsQuery            =   $db->prepare("UPDATE settings SET twitter_username = :tu,
                                                                      facebook = :fb,
                                                                      twitter = :tu,
                                                                      youtube = :yt,
                                                                      instagram = :instagram,
                                                                      gp = :gp,
                                                                      pinterest = :pin,
                                                                      facebook_app_key = :ak
                                                  WHERE id='1'");
$updateSettingsQuery->execute(array(
    ":tu"                          =>  $username,
    ":fb"                          =>  $fb,
    ":yt"                          =>  $yt,
    ":instagram"                   =>  $instagram,
    ":gp"                          =>  $gp,
    ":pin"                         =>  $pinterest,
    ":ak"                          =>  $app_key
));

$output['status']   =   2;
dj($output);