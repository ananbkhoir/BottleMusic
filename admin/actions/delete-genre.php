<?php
/**
 * Created by PhpStorm.
 * User: jaskokoyn
 * Date: 3/28/2015
 * Time: 9:33 PM
 */

require( '../../inc/db.php' );
include( '../../inc/func.inc.php' );

if(!isset($_SESSION['isAdmin']) || $_SESSION['isAdmin'] !== true){
    header("location:../");
    exit();
}

$output             =   array();

if(!isReady($_POST, array('gid'))){
    dj($output);
}

$gid                =   intval($_POST['gid']);
$delTrackQuery      =   $db->prepare("DELETE FROM genres WHERE id = :id");
$delTrackQuery->execute(array(":id"           =>  $gid));

dj($output);