<?php
/**
 * Created by PhpStorm.
 * User: jaskokoyn
 * Date: 3/30/2015
 * Time: 11:17 PM
 */

require( '../../inc/db.php' );
include( '../../inc/func.inc.php' );

if(!isset($_SESSION['isAdmin']) || $_SESSION['isAdmin'] !== true){
    header("location:../");
    exit();
}

$output             =   array('status' => 1);

if(!isReady($_POST, array('pro_accounts'))){
    dj($output);
}

$pro_accounts                   =   intval($_POST['pro_accounts']);
$paypal_email                   =   secure($_POST['paypal_email']);
$monthly_price                  =   secure($_POST['monthly_price']);
$yearly_price                   =   secure($_POST['yearly_price']);
$currency                       =   secure($_POST['currency']);

$updateSettingsQuery            =   $db->prepare("UPDATE settings SET pro_accounts = :pa,
                                                                      paypal_email = :pe,
                                                                      monthly_price = :mp,
                                                                      yearly_price = :yp,
                                                                      currency_code = :cur
                                                  WHERE id='1'");
$updateSettingsQuery->execute(array(
    ":pa"                          =>  $pro_accounts,
    ":pe"                          =>  $paypal_email,
    ":mp"                          =>  $monthly_price,
    ":yp"                          =>  $yearly_price,
    ":cur"                         =>  $currency
));

$output['status']   =   2;
dj($output);