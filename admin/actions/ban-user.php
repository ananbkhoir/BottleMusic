<?php
/**
 * Created by PhpStorm.
 * User: jaskokoyn
 * Date: 4/6/2015
 * Time: 3:09 PM
 */

require( '../../inc/db.php' );
include( '../../inc/func.inc.php' );

if(!isset($_SESSION['isAdmin']) || $_SESSION['isAdmin'] !== true){
    header("location:../");
    exit();
}

$output             =   array();

if(!isReady($_POST, array('uid'))){
    dj($output);
}

$uid                =   intval($_POST['uid']);
$rid                =   intval($_POST['rid']);

$updateUserQuery    =   $db->prepare("UPDATE users SET status='2' WHERE id = :id");
$updateUserQuery->execute(array(
    ":id"           =>  $uid
));

$updateReportQuery  =   $db->prepare("UPDATE reports SET status='5' WHERE id = :id");
$updateReportQuery->execute(array('id' => $rid));
dj($output);