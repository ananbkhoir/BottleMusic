<?php
/**
 * Created by PhpStorm.
 * User: jaskokoyn
 * Date: 3/27/2015
 * Time: 4:49 PM
 */

require( '../../inc/db.php' );
include( '../../inc/func.inc.php' );

if(!isset($_SESSION['isAdmin']) || $_SESSION['isAdmin'] !== true){
    header("location:../");
    exit();
}

$output             =   array();

if(!isReady($_POST, array('tid'))){
    dj($output);
}

$tid                =   intval($_POST['tid']);

$checkTrackQuery    =   $db->prepare("SELECT `status` FROM tracks WHERE id = :id");
$checkTrackQuery->execute(array(
    ":id"           =>  $tid
));
$checkTrackRow      =   $checkTrackQuery->fetch(PDO::FETCH_ASSOC);

if($checkTrackRow['status'] == 1){
    $updateTrackQuery    =   $db->prepare("UPDATE tracks SET status='2' WHERE id = :id");
    $updateTrackQuery->execute(array(
        ":id"           =>  $tid
    ));
}else{
    $updateTrackQuery    =   $db->prepare("UPDATE tracks SET status='1' WHERE id = :id");
    $updateTrackQuery->execute(array(
        ":id"           =>  $tid
    ));
}

if(isset($_POST['rid'])){
    $rid                =   intval($_POST['rid']);
    $updateReportQuery  =   $db->prepare("UPDATE reports SET status='4' WHERE id = :id");
    $updateReportQuery->execute(array('id' => $rid));
}

dj($output);