<?php
/**
 * Created by PhpStorm.
 * User: jaskokoyn
 * Date: 3/28/2015
 * Time: 7:11 PM
 */

require( '../../inc/db.php' );
include( '../../inc/func.inc.php' );

if(!isset($_SESSION['isAdmin']) || $_SESSION['isAdmin'] !== true){
    header("location:../");
    exit();
}

$output             =   array('status' => 1);

if(!isReady($_POST, array('name', 'pos'))){
    dj($output);
}

$pos                =   intval($_POST['pos']);
$name               =   secure($_POST['name']);

$insertGenreQuery   =   $db->prepare("INSERT INTO genres(genre_name,pos) VALUES( :gn, :pos )");
$insertGenreQuery->execute(array(
    ":gn"           =>  $name,
    ":pos"          =>  $pos
));

$output['status']   =   2;
dj($output);