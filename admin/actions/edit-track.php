<?php
/**
 * Created by PhpStorm.
 * User: jaskokoyn
 * Date: 3/27/2015
 * Time: 7:46 PM
 */
require( '../../inc/db.php' );
include( '../../inc/func.inc.php' );

if(!isset($_SESSION['isAdmin']) || $_SESSION['isAdmin'] !== true){
    header("location:../");
    exit();
}

$output             =   array('status' => 1);

if(!isReady($_POST, array('id'))){
    dj($output);
}

$id                 =   intval($_POST['id']);
$title              =   secure($_POST['title']);
$desc               =   secure($_POST['desc']);
$tags               =   secure($_POST['tags']);
$purchase_link      =   secure($_POST['pl']);
$release_date       =   secure($_POST['rd']);
$visible            =   intval($_POST['visible']);
$downloadable       =   intval($_POST['downloadable']);
$gid                =   intval($_POST['gid']);

$updateTrackQuery   =   $db->prepare("UPDATE tracks SET title = :title, track_desc = :track_desc,
                                                        tags = :tags, purchase_link = :pl,
                                                        release_date = :rd, visibility = :visib,
                                                        downloadable = :dl, gid = :gid
                                      WHERE id = :id");
$updateTrackQuery->execute(array(
    ":id"               =>  $id,
    ":title"            =>  $title,
    ":track_desc"       =>  $desc,
    ":tags"             =>  $tags,
    ":pl"               =>  $purchase_link,
    ":rd"               =>  $release_date,
    ":visib"            =>  $visible,
    ":dl"               =>  $downloadable,
    ":gid"              =>  $gid
));
$output['status']       =   2;
dj($output);