<?php
/**
 * Created by PhpStorm.
 * User: jaskokoyn
 * Date: 3/28/2015
 * Time: 6:17 PM
 */

require( '../../inc/db.php' );
include( '../../inc/func.inc.php' );

if(!isset($_SESSION['isAdmin']) || $_SESSION['isAdmin'] !== true){
    header("location:../");
    exit();
}

$output             =   array();

if(!isReady($_POST, array('tid'))){
    dj($output);
}

$tid                =   intval($_POST['tid']);

$delTrackQuery    =   $db->prepare("DELETE FROM tracks WHERE id = :id");
$delTrackQuery->execute(array(":id"           =>  $tid));

if(isset($_POST['rid'])){
    $rid                =   intval($_POST['rid']);
    $updateReportQuery  =   $db->prepare("UPDATE reports SET status='3' WHERE id = :id");
    $updateReportQuery->execute(array('id' => $rid));
}

dj($output);