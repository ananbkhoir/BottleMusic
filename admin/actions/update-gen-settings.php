<?php
/**
 * Created by PhpStorm.
 * User: jaskokoyn
 * Date: 3/30/2015
 * Time: 9:30 PM
 */

require( '../../inc/db.php' );
include( '../../inc/func.inc.php' );

if(!isset($_SESSION['isAdmin']) || $_SESSION['isAdmin'] !== true){
    header("location:../");
    exit();
}

$output             =   array('status' => 1);

if(!isReady($_POST, array('name'))){
    dj($output);
}

$name               =   secure($_POST['name']);
$desc               =   secure($_POST['desc']);
$count              =   intval($_POST['count']);
$pagination         =   intval($_POST['pagination']);
$footer             =   secure($_POST['footer']);

$updateSettingsQuery    =   $db->prepare("UPDATE settings SET site_name = :sn, site_desc = :sd, search_count = :sc, footer_text = :ft, infinite_pagination = :ip WHERE id='1'");
$updateSettingsQuery->execute(array(
    ":sn"           =>  $name,
    ":sd"           =>  $desc,
    ":sc"           =>  $count,
    ":ip"           =>  $pagination,
    ":ft"           =>  $footer
));

$output['status']   =   2;
dj($output);