<?php
/**
 * Created by PhpStorm.
 * User: jaskokoyn
 * Date: 3/28/2015
 * Time: 9:57 PM
 */

require( '../../inc/db.php' );
include( '../../inc/func.inc.php' );

if(!isset($_SESSION['isAdmin']) || $_SESSION['isAdmin'] !== true){
    header("location:../");
    exit();
}

$output             =   array('status' => 1);

if(!isReady($_POST, array('gid', 'name', 'pos'))){
    dj($output);
}

$gid                =   intval($_POST['gid']);
$pos                =   intval($_POST['pos']);
$name               =   secure($_POST['name']);

$updateGenreQuery   =   $db->prepare("UPDATE genres SET genre_name = :gn, pos = :pos WHERE id = :gid");
$updateGenreQuery->execute(array(
    ":gid"          =>  $gid,
    ":gn"           =>  $name,
    ":pos"          =>  $pos
));

$output['status']   =   2;
dj($output);