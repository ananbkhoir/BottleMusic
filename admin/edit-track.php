<?php
/**
 * Created by PhpStorm.
 * User: jaskokoyn
 * Date: 3/27/2015
 * Time: 4:53 PM
 */

require( '../inc/db.php' );
include( '../inc/func.inc.php' );

if(!isset($_SESSION['isAdmin']) || $_SESSION['isAdmin'] !== true){
    header("location:../");
    exit();
}

if(!isset($_GET['id'])){
    header("location:manage-tracks.php");
    exit();
}

$id                 =   intval($_GET['id']);
$getQuery           =   $db->prepare("SELECT * FROM tracks WHERE id = :id");
$getQuery->execute(array(
    ":id"           =>  $id
));

if($getQuery->rowCount() === 0){
    header("location:manage-tracks.php");
    exit();
}

$getRow             =   $getQuery->fetch();

$genresQuery        =   $db->prepare("SELECT * FROM genres");
$genresQuery->execute();
$genres             =   $genresQuery->fetchAll(PDO::FETCH_ASSOC);

include('inc/header.inc.php');
?>
<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <h3><i class="fa fa-angle-right"></i> Edit Track - <?php echo $getRow['title']; ?></h3>
        <div class="row mt">
            <div class="col-lg-12">
                <div class="form-panel">
                    <div id="editTrackStatus"></div>
                    <hr>
                    <form id="editTrackForm">
                        <input type="hidden" id="inputID" value="<?php echo $getRow['id']; ?>">
                        <div class="form-group">
                            <label>Title</label>
                            <input type="text" class="form-control" id="inputTitle" value="<?php echo $getRow['title']; ?>">
                        </div>
                        <div class="form-group">
                            <label>Description</label>
                            <textarea class="form-control" id="inputDesc"><?php echo $getRow['track_desc']; ?></textarea>
                        </div>
                        <div class="form-group">
                            <label>Tags</label>
                            <input type="text" class="form-control" id="inputTags" value="<?php echo $getRow['tags']; ?>">
                        </div>
                        <div class="form-group">
                            <label>Purchase Link</label>
                            <input type="text" class="form-control" id="inputPL" value="<?php echo $getRow['purchase_link']; ?>">
                        </div>
                        <div class="form-group">
                            <label>Release Date</label>
                            <textarea class="form-control" id="inputRD"><?php echo $getRow['release_date']; ?></textarea>
                        </div>
                        <div class="form-group">
                            <label>Visibility</label>
                            <select class="form-control" id="inputVisib">
                                <option value="1">Public</option>
                                <option value="2" <?php echo $getRow['visibility'] == 2 ? "SELECTED" : ""; ?>>Private</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Downloadable</label>
                            <select class="form-control" id="inputDL">
                                <option value="1">Public</option>
                                <option value="2" <?php echo $getRow['downloadable'] == 2 ? "SELECTED" : ""; ?>>Private</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Genre</label>
                            <select class="form-control" id="inputGID">
                                <?php
                                foreach($genres as $gk => $gv){
                                    ?>
                                    <option value="<?php echo $gv['id']; ?>" <?php echo $gv['id'] == $getRow['gid'] ? "SELECTED" : ""; ?>>
                                        <?php echo $gv['genre_name']; ?>
                                    </option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Update</button>
                        </div>
                    </form>
                </div>
            </div><!-- /col-lg-9 END SECTION MIDDLE -->
        </div>
    </section>
</section>

<?php
include('inc/footer.inc.php');
?>
<script src="assets/custom/edit-track.js"></script>