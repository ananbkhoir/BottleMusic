<?php
/**
 * Created by PhpStorm.
 * User: jaskokoyn
 * Date: 3/27/2015
 * Time: 9:13 PM
 */

require( '../inc/db.php' );
include( '../inc/func.inc.php' );

if(!isset($_SESSION['isAdmin']) || $_SESSION['isAdmin'] !== true){
    header("location:../");
    exit();
}

$page               =   isset($_GET['p']) ? intval($_GET['p']) : 1;
$offset             =   ($page * 30) - 30;
$getQuery           =   $db->prepare("
SELECT *,(
  SELECT `title` FROM tracks WHERE id = r.tid
) as `track_title`, (
  SELECT `username` FROM users WHERE id = r.uid
) as `username` FROM reports r
ORDER BY `id` DESC LIMIT " . $offset . ", 30
");
$getQuery->execute();

include('inc/header.inc.php');
?>
<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <h3><i class="fa fa-angle-right"></i> Manage Reports</h3>
        <div class="row mt">
            <div class="col-lg-12">
                <div class="form-panel">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <td>ID</td>
                            <td>Track</td>
                            <td>Submitted By</td>
                            <td>Type</td>
                            <td>Actions</td>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        while($getRow   =   $getQuery->fetch(PDO::FETCH_ASSOC)){
                            if($getRow['r_type'] == 1){
                                $type   =   "Track";
                            }else if($getRow['r_type'] == 2){
                                $type   =   "Comment";
                            }else if($getRow['r_type'] == 3){
                                $type   =   "User";
                            }
                            ?>
                            <tr>
                                <td><?php echo $getRow['id']; ?></td>
                                <td><?php echo $getRow['track_title']; ?></td>
                                <td><?php echo $getRow['username']; ?></td>
                                <td><?php echo $type; ?></td>
                                <td>
                                    <a href="view-report.php?id=<?php echo $getRow['id']; ?>" class="btn btn-sm btn-primary"><i class="fa fa-eye"></i></a>
                                </td>
                            </tr>
                        <?php
                        }
                        ?>
                        </tbody>
                    </table>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="btn-group pull-right">
                                <?php
                                if($page > 1){
                                    ?>
                                    <a href="?p=<?php echo ($page-1); ?>" class="btn btn-default"><i class="fa fa-long-arrow-left"></i></a>
                                <?php
                                }
                                ?>
                                <a href="?p=<?php echo ($page+1); ?>" class="btn btn-default"><i class="fa fa-long-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- /col-lg-9 END SECTION MIDDLE -->
        </div>
    </section>
</section>
<?php
include('inc/footer.inc.php');
?>
<script src="assets/custom/manage-tracks.js"></script>