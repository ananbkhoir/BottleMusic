<?php
/**
 * Created by PhpStorm.
 * User: jaskokoyn
 * Date: 3/28/2015
 * Time: 6:48 PM
 */

require( '../inc/db.php' );
include( '../inc/func.inc.php' );

if(!isset($_SESSION['isAdmin']) || $_SESSION['isAdmin'] !== true){
    header("location:../");
    exit();
}

$getQuery           =   $db->prepare("SELECT * FROM genres ORDER BY `pos`");
$getQuery->execute();

include('inc/header.inc.php');
?>
<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <h3><i class="fa fa-angle-right"></i> Genres
            <button type="button" class="btn btn-success pull-right"
                    data-toggle="modal" data-target="#createGenreModal"><i class="fa fa-plus"></i> Add</button></h3>
        <div class="row mt">
            <div class="col-lg-12">
                <div class="form-panel">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <td>ID</td>
                            <td>Name</td>
                            <td>Position</td>
                            <td>Actions</td>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        while($getRow   =   $getQuery->fetch()){
                            ?>
                            <tr>
                                <td><?php echo $getRow['id']; ?></td>
                                <td><?php echo $getRow['genre_name']; ?></td>
                                <td><?php echo $getRow['pos']; ?></td>
                                <td>
                                    <button type="button" class="btn btn-sm btn-primary btnEditGenre"
                                            data-toggle="modal" data-target="#editGenreModal"><i class="fa fa-wrench"></i></button>
                                    <button type="button" class="btn btn-sm btn-danger btnDelGenre"
                                            data-gid="<?php echo $getRow['id']; ?>"><i class="fa fa-remove"></i></button>
                                </td>
                            </tr>
                        <?php
                        }
                        ?>
                        </tbody>
                    </table>

                </div>
            </div><!-- /col-lg-9 END SECTION MIDDLE -->
        </div>
    </section>
</section>
<div class="modal fade" id="createGenreModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Create Genre</h4>
            </div>
            <div class="modal-body">
                <div id="createGenreStatus"></div>
                <form id="createGenreForm">
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" class="form-control" id="inputGenreName">
                    </div>
                    <div class="form-group">
                        <label>Position</label>
                        <input type="number" class="form-control" id="inputGenrePos" value="1">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success"><i class="fa fa-plus"></i> Create</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="editGenreModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit Genre</h4>
            </div>
            <div class="modal-body">
                <div id="editGenreStatus"></div>
                <form id="editGenreForm">
                    <input type="hidden" id="inputEditGenreID">
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" class="form-control" id="inputEditGenreName">
                    </div>
                    <div class="form-group">
                        <label>Position</label>
                        <input type="number" class="form-control" id="inputEditGenrePos">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<?php
include('inc/footer.inc.php');
?>
<script src="assets/custom/genres.js"></script>