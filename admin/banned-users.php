<?php
/**
 * Created by PhpStorm.
 * User: jaskokoyn
 * Date: 3/27/2015
 * Time: 3:26 PM
 */

require( '../inc/db.php' );
include( '../inc/func.inc.php' );

if(!isset($_SESSION['isAdmin']) || $_SESSION['isAdmin'] !== true){
    header("location:../");
    exit();
}

$page               =   isset($_GET['p']) ? intval($_GET['p']) : 1;
$offset             =   ($page * 30) - 30;
$getQuery           =   $db->prepare("SELECT * FROM users WHERE status='2' LIMIT " . $offset . ", 30");
$getQuery->execute();

include('inc/header.inc.php');
?>
<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <h3><i class="fa fa-angle-right"></i> Banned Users</h3>
        <div class="row mt">
            <div class="col-lg-12">
                <div class="form-panel">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <td>ID</td>
                            <td>Username</td>
                            <td>Email</td>
                            <td>IP</td>
                            <td>Actions</td>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        while($getRow   =   $getQuery->fetch(PDO::FETCH_ASSOC)){
                            ?>
                            <tr>
                                <td><?php echo $getRow['id']; ?></td>
                                <td><?php echo $getRow['username']; ?></td>
                                <td><?php echo $getRow['email']; ?></td>
                                <td><?php echo $getRow['ip']; ?></td>
                                <td>
                                    <a href="edit-user.php?id=<?php echo $getRow['id']; ?>" class="btn btn-sm btn-primary"><i class="fa fa-cogs"></i></a>
                                    <button type="button" class="btn btn-sm btn-danger unbanUserBtn"
                                            data-uid="<?php echo $getRow['id']; ?>"><i class="fa fa-ban"></i></button>
                                </td>
                            </tr>
                        <?php
                        }
                        ?>
                        </tbody>
                    </table>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="btn-group pull-right">
                                <?php
                                if($page > 1){
                                    ?>
                                    <a href="?p=<?php echo ($page-1); ?>" class="btn btn-default"><i class="fa fa-long-arrow-left"></i></a>
                                <?php
                                }
                                ?>
                                <a href="?p=<?php echo ($page+1); ?>" class="btn btn-default"><i class="fa fa-long-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- /col-lg-9 END SECTION MIDDLE -->
        </div>
    </section>
</section>
<?php
include('inc/footer.inc.php');
?>
<script src="assets/custom/banned-users.js"></script>