<?php
/**
 * Created by PhpStorm.
 * User: jaskokoyn
 * Date: 3/28/2015
 * Time: 6:20 PM
 */

require( '../inc/db.php' );
include( '../inc/func.inc.php' );

if(!isset($_SESSION['isAdmin']) || $_SESSION['isAdmin'] !== true){
    header("location:../");
    exit();
}

$getQuery           =   $db->prepare("SELECT * FROM settings WHERE id='1'");
$getQuery->execute();
$getRow             =   $getQuery->fetch();

include('inc/header.inc.php');
?>
<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <h3><i class="fa fa-angle-right"></i> Settings</h3>
        <div class="row mt">
            <div class="col-lg-12">
                <div class="form-panel">
                    <div role="tabpanel">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#generalSettingsTab" data-toggle="tab">General</a></li>
                            <li><a href="#userSettingsTab" data-toggle="tab">User</a></li>
                            <li><a href="#trackSettingsTab" data-toggle="tab">Tracks</a></li>
                            <li><a href="#proSettingsTab" data-toggle="tab">Pro</a></li>
                            <li><a href="#socialSettingsTab" data-toggle="tab">Social</a></li>
                            <li><a href="#adSettingsTab" data-toggle="tab">Ads</a></li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="tab-pane active" id="generalSettingsTab">
                                <h3>General Settings</h3>
                                <div id="genSettingsStatus"></div>
                                <form id="genSettingsForm">
                                    <div class="form-group">
                                        <label>Site Name</label>
                                        <input type="text" class="form-control" id="inputSiteName"
                                               value="<?php echo $getRow['site_name']; ?>">
                                    </div>
                                    <div class="form-group">
                                        <label>Site Description</label>
                                        <input type="text" class="form-control" id="inputSiteDesc"
                                               value="<?php echo $getRow['site_desc']; ?>">
                                    </div>
                                    <div class="form-group">
                                        <label>Search Result Count</label>
                                        <input type="number" class="form-control" id="inputSearchCount"
                                               value="<?php echo $getRow['search_count']; ?>">
                                    </div>
                                    <div class="form-group">
                                        <label>Infinite Pagination</label>
                                        <select class="form-control" id="inputInfinitePagination">
                                            <option value="1">Enable</option>
                                            <option value="2" <?php echo $getRow['infinite_pagination'] == 2 ? "SELECTED" : ""; ?>>Disable</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Footer Text</label>
                                        <input type="text" class="form-control" id="inputFooterText"
                                               value="<?php echo $getRow['footer_text']; ?>">
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Update</button>
                                    </div>
                                </form>
                            </div>
                            <div class="tab-pane" id="userSettingsTab">
                                <h3>User Settings</h3>
                                <div id="userSettingsStatus"></div>
                                <form id="userSettingsForm">
                                    <div class="form-group">
                                        <label>User Registration</label>
                                        <select class="form-control" id="inputUserReg">
                                            <option value="1">Enabled</option>
                                            <option value="2" <?php echo $getRow['user_registration'] == 2 ? "SELECTED" : ""; ?>>Disabled</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Captcha</label>
                                        <select class="form-control" id="inputUserCaptcha">
                                            <option value="1">Enabled</option>
                                            <option value="2" <?php echo $getRow['captcha_enabled'] == 2 ? "SELECTED" : ""; ?>>Disabled</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>reCaptcha Site Key</label>
                                        <input type="text" class="form-control" id="inputRCSiteKey" value="<?php echo $getRow['recaptcha_site_key']; ?>">
                                    </div>
                                    <div class="form-group">
                                        <label>reCaptcha Private Key</label>
                                        <input type="text" class="form-control" id="inputRCPrivateKey" value="<?php echo $getRow['recaptcha_private_key']; ?>">
                                    </div>
                                    <div class="form-group">
                                        <label>Validate E-mail</label>
                                        <select class="form-control" id="inputValidateEmail">
                                            <option value="1">Enabled</option>
                                            <option value="2" <?php echo $getRow['validate_email'] == 2 ? "SELECTED" : ""; ?>>Disabled</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Profile Image Size (mb)</label>
                                        <input type="number" class="form-control" id="inputProfileImgSize" value="<?php echo $getRow['profile_img_size']; ?>">
                                    </div>
                                    <div class="form-group">
                                        <label>Profile Header Image Size (mb)</label>
                                        <input type="number" class="form-control" id="inputProfileHeaderImgSize" value="<?php echo $getRow['profile_header_size']; ?>">
                                    </div>
                                    <div class="form-group">
                                        <label>Friends per Page</label>
                                        <input type="number" class="form-control" id="inputFriendsPerPage"  value="<?php echo $getRow['friend_display_count']; ?>">
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Update</button>
                                    </div>
                                </form>
                            </div>
                            <div class="tab-pane" id="trackSettingsTab">
                                <h3>Track Settings</h3>
                                <div id="trackSettingsStatus"></div>
                                <form id="trackSettingsForm">
                                    <div class="form-group">
                                        <label>Track Image Size (mb)</label>
                                        <input type="number" class="form-control" id="inputTrackImgSize"
                                               value="<?php echo $getRow['track_img_size']; ?>">
                                    </div>
                                    <div class="form-group">
                                        <label>Regular Account Track Size (mb)</label>
                                        <input type="number" class="form-control" id="inputRegularTrackSize"
                                               value="<?php echo $getRow['regular_track_size']; ?>">
                                    </div>
                                    <div class="form-group">
                                        <label>Pro Account Track Size (mb)</label>
                                        <input type="number" class="form-control" id="inputProTrackSize"
                                               value="<?php echo $getRow['pro_track_size']; ?>">
                                    </div>
                                    <div class="form-group">
                                        <label>Regular Account Storage Size (mb)</label>
                                        <input type="number" class="form-control" id="inputRegularStorage"
                                               value="<?php echo $getRow['regular_storage_size']; ?>">
                                    </div>
                                    <div class="form-group">
                                        <label>Pro Account Storage Size (mb)</label>
                                        <input type="number" class="form-control" id="inputProStorage"
                                               value="<?php echo $getRow['pro_storage_size']; ?>">
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Update</button>
                                    </div>
                                </form>
                            </div>
                            <div class="tab-pane" id="proSettingsTab">
                                <h3>Pro Settings</h3>
                                <div id="proSettingsStatus"></div>
                                <form id="proSettingsForm">
                                    <div class="form-group">
                                        <label>Pro Accounts</label>
                                        <select class="form-control" id="inputProAccounts">
                                            <option value="1">Enabled</option>
                                            <option value="2" <?php echo $getRow['pro_accounts'] == 2 ? "SELECTED": ""; ?>>Disabled</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Paypal Email</label>
                                        <input type="text" class="form-control" id="inputPaypalEmail"
                                               value="<?php echo $getRow['paypal_email']; ?>">
                                    </div>
                                    <div class="form-group">
                                        <label>Currency Code</label>
                                        <input type="text" class="form-control" id="inputCurrencyCode"
                                               value="<?php echo $getRow['currency_code']; ?>">
                                    </div>
                                    <div class="form-group">
                                        <label>Monthly Price</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
                                            <input type="text" class="form-control" id="inputMonthlyPrice"
                                                   value="<?php echo $getRow['monthly_price']; ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Yearly Price</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
                                            <input type="text" class="form-control" id="inputYearlyPrice"
                                                   value="<?php echo $getRow['yearly_price']; ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Update</button>
                                    </div>
                                </form>
                            </div>
                            <div class="tab-pane" id="socialSettingsTab">
                                <h3>Social Settings</h3>
                                <div id="socialSettingsStatus"></div>
                                <form id="socialSettingsForm">
                                    <div class="form-group">
                                        <label>Facebook</label>
                                        <input type="text" class="form-control" id="inputFB"
                                               value="<?php echo $getRow['facebook']; ?>">
                                    </div>
                                    <div class="form-group">
                                        <label>Twitter Username</label>
                                        <input type="text" class="form-control" id="inputTwitterUsername"
                                               value="<?php echo $getRow['twitter_username']; ?>">
                                    </div>
                                    <div class="form-group">
                                        <label>Youtube</label>
                                        <input type="text" class="form-control" id="inputYoutube"
                                               value="<?php echo $getRow['youtube']; ?>">
                                    </div>
                                    <div class="form-group">
                                        <label>Instagram</label>
                                        <input type="text" class="form-control" id="inputInstagram"
                                               value="<?php echo $getRow['instagram']; ?>">
                                    </div>
                                    <div class="form-group">
                                        <label>Google Plus</label>
                                        <input type="text" class="form-control" id="inputGP"
                                               value="<?php echo $getRow['gp']; ?>">
                                    </div>
                                    <div class="form-group">
                                        <label>Pinterest</label>
                                        <input type="text" class="form-control" id="inputPinterest"
                                               value="<?php echo $getRow['pinterest']; ?>">
                                    </div>
                                    <div class="form-group">
                                        <label>Facebook App Key</label>
                                        <input type="text" class="form-control" id="inputFBAppKey"
                                               value="<?php echo $getRow['facebook_app_key']; ?>">
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Update</button>
                                    </div>
                                </form>
                            </div>
                            <div class="tab-pane" id="adSettingsTab">
                                <h3>Ad Settings</h3>
                                <div id="adSettingsStatus"></div>
                                <form id="adSettingsForm">
                                    <div class="form-group">
                                        <label>Sidebar Ad</label>
                                        <textarea class="form-control" id="inputSidebarAd"><?php echo $getRow['sidebar_ad']; ?></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Footer ad</label>
                                        <textarea class="form-control" id="inputFooterAd"><?php echo $getRow['footer_ad']; ?></textarea>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Update</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- /col-lg-9 END SECTION MIDDLE -->
        </div>
    </section>
</section>
<?php
include('inc/footer.inc.php');
?>
<script src="assets/custom/settings.js"></script>