<?php
/**
 * Created by PhpStorm.
 * User: jaskokoyn
 * Date: 3/27/2015
 * Time: 12:38 AM
 */

require( '../inc/db.php' );
include( '../inc/func.inc.php' );

if(!isset($_SESSION['isAdmin']) || $_SESSION['isAdmin'] !== true){
    header("location:../");
    exit();
}

if(!isset($_GET['id'])){
    header("location:manage-users.php");
    exit();
}

$id                 =   intval($_GET['id']);
$getQuery           =   $db->prepare("SELECT * FROM users WHERE id = :id");
$getQuery->execute(array(
    ":id"           =>  $id
));

if($getQuery->rowCount() === 0){
    header("location:manage-users.php");
    exit();
}

$getRow             =   $getQuery->fetch();

include('inc/header.inc.php');
?>
<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <h3><i class="fa fa-angle-right"></i> Edit User - <?php echo $getRow['username']; ?></h3>
        <div class="row mt">
            <div class="col-lg-12">
                <div class="form-panel">
                    <div id="editUserStatus"></div>
                    <form id="editUserForm">
                        <input type="hidden" id="inputID" value="<?php echo $getRow['id']; ?>">
                        <div class="form-group">
                            <label>E-mail</label>
                            <input type="text" class="form-control" id="inputEmail" value="<?php echo $getRow['email']; ?>">
                        </div>
                        <div class="form-group">
                            <label>Location</label>
                            <input type="text" class="form-control" id="inputLocation" value="<?php echo $getRow['location']; ?>">
                        </div>
                        <div class="form-group">
                            <label>Display Name</label>
                            <input type="text" class="form-control" id="inputDN" value="<?php echo $getRow['display_name']; ?>">
                        </div>
                        <div class="form-group">
                            <label>Intro Text</label>
                            <input type="text" class="form-control" id="inputIntroText" value="<?php echo $getRow['intro_text']; ?>">
                        </div>
                        <div class="form-group">
                            <label>About</label>
                            <textarea class="form-control" id="inputAbout"><?php echo $getRow['about']; ?></textarea>
                        </div>
                        <div class="form-group">
                            <label>Website</label>
                            <input type="text" class="form-control" id="inputWebsite" value="<?php echo $getRow['website']; ?>">
                        </div>
                        <div class="form-group">
                            <label>Facebook</label>
                            <input type="text" class="form-control" id="inputFB" value="<?php echo $getRow['facebook']; ?>">
                        </div>
                        <div class="form-group">
                            <label>Twitter</label>
                            <input type="text" class="form-control" id="inputTwitter" value="<?php echo $getRow['twitter']; ?>">
                        </div>
                        <div class="form-group">
                            <label>Google Plus</label>
                            <input type="text" class="form-control" id="inputGP" value="<?php echo $getRow['gplus']; ?>">
                        </div>
                        <div class="form-group">
                            <label>Youtube</label>
                            <input type="text" class="form-control" id="inputYT" value="<?php echo $getRow['youtube']; ?>">
                        </div>
                        <div class="form-group">
                            <label>VK</label>
                            <input type="text" class="form-control" id="inputVK" value="<?php echo $getRow['vk']; ?>">
                        </div>
                        <div class="form-group">
                            <label>Soundcloud</label>
                            <input type="text" class="form-control" id="inputSoundcloud" value="<?php echo $getRow['soundcloud']; ?>">
                        </div>
                        <div class="form-group">
                            <label>Pro account</label>
                            <select class="form-control" id="inputPro">
                                <option value="1">Disabled</option>
                                <option value="2" <?php echo $getRow['isPro'] == 2 ? "SELECTED" : ""; ?>>Enabled</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Update</button>
                        </div>
                    </form>
                </div>
            </div><!-- /col-lg-9 END SECTION MIDDLE -->
        </div>
    </section>
</section>

    <!-- /.row -->
<?php
include('inc/footer.inc.php');
?>
<script src="assets/custom/edit-user.js"></script>