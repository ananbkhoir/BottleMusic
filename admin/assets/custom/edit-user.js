/**
 * Created by jaskokoyn on 3/27/2015.
 */
jQuery(function($){
    $("#editUserForm").submit(function(e){
        e.preventDefault();

        $("#editUserStatus").html(
            '<div class="alert alert-info">User currently being updated.</div>'
        );

        $("#editUserForm").slideUp('fast', function(){
            var formObj         =   {
                id:             $("#inputID").val(),
                email:          $("#inputEmail").val(),
                location:       $("#inputLocation").val(),
                dn:             $("#inputDN").val(),
                intro:          $("#inputIntroText").val(),
                about:          $("#inputAbout").val(),
                website:        $("#inputWebsite").val(),
                fb:             $("#inputFB").val(),
                twitter:        $("#inputTwitter").val(),
                gp:             $("#inputGP").val(),
                yt:             $("#inputYT").val(),
                vk:             $("#inputVK").val(),
                soundcloud:     $("#inputSoundcloud").val(),
                isPro:         $("#inputPro").val()
            };

            $.post("actions/edit-user.php", formObj, function(res){
                var res         =   JSON.parse(res);
                if(res.status == 1){
                    $("#editUserStatus").html(
                        '<div class="alert alert-danger">Unable to update user.</div>'
                    );
                    $("#editUserForm").slideDown('fast');
                    return null;
                }

                $("#editUserStatus").html(
                    '<div class="alert alert-success"><strong>User updated!</strong>.</div>'
                );
                $("#editUserForm").slideDown('fast');
            });
        });
    });
});