/**
 * Created by jaskokoyn on 3/28/2015.
 */
jQuery(function($){
    $(document).on('click', '.delTrack', function(e){
        e.preventDefault();
        if(confirm("Are you sure you want to delete this track? This cannot be undone.")){
            $(e.target).remove();
            toastr.info("Track being deleted.", "Please wait!");
            $.post("actions/delete-track.php", { rid: $("#report_id").val(), tid: $("#track_id").val() }, function(res){
                toastr.success("Track deleted.", "Success!");
                $("#statusText").text("Track Deleted");
            });
        }
    });
    $(document).on('click', '.suspendTrack', function(e){
        e.preventDefault();
        if(confirm("Are you sure you want to suspend this track?")){
            $(e.target).remove();
            toastr.info("Track being suspended.", "Please wait!");
            $.post("actions/toggle-track-status.php", { rid: $("#report_id").val(), tid: $("#track_id").val() }, function(res){
                toastr.success("Track suspended.", "Success!");
                $("#statusText").text("Track Suspended");
            });
        }
    });
    $(document).on('click', '.delComment', function(e){
        e.preventDefault();
        if(confirm("Are you sure you want to delete this comment? This cannot be undone.")){
            $(e.target).remove();
            toastr.info("Comment being deleted.", "Please wait!");
            $.post("actions/delete-comment.php", { rid: $("#report_id").val(), cid: $("#comment_id").val() }, function(res){
                toastr.success("Comment deleted.", "Success!");
                $("#statusText").text("Comment Deleted");
            });
        }
    });
    $(document).on('click', '.banUser', function(e){
        e.preventDefault();
        if(confirm("Are you sure you want to ban this user?")){
            $(e.target).remove();
            toastr.info("User being banned.", "Please wait!");
            $.post("actions/ban-user.php", { rid: $("#report_id").val(), uid: $("#user_id").val() }, function(res){
                toastr.success("User banned.", "Success!");
                $("#statusText").text("User Banned");
            });
        }
    });
});