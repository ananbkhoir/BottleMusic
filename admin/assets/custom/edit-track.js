/**
 * Created by jaskokoyn on 3/27/2015.
 */
jQuery(function($){
    $("#editTrackForm").submit(function(e){
        e.preventDefault();

        $("#editTrackStatus").html(
            '<div class="alert alert-info">Track currently being updated.</div>'
        );

        $("#editTrackForm").slideUp('fast', function(){
            var formObj         =   {
                id:             $("#inputID").val(),
                title:          $("#inputTitle").val(),
                desc:           $("#inputDesc").val(),
                tags:           $("#inputTags").val(),
                pl:             $("#inputPL").val(),
                rd:             $("#inputRD").val(),
                visible:        $("#inputVisib").val(),
                downloadable:   $("#inputDL").val(),
                gid:            $("#inputGID").val()
            };

            $.post("actions/edit-track.php", formObj, function(res){
                res         =   JSON.parse(res);
                if(res.status == 1){
                    $("#editTrackStatus").html(
                        '<div class="alert alert-danger">Unable to update track.</div>'
                    );
                    $("#editUserForm").slideDown('fast');
                    return null;
                }

                $("#editTrackStatus").html(
                    '<div class="alert alert-success"><strong>Track updated!</strong>.</div>'
                );
                $("#editTrackForm").slideDown('fast');
            });
        });
    });
});