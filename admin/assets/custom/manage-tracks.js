/**
 * Created by jaskokoyn on 3/27/2015.
 */
jQuery(function($){
    $(document).on('click', '.suspendTrackBtn', function(e){
        e.preventDefault();

        if(confirm("Are you sure you want to suspend this track?")){
            var tid     =   $(this).data('tid');

            $(this).closest("tr").fadeOut('fast', function(){
                $.post("actions/toggle-track-status.php", { tid: tid });
                $(this).remove();
            });
        }
    });

    $(document).on('click', '.deleteTrackBtn', function(e){
        e.preventDefault();

        if(confirm("Are you sure you want to suspend this track? This action can not be undone")){
            var tid     =   $(this).data('tid');

            $(this).closest("tr").fadeOut('fast', function(){
                $.post("actions/delete-track.php", { tid: tid });
                $(this).remove();
            });
        }
    });
});