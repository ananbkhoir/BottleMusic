/**
 * Created by jaskokoyn on 3/27/2015.
 */
jQuery(function($){
    $(document).on('click', '.unbanUserBtn', function(e){
        e.preventDefault();

        if(confirm("Are you sure you want to unban this user?")){
            var uid     =   $(this).data('uid');

            $(this).closest("tr").fadeOut('fast', function(){
                $.post("actions/toggle-ban-user.php", { uid: uid });
                $(this).remove();
            });
        }
    });
});