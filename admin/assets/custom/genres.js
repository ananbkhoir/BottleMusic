/**
 * Created by jaskokoyn on 3/28/2015.
 */
jQuery(function($){
    $("#createGenreForm").submit(function(e){
        e.preventDefault();

        $("#createGenreStatus").html(
            '<div class="alert alert-info">Creating Genre. Please wait.</div>'
        );

        $(this).slideUp('fast', function(){
            var formObj     =   {
                name: $("#inputGenreName").val(),
                pos: $("#inputGenrePos").val()
            };

            $.post("actions/create-genre.php", formObj, function(res){
                res         =   JSON.parse(res);
                if(res.status == 2){
                    $("#createGenreStatus").html(
                        '<div class="alert alert-success">Success! Genre Created!</div>'
                    );
                    location.reload();
                    return null;
                }

                $("#createGenreStatus").html(
                    '<div class="alert alert-danger">Could not create genre. Try again later.</div>'
                );
                $("#createGenreForm").slideDown('fast');
            });
        });
    });

    $(".btnDelGenre").click(function(e){
        e.preventDefault();

        var gid         =   $(this).data('gid');

        if(confirm("Are you sure you want to do this? This action can not be undone.")){
            $(this).closest("tr").fadeOut('fast', function(){
                $.post("actions/delete-genre.php", { gid: gid } );
            });
        }
    });

    $(document).on("click", ".btnEditGenre", function(e){
        e.preventDefault();
        var parentElem  =   $(this).closest("tr");
        $("#inputEditGenreID").val( parentElem.find("td").eq(0).text() );
        $("#inputEditGenreName").val( parentElem.find("td").eq(1).text() );
        $("#inputEditGenrePos").val( parentElem.find("td").eq(2).text() );
    });

    $("#editGenreForm").submit(function(e){
        e.preventDefault();

        $("#editGenreStatus").html(
            '<div class="alert alert-info">Updating Genre. Please wait.</div>'
        );

        $(this).slideUp('fast', function(){
            var formObj     =   {
                gid:  $("#inputEditGenreID").val(),
                name: $("#inputEditGenreName").val(),
                pos: $("#inputEditGenrePos").val()
            };

            $.post("actions/edit-genre.php", formObj, function(res){
                res         =   JSON.parse(res);
                if(res.status == 2){
                    $("#createGenreStatus").html(
                        '<div class="alert alert-success">Success! Genre Updated!</div>'
                    );
                    location.reload();
                    return null;
                }

                $("#editGenreStatus").html(
                    '<div class="alert alert-danger">Could not update genre. Try again later.</div>'
                );
                $("#editGenreForm").slideDown('fast');
            });
        });
    });
});