/**
 * Created by jaskokoyn on 3/30/2015.
 */
jQuery(function($){
    $("#genSettingsForm").submit(function(e){
        e.preventDefault();

        $("#genSettingsStatus").html(
            '<div class="alert alert-info">Updating settings please wait.</div>'
        );

        $("#genSettingsForm").slideUp('fast', function(){
            var formObj         =   {
                name:               $("#inputSiteName").val(),
                desc:               $("#inputSiteDesc").val(),
                count:              $("#inputSearchCount").val(),
                pagination:         $("#inputInfinitePagination").val(),
                footer:             $("#inputFooterText").val()
            };

            $.post("actions/update-gen-settings.php", formObj, function(res){
                res         =   JSON.parse(res);
                if(res.status == 1){
                    $("#genSettingsStatus").html(
                        '<div class="alert alert-danger">Unable to update settings.</div>'
                    );
                    $("#genSettingsForm").slideDown('fast');
                    return null;
                }

                $("#genSettingsStatus").html(
                    '<div class="alert alert-success"><strong>Settings updated!</strong>.</div>'
                );
                $("#genSettingsForm").slideDown('fast');
            });
        });
    });

    $("#userSettingsForm").submit(function(e){
        e.preventDefault();

        $("#userSettingsStatus").html(
            '<div class="alert alert-info">Updating settings please wait.</div>'
        );

        $("#userSettingsForm").slideUp('fast', function(){
            var formObj         =   {
                user_reg:                   $("#inputUserReg").val(),
                user_captcha:               $("#inputUserCaptcha").val(),
                site_key:                   $("#inputRCSiteKey").val(),
                private_key:                $("#inputRCPrivateKey").val(),
                validate_email:             $("#inputValidateEmail").val(),
                profile_size:               $("#inputProfileImgSize").val(),
                header_size:                $("#inputProfileHeaderImgSize").val(),
                friend_count:               $("#inputFriendsPerPage").val()
            };

            $.post("actions/update-user-settings.php", formObj, function(res){
                res         =   JSON.parse(res);
                if(res.status == 1){
                    $("#userSettingsStatus").html(
                        '<div class="alert alert-danger">Unable to update settings.</div>'
                    );
                    $("#userSettingsForm").slideDown('fast');
                    return null;
                }

                $("#userSettingsStatus").html(
                    '<div class="alert alert-success"><strong>Settings updated!</strong>.</div>'
                );
                $("#userSettingsForm").slideDown('fast');
            });
        });
    });

    $("#trackSettingsForm").submit(function(e){
        e.preventDefault();

        $("#trackSettingsStatus").html(
            '<div class="alert alert-info">Updating settings please wait.</div>'
        );

        $("#trackSettingsForm").slideUp('fast', function(){
            var formObj         =   {
                track_img_size:                     $("#inputTrackImgSize").val(),
                regular_track_size:                 $("#inputRegularTrackSize").val(),
                pro_track_size:                     $("#inputProTrackSize").val(),
                regular_storage:                    $("#inputRegularStorage").val(),
                pro_storage:                        $("#inputProStorage").val()
            };

            $.post("actions/update-track-settings.php", formObj, function(res){
                res         =   JSON.parse(res);
                if(res.status == 1){
                    $("#trackSettingsStatus").html(
                        '<div class="alert alert-danger">Unable to update settings.</div>'
                    );
                    $("#trackSettingsForm").slideDown('fast');
                    return null;
                }

                $("#trackSettingsStatus").html(
                    '<div class="alert alert-success"><strong>Settings updated!</strong>.</div>'
                );
                $("#trackSettingsForm").slideDown('fast');
            });
        });
    });

    $("#proSettingsForm").submit(function(e){
        e.preventDefault();

        $("#proSettingsStatus").html(
            '<div class="alert alert-info">Updating settings please wait.</div>'
        );

        $("#proSettingsForm").slideUp('fast', function(){
            var formObj         =   {
                pro_accounts:                   $("#inputProAccounts").val(),
                paypal_email:                   $("#inputPaypalEmail").val(),
                currency:                       $("#inputCurrencyCode").val(),
                monthly_price:                  $("#inputMonthlyPrice").val(),
                yearly_price:                   $("#inputYearlyPrice").val()
            };

            $.post("actions/update-pro-settings.php", formObj, function(res){
                res         =   JSON.parse(res);
                if(res.status == 1){
                    $("#proSettingsStatus").html(
                        '<div class="alert alert-danger">Unable to update settings.</div>'
                    );
                    $("#proSettingsForm").slideDown('fast');
                    return null;
                }

                $("#proSettingsStatus").html(
                    '<div class="alert alert-success"><strong>Settings updated!</strong>.</div>'
                );
                $("#proSettingsForm").slideDown('fast');
            });
        });
    });

    $("#socialSettingsForm").submit(function(e){
        e.preventDefault();

        $("#socialSettingsStatus").html(
            '<div class="alert alert-info">Updating settings please wait.</div>'
        );

        $("#socialSettingsForm").slideUp('fast', function(){
            var formObj         =   {
                username:                       $("#inputTwitterUsername").val(),
                fb:                             $("#inputFB").val(),
                yt:                             $("#inputYoutube").val(),
                instagram:                      $("#inputInstagram").val(),
                gp:                             $("#inputGP").val(),
                pinterest:                      $("#inputPinterest").val(),
                app_key:                        $("#inputFBAppKey").val()
            };

            $.post("actions/update-social-settings.php", formObj, function(res){
                console.log(res);
                res         =   JSON.parse(res);
                if(res.status == 1){
                    $("#socialSettingsStatus").html(
                        '<div class="alert alert-danger">Unable to update settings.</div>'
                    );
                    $("#socialSettingsForm").slideDown('fast');
                    return null;
                }

                $("#socialSettingsStatus").html(
                    '<div class="alert alert-success"><strong>Settings updated!</strong>.</div>'
                );
                $("#socialSettingsForm").slideDown('fast');
            });
        });
    });

    $("#adSettingsForm").submit(function(e){
        e.preventDefault();

        $("#adSettingsStatus").html(
            '<div class="alert alert-info">Updating settings please wait.</div>'
        );

        $("#adSettingsForm").slideUp('fast', function(){
            var formObj         =   {
                sidebar:                     $("#inputSidebarAd").val(),
                footer:                      $("#inputFooterAd").val()
            };

            $.post("actions/update-ad-settings.php", formObj, function(res){
                res         =   JSON.parse(res);
                $("#adSettingsForm").slideDown('fast');

                if(res.status == 1){
                    $("#adSettingsStatus").html(
                        '<div class="alert alert-danger">Unable to update settings.</div>'
                    );
                    return null;
                }
                $("#adSettingsStatus").html(
                    '<div class="alert alert-success"><strong>Settings updated!</strong>.</div>'
                );
            });
        });
    });
});