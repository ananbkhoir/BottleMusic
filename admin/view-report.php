<?php
/**
 * Created by PhpStorm.
 * User: jaskokoyn
 * Date: 3/27/2015
 * Time: 10:20 PM
 */

require( '../inc/db.php' );
include( '../inc/func.inc.php' );

if(!isset($_SESSION['isAdmin']) || $_SESSION['isAdmin'] !== true){
    header("location:../");
    exit();
}

if(!isset($_GET['id'])){
    header("location:manage-users.php");
    exit();
}

$id                 =   intval($_GET['id']);
$getQuery           =   $db->prepare("
SELECT *,(
  SELECT `title` FROM tracks WHERE id = r.tid
) as `track_title`,(
  SELECT `track_url` FROM tracks WHERE id = r.tid
) as `track_url`,(
  SELECT `username` FROM users WHERE id = r.uid
) as `username` FROM reports r
WHERE id = :id
");
$getQuery->execute(array(
    ":id"           =>  $id
));

if($getQuery->rowCount() === 0){
    header("location:manage-reports.php");
    exit();
}

$getRow             =   $getQuery->fetch();

if($getRow['status'] == 2){
    $status         =   "Commented Deleted";
}else if($getRow['status'] == 3){
    $status         =   "Track Deleted";
}else if($getRow['status'] == 4){
    $status         =   "Track Suspended";
}else if($getRow['status'] == 5){
    $status         =   "User Banned";
}else{
    $status         =   "No Action Taken";
}

if($getRow['r_type'] == 1){
    $type   =   "Track";
}else if($getRow['r_type'] == 2){
    $type   =   "Comment";
}else if($getRow['r_type'] == 3){
    $type   =   "User";
    $userReportedQuery  =   $db->prepare("SELECT * FROM users WHERE id = :ruid");
    $userReportedQuery->execute(array(
        ":ruid"         =>  $getRow['ruid']
    ));
    $userReportedRow    =   $userReportedQuery->fetch();
}

include('inc/header.inc.php');
?>
<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <h3><i class="fa fa-angle-right"></i> Viewing Report - ID: <?php echo $getRow['id']; ?></h3>
        <div class="row mt">
            <div class="col-lg-12">
                <div class="form-panel">
                    <ul class="list-unstyled">
                        <li><strong>Type: </strong> - <?php echo $type; ?></li>
                        <li><strong>Track: </strong> - <a href="../track/<?php echo $getRow['track_url'] . '-' . $getRow['tid']; ?>" target="_blank"><?php echo $getRow['track_title']; ?></a></li>
                        <li><strong>Submitted By: </strong> - <a href="../profile/<?php echo $getRow['username']; ?>" target="_blank"><?php echo $getRow['username']; ?></a></li>
                        <li><strong>Contact E-mail: </strong> - <?php echo $getRow['contact_email']; ?></li>
                        <li><strong>Contact Number: </strong> - <?php echo $getRow['contact_number']; ?></li>
                        <li><strong>Status: </strong> - <span id="statusText"><?php echo $status; ?></span></li>
                        <?php
                        if($getRow['r_type'] == 3){
                            ?>
                            <li><strong>User Reported: </strong> - <?php echo $userReportedRow['username']; ?></li>
                        <?php
                        }
                        ?>
                    </ul>
                    <h3>Details</h3>
                    <p><?php echo $getRow['report_desc']; ?></p>
                    <hr>
                    <input type="hidden" id="report_id" value="<?php echo $getRow['id']; ?>">
                    <?php
                    if($getRow['status'] == 1){
                        if($getRow['r_type'] == 1){
                            ?>
                            <input type="hidden" id="track_id" value="<?php echo $getRow['tid']; ?>">
                            <button type="button" class="btn btn-danger delTrack">Delete Track</button>
                            <button type="button" class="btn btn-primary suspendTrack">Suspend Track</button>
                        <?php
                        }else if($getRow['r_type'] == 3){
                            ?>
                            <input type="hidden" id="user_id" value="<?php echo $getRow['ruid']; ?>">
                            <button type="button" class="btn btn-danger banUser">Ban User</button>
                        <?php
                        }else{
                            ?>
                            <input type="hidden" id="comment_id" value="<?php echo $getRow['cid']; ?>">
                            <button type="button" class="btn btn-danger delComment">Delete Comment</button>
                        <?php
                        }
                    }
                    ?>
                </div>
            </div><!-- /col-lg-9 END SECTION MIDDLE -->
        </div>
    </section>
</section>

<!-- /.row -->
<?php
include('inc/footer.inc.php');
?>
<link rel="stylesheet" href="../assets/toastr/toastr.min.css">
<script src="../assets/toastr/toastr.min.js"></script>
<script src="assets/custom/view-report.js"></script>