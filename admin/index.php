<?php
/**
 * Created by PhpStorm.
 * User: jaskokoyn
 * Date: 3/26/2015
 * Time: 4:44 PM
 */

require( '../inc/db.php' );
include( '../inc/func.inc.php' );

if(!isset($_SESSION['isAdmin']) || $_SESSION['isAdmin'] !== true){
    header("location:../");
    exit();
}

$userCount  =   $db->prepare("SELECT COUNT(*) as `userCount` FROM users");
$userCount->execute();
$userCount  =   $userCount->fetch();

$trackCount =   $db->prepare("SELECT COUNT(*) as `trackCount` FROM tracks");
$trackCount->execute();
$trackCount =   $trackCount->fetch();

$reportCount =   $db->prepare("SELECT COUNT(*) as `reportCount` FROM reports");
$reportCount->execute();
$reportCount =   $reportCount->fetch();

$playlistCount =   $db->prepare("SELECT COUNT(*) as `playlistCount` FROM playlists");
$playlistCount->execute();
$playlistCount =   $playlistCount->fetch();

include( 'inc/header.inc.php' );
?>
<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h3>Dashboard Overview</h3>
                <div class="row">
                    <div class="col-md-2 col-sm-2 col-md-offset-1 box0">
                        <div class="box1">
                            <span class="li_user"></span>
                            <h3><?php echo $userCount['userCount']; ?></h3>
                        </div>
                        <p>You have <?php echo $userCount['userCount']; ?> registered user(s) on your site!</p>
                    </div>
                    <div class="col-md-2 col-sm-2 box0">
                        <div class="box1">
                            <span class="li_music"></span>
                            <h3><?php echo $trackCount['trackCount']; ?></h3>
                        </div>
                        <p>There are <?php echo $trackCount['trackCount']; ?> tracks currently uploaded.</p>
                    </div>
                    <div class="col-md-2 col-sm-2 box0">
                        <div class="box1">
                            <span class="li_stack"></span>
                            <h3><?php echo $reportCount['reportCount']; ?></h3>
                        </div>
                        <p>There are <?php echo $reportCount['reportCount']; ?> reports.</p>
                    </div>
                    <div class="col-md-2 col-sm-2 box0">
                        <div class="box1">
                            <span class="li_star"></span>
                            <h3><?php echo $playlistCount['playlistCount']; ?></h3>
                        </div>
                        <p>There are <?php echo $playlistCount['playlistCount']; ?> playlists.</p>
                    </div>
                    <div class="col-md-2 col-sm-2 box0">
                        <div class="box1">
                            <span class="li_data"></span>
                            <h3>OK!</h3>
                        </div>
                        <p>Your server is working perfectly. Relax & enjoy.</p>
                    </div>

                </div><!-- /row mt -->
            </div><!-- /col-lg-9 END SECTION MIDDLE -->
        </div>
    </section>
</section>
<!--main content end-->
<?php
include( 'inc/footer.inc.php' );