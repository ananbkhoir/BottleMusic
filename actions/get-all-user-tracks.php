<?php
/**
 * Created by PhpStorm.
 * User: jaskokoyn
 * Date: 4/6/2015
 * Time: 4:08 PM
 */

require( '../inc/db.php' );
include( '../inc/func.inc.php' );

$params                 =   json_decode(file_get_contents('php://input'), true);
$output                 =   array('status' => 1);

if(!isset($_SESSION['loggedin'])){
    dj($output);
}

$tracksQuery            =   $db->prepare("SELECT `id`,`title`,`original_name` FROM tracks WHERE uid = :uid");
$tracksQuery->execute(array(
    ":uid"              =>  $_SESSION['uid']
));

$output['tracks']       =   $tracksQuery->fetchAll(PDO::FETCH_ASSOC);
$output['status']       =   2;
dj($output);