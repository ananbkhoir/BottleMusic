<?php
/**
 * Created by PhpStorm.
 * User: jaskokoyn
 * Date: 3/4/2015
 * Time: 7:31 PM
 */

require( '../inc/db.php' );
include( '../inc/func.inc.php' );

$output             =   array();
$params             =   json_decode(file_get_contents('php://input'), true);
$getTrackQuery      =   $db->prepare("SELECT * FROM tracks WHERE id = :tid AND uid = :uid");
$getTrackQuery->execute(array(
    ":tid"      =>  $params['tid'],
    ":uid"      =>  $_SESSION['uid']
));

if($getTrackQuery->rowCount() == 0){
    $output['status']   =   1;
    dj($output);
}

$getTrackRow        =   $getTrackQuery->fetch(PDO::FETCH_ASSOC);
$output['status']   =   2;
$output['track']    =   $getTrackRow;
dj($output);