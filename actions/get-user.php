<?php
/**
 * Created by PhpStorm.
 * User: jaskokoyn
 * Date: 2/26/2015
 * Time: 9:43 PM
 */

require( '../inc/db.php' );
include( '../inc/func.inc.php' );

$params             =   json_decode(file_get_contents('php://input'), true);
$output             =   array();
$followee           =   (isset($_SESSION['username'])) ? $_SESSION['username'] : "";
$uid                =   (isset($_SESSION['uid'])) ? $_SESSION['uid'] : 0;

$getUserQuery       =   $db->prepare("
SELECT `id`,`email`,`username`,`profile_img`,`header_img`,`display_name`,`intro_text`,`location`,`website`,
       `facebook`,`twitter`,`gplus`,`youtube`,`vk`,`soundcloud`,`about`,`isPro`, (
       SELECT COUNT(*) FROM followers
       WHERE followee = :f1 AND following = u.username
) as `isOrange` FROM users u WHERE username = :u
");
$getUserQuery->execute(array(
    "f1"    =>  $followee,
    ":u"    =>  $params['username']
));
$getUserRow         =   $getUserQuery->fetch(PDO::FETCH_ASSOC);

$getUserTracks      =   $db->prepare(
    "SELECT *, (
      SELECT COUNT(*) FROM track_likes
      WHERE uid = :uid AND tid = t.id
    ) as `isOrange` FROM tracks t WHERE username = :username ORDER BY `id` DESC"
);
$getUserTracks->execute(array(
    ":uid"       =>     $uid,
    ":username"  =>     $params['username']
));
$getUserTracks      =   $getUserTracks->fetchAll(PDO::FETCH_ASSOC);

$output['user']     =   $getUserRow;
$output['tracks']   =   $getUserTracks;

dj($output);