<?php
/**
 * Created by PhpStorm.
 * User: jaskokoyn
 * Date: 3/4/2015
 * Time: 1:35 PM
 */

require( '../inc/db.php' );
include( '../inc/func.inc.php' );

$output     =   array();
$output['status']   =   1;

if( !isReady($_POST,array("tid")) ){
    $output['message']  =   "You can not update this track.";
    dj($output);
}

if(!isset($_SESSION['loggedin'])){
    dj($output);
}

$userQuery          =   $db->prepare("SELECT * FROM users WHERE id = :id");
$userQuery->execute(array( ":id"    =>  $_SESSION['uid']));
if($userQuery->rowCount() == 0){
    $output['message']  =   "You can not update this track.";
    dj($output);
}
$userRow            =   $userQuery->fetch();

$tid                =   intval($_POST['tid']);

$checkTrackQuery    =   $db->prepare("SELECT * FROM tracks WHERE id = :id AND uid = :uid");
$checkTrackQuery->execute(array(
    ":id"   =>  $tid,
    ":uid"  =>  $_SESSION['uid']
));
if($checkTrackQuery->rowCount() != 1){
    $output['message']  =   "You can not update this track.";
    dj($output);
}

$checkTrackRow      =   $checkTrackQuery->fetch();
$title              =   secure($_POST['title']);                        // Ready
$desc               =   secure($_POST['desc']);                         // Ready
$tags               =   secure($_POST['tags']);                         // Ready
$purchase_link      =   $_POST['purchase_link'] != "null" ? secure($_POST['purchase_link']) : null;
$label              =   $_POST['record_label'] != "null" ? secure($_POST['record_label']) : null;
$release_date       =   $_POST['release_date'] != "null" ? secure($_POST['release_date']) : null;
$license            =   $_POST['license'] != "null" ? secure($_POST['license']) : null;
$license_extras     =   secure($_POST['license_extras']);               // Ready
$visibility         =   intval($_POST['visibility']) == 1 ? 1 : 2;      // Ready
$downloadable       =   intval($_POST['downloadable']) == 1 ? 1 : 2;    // Ready
$mode               =   strlen($title) > 0 ? 1 : 2;                     // Ready
$genre              =   intval($_POST['genre']);
$track_url          =   strtolower(str_replace(" ", "-", $title));
$track_url          =   str_replace("'", "", $track_url);
$track_url          =   str_replace("-", "", $track_url);
$track_url          =   urldecode($track_url);

if( !empty($purchase_link) && !filter_var($purchase_link, FILTER_VALIDATE_URL) ){ // Check Purchase Link
    $output['message']  =   "You can not update this track. The purchase link is an invalid URL.";
    dj($output);
}

$updateTrackQuery   =   $db->prepare("UPDATE tracks SET title = :title, track_desc = :t_desc,
                                                        tags = :tags, license_extra = :license_extra,
                                                        purchase_link = :purchase_link, record_label = :label,
                                                        release_date = :release_date, license = :license,
                                                        visibility = :visibility, downloadable = :downloadable,
                                                        gid = :gid, track_url = :url
                                      WHERE id = :tid AND uid = :uid");
$updateTrackQuery->execute(array(
    ":title"            =>  $title,
    ":t_desc"           =>  $desc,
    ":tags"             =>  $tags,
    ":purchase_link"    =>  $purchase_link,
    ":label"            =>  $label,
    ":release_date"     =>  $release_date,
    ":license"          =>  $license,
    ":visibility"       =>  $visibility,
    ":downloadable"     =>  $downloadable,
    ":license_extra"    =>  $license_extras,
    ":tid"              =>  $tid,
    ":gid"              =>  $genre,
    ":url"              =>  $track_url,
    ":uid"              =>  $_SESSION['uid']
));

$output['status']   =   2;
$output['message']  =   "This track has been updated successfully!";
$output['license']  =   $_POST['license'];
dj($output);