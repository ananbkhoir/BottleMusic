<?php
/**
 * Created by PhpStorm.
 * User: jaskokoyn
 * Date: 3/14/2015
 * Time: 9:58 PM
 */

require( '../inc/db.php' );
include( '../inc/func.inc.php' );

$output             =   array();
$params             =   json_decode(file_get_contents('php://input'), true);
$uid                =   isset($_SESSION['uid']) ? $_SESSION['uid'] : 0;

$getTrackQuery      =   $db->prepare("SELECT *, (
  SELECT COUNT(*) FROM track_likes
  WHERE uid = :uid AND tid = t.id
) as `isOrange` FROM tracks t WHERE id = :tid"
);
$getTrackQuery->execute(array(
    ":tid"      =>  $params['tid'],
    ":uid"      =>  $uid
));

if($getTrackQuery->rowCount() == 0){
    $output['status']   =   1;
    dj($output);
}

$getTrackRow        =   $getTrackQuery->fetch(PDO::FETCH_ASSOC);

if($getTrackRow['visibility'] == 2 && $getTrackRow['uid'] != $uid){
    dj($output);
}

$commentsQuery      =   $db->prepare("
SELECT comments.id,comments.time_created,comments.comment,users.display_name,users.profile_img
FROM comments
INNER JOIN users
ON comments.uid = users.id
WHERE tid = :tid
ORDER BY comments.id DESC
LIMIT " . $settings['search_count'] ."
");
$commentsQuery->execute(array(
    ":tid"  =>  $params['tid']
));
$commentsRow        =   $commentsQuery->fetchAll(PDO::FETCH_ASSOC);


$output['status']   =   2;
$output['track']    =   $getTrackRow;
$output['comments'] =   $commentsRow;
dj($output);