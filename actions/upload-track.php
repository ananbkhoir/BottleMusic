<?php
/**
 * Created by PhpStorm.
 * User: jaskokoyn
 * Date: 3/1/2015
 * Time: 10:19 PM
 */

require( '../inc/db.php' );
include( '../inc/func.inc.php' );

$response   =   array();

if( !isset($_SESSION['loggedin']) ){
    dj($response);
}

if(!isset($_FILES['track'])){
    $response['status']     =   1;
    $response['message']    =   "Please upload a file";
    dj($response);
}

$userQuery  =   $db->prepare("SELECT * FROM users WHERE id = :id");
$userQuery->execute(array(":id" =>  $_SESSION['uid']));
$userRow    =   $userQuery->fetch();

if($settings['validate_email'] == 1 && $userRow['email_confirmed'] == 1){
    $response['status']     =   1;
    $response['message']    =   "Confirm your email first before uploading.";
    dj($response);
}

$track          =   $_FILES['track'];
$max_size       =   $userRow['isPro'] == 1 ? $settings['regular_track_size'] : $settings['pro_track_size'];
$max_size       *=  (1024 * 1024);
$max_storage    =   $userRow['isPro'] == 1 ? $settings['regular_storage_size'] : $settings['pro_storage_size'];
$max_storage    *=   (1024 * 1024);
$file_ext       =   get_extension($track['name']);

if($track['error'] != 0 || $file_ext != "mp3" || $track['size'] < 1024 ){
    $response['status']     =   1;
    $response['message']    =   "Invalid File Upload! ";
    dj($response);
}

if($track['size'] > $max_size){
    $response['status']     =   1;
    $response['message']    =   "File track too big! Try purchasing our pro membership to upload bigger file sizes.";
    dj($response);
}

$trackStorageQuery  =   $db->prepare("SELECT SUM(track_size) as `storageSize` FROM tracks WHERE uid = :uid");
$trackStorageQuery->execute(array(
    ":uid"          =>  $_SESSION['uid']
));
$trackStorageRow    =   $trackStorageQuery->fetch();
$userStorage        =   $trackStorageRow['storageSize'] + $track['size'];
if($userStorage > $max_storage){
    $response['status']     =   1;
    $response['message']    =   "You can not upload anymore tracks. You have exceeded your storage amount. Try upgrading to pro to have a bigger storage size.";
    dj($response);
}

$track_name =   get_random_string(mt_rand(6,10)) . ".mp3";
$upl_dir    =   $userRow['upl_dir'] . "/" . $track_name;

if( !move_uploaded_file($track['tmp_name'], "../uploads/" . $upl_dir ) ){
    $response['status']     =   1;
    $response['message']    =   "File could not be uploaded. Try again later.";
    dj($response);
}

$insertTrackQuery   =   $db->prepare("INSERT INTO tracks(uid,username,upl_dir,track_name,original_name, artist)
                                      VALUES( :uid, :username, :upl, :track, :original, :artist)");
$insertTrackQuery->execute(array(
    ":uid"          =>  $_SESSION['uid'],
    ":username"     =>  $_SESSION['username'],
    ":upl"          =>  $upl_dir,
    ":track"        =>  $track_name,
    ":original"     =>  secure($track['name']),
    ":artist"       =>  $userRow['display_name']
));
$response['status']     =   2;
$response['message']    =   "Track Uploaded Successfully!";
$response['track']      =   array(
    "tid"           =>  $db->lastInsertId(),
    "title"         =>  "Untitled Track",
    "track_desc"    =>  null,
    "tags"          =>  array(),
    "track_img"     =>  null,
    "purchase_link" =>  null,
    "record_label"  =>  null,
    "release_date"  =>  null,
    "license"       =>  null,
    "license_extra" =>  array(),
    "original_name" =>  secure($track['name']),
    "visibility"    =>  "1",
    "downloadable"  =>  "1"
);
dj($response);