<?php
/**
 * Created by PhpStorm.
 * User: jaskokoyn
 * Date: 3/24/2015
 * Time: 12:27 PM
 */

require( '../inc/db.php' );
include( '../inc/func.inc.php' );

$params                 =   json_decode(file_get_contents('php://input'), true);
$output                 =   array();
$output['status']       =   1;

if(!isReady($params, array('username'))){
    dj($output);
}

$username               =   secure($params['username']);

$checkUserQuery         =   $db->prepare("SELECT `username` FROM users WHERE username = :username");
$checkUserQuery->execute(array(
    ":username"         =>  $username
));
if($checkUserQuery->rowCount() !== 1){
    dj($output);
}

