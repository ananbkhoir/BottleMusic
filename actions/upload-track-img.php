<?php
/**
 * Created by PhpStorm.
 * User: jaskokoyn
 * Date: 3/4/2015
 * Time: 8:38 PM
 */
require( '../inc/db.php' );
include( '../inc/func.inc.php' );

$output             =   array();
$output['status']   =   1;

if( !isReady($_POST,array("tid")) ){
    $output['message']  =   "You can not update this track.";
    dj($output);
}

if(!isset($_SESSION['loggedin'])){
    dj($output);
}

$userQuery          =   $db->prepare("SELECT * FROM users WHERE id = :id");
$userQuery->execute(array( ":id"    =>  $_SESSION['uid']));
if($userQuery->rowCount() == 0){
    $output['message']  =   "You can not update this track.";
    dj($output);
}
$userRow            =   $userQuery->fetch();

$tid                =   intval($_POST['tid']);

$checkTrackQuery    =   $db->prepare("SELECT * FROM tracks WHERE id = :id AND uid = :uid");
$checkTrackQuery->execute(array(
    ":id"   =>  $tid,
    ":uid"  =>  $_SESSION['uid']
));
if($checkTrackQuery->rowCount() !== 1){
    $output['message']  =   "You can not update this track.";
    dj($output);
}

if(!isset($_FILES['file'])) {
    $output['message']  =   "You must upload a image.";
    dj($output);
}

$upl_img        =   $_FILES['file'];
$img_ext        =   get_extension($upl_img['name']);

if($upl_img['error'] != 0){
    $output['message']  =   "We were unable to upload your track image. Try again later.";
    dj($output);
}

if($upl_img['size'] > 2000000){
    $output['message']  =   "The track image size is too big.";
    dj($output);
}

if( !in_array($img_ext, array("jpg","jpeg","png")) ){
    $output['message']  =   "You are only allowed to upload JPG and PNG images.";
    dj($output);
}

$img_name       =   get_random_string(16) . "." . $img_ext;
$track_img      =   $userRow['upl_dir'] . "/" . $img_name;

if( !move_uploaded_file($upl_img['tmp_name'], "../uploads/" . $track_img ) ){
    $response['message']    =   "Track image could not be uploaded.";
    dj($response);
}

$updateTrackQuery   =   $db->prepare("UPDATE tracks SET track_img = :img
                                      WHERE id = :tid AND uid = :uid");
$updateTrackQuery->execute(array(
    ":img"      =>  $track_img,
    ":tid"      =>  $tid,
    ":uid"      =>  $_SESSION['uid']
));

$response['status']     =   2;
$response['message']    =   "Track image successfully uploaded!";
$response['img']        =   "uploads/" . $track_img;
dj($response);