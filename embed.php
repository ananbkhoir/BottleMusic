<?php
/**
 * Created by PhpStorm.
 * User: jaskokoyn
 * Date: 4/4/2015
 * Time: 10:16 PM
 */

require( 'inc/db.php' );
include( 'inc/func.inc.php' );

if(!isReady($_GET, array('id'))){
    header("location:index.php");
    exit();
}

$id                         =   intval($_GET['id']);
$getTrackQuery              =   $db->prepare("SELECT * FROM tracks WHERE id = :id AND visibility='1'");
$getTrackQuery->execute(array(
    ":id"                   =>  $id
));

if($getTrackQuery->rowCount() !== 1){
    header("location:index.php");
    exit();
}

$getTrackRow                =   $getTrackQuery->fetch(PDO::FETCH_ASSOC);
?>
<link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="assets/soundmanager/demo/bar-ui/css/bar-ui.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="assets/soundmanager/script/soundmanager2-jsmin.js"></script>
<script>
    var track_url           =   '<?php echo $getTrackRow['upl_dir']; ?>';
</script>
<div class="sm2-bar-ui full-width flat">
    <div class="bd sm2-main-controls">
        <div class="sm2-inline-element sm2-button-element">
            <div class="sm2-button-bd" style="cursor:pointer;"><a class="sm2-inline-button play-pause">Play / pause</a></div>
        </div>
        <div class="sm2-inline-element sm2-inline-status">
            <div class="sm2-playlist">
                <div class="sm2-playlist-target text-center">
                    <b><a href="profile/<?php echo $getTrackRow['artist']; ?>"><?php echo $getTrackRow['artist']; ?></a></b>
                    <?php echo $getTrackRow['title']; ?>
                </div>
            </div>
            <div class="sm2-progress">
                <div class="sm2-row">
                    <div class="sm2-inline-time">0:00</div>
                    <div class="sm2-progress-bd">
                        <div class="sm2-progress-track">
                            <div class="sm2-progress-bar"></div>
                            <div class="sm2-progress-ball"><div class="icon-overlay fa-spin"></div></div>
                        </div>
                    </div>
                    <div class="sm2-inline-duration">0:00</div>
                </div>
            </div>
        </div>
        <div class="sm2-inline-element sm2-button-element sm2-volume" style="cursor:pointer;">
            <div class="sm2-button-bd">
                <a class="sm2-inline-button sm2-volume-control">volume</a>
            </div>
        </div>
    </div>
</div>
<script src="assets/main/js/player.js"></script>